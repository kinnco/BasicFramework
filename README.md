# ERP管理系统-BasicFramework

#### 介绍
前端VUE2.0使用ElementUI，后端asp.net core api+Swagger+Dapper NoSQL数据持久化+PostgreSql数据库的前后端分离的通用管理系统。

```
WebManage 文件夹放前端代码
DesktopApplication 文件夹桌面应用
WebApiServices 文件夹放服务端代码
HelpTool 放工具类代码

```
```
在线体验
演示地址：http://119.3.125.240:806/
账号密码：admin admin123
```
#### 软件架构
注意事项：

### 后端服务使用的工具类库，不是直接引用的，是通过自建私有nuget库来引用的。如果要在自己的机子上跑起来，需要增加nuget连接【私有小服务器，请不要恶意破坏】
自己也可以建立Nuget服务，将Lib中的工具类库生成nuget包，自己管理。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0826/173445_d209c3b1_1337151.png "屏幕截图.png")

软件架构说明
结构图如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0826/172953_bf4aade2_1337151.png "屏幕截图.png")

配置文件说明：

```
注意：数据库和redis链接字符串，在正式环境中，需要加密处理。
打开文件夹\ProjectCode\4.HelpTool中，找到ZHDL.Tools.sln项目启动文件，并打开，其中EncryptionDecryption项目为加密工具。
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0826/143514_6cf1bfe3_1337151.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0826/143527_ca4bceb0_1337151.png "屏幕截图.png")

前端结构：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0826/144217_ae05d0d1_1337151.png "屏幕截图.png")
前端端口配置：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0826/144248_191ffd2f_1337151.png "屏幕截图.png")

####项目演示

`Swagger图示：`
![输入图片说明](https://images.gitee.com/uploads/images/2021/0826/144510_107da419_1337151.png "屏幕截图.png")
`管理界面：`
![输入图片说明](https://images.gitee.com/uploads/images/2021/0826/144541_0985743f_1337151.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0826/144617_1f8404a5_1337151.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0826/144832_67d61003_1337151.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0826/144639_8ffa5470_1337151.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0826/144653_0015d001_1337151.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
