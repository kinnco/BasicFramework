﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace Official.Logic
{
    /// <summary>
    /// 网站设置基础信息修改实体
    /// </summary>
    public class WebBasicSettingsRequest
    {
        /// <summary>
        /// Id
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 传真
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 100, Description = "传真")]
        public string fax
        {
            get; set;
        }

        /// <summary>
        /// 公司地址
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 100, Description = "公司地址")]
        public string address
        {
            get; set;
        }

        /// <summary>
        /// 公司授权内容信息【Copyright @2019-2020 四川地理智绘科技 All Rights Reserved 】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 100, Description = "公司授权内容信息")]
        public string copyright_info
        {
            get; set;
        }

        /// <summary>
        /// 备案编号
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 100, Description = "备案编号")]
        public string record_number
        {
            get; set;
        }

        /// <summary>
        /// LOG保存地址
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 100, Description = "LOG保存地址")]
        public string log_pic
        {
            get; set;
        }

        /// <summary>
        /// 网站标题【在界面底部显示专业创造价值 诚信铸就未来】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 100, Description = "网站标题")]
        public string website_title
        {
            get; set;
        }

        /// <summary>
        /// 友情链接【包括链接名称和链接地址，使用json数组保存】
        /// </summary>
        public string friendship_links
        {
            get; set;
        }

        /// <summary>
        /// 二维码地址
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 200, Description = "二维码地址")]
        public string qr_code
        {
            get; set;
        }

        /// <summary>
        /// 电话号码
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "电话号码")]
        public string telephone
        {
            get; set;
        }

        /// <summary>
        /// 邮箱
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 200, Description = "邮箱")]
        public string email
        {
            get; set;
        }
    }
}
