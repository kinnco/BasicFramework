﻿using Common.Library;
using Common.Model;
using Container.Library;
using Dapper.Library;
using Official.Model;
using Serialize.Library;
using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  Official.Logic
*
* 功 能：管网基础设置功能
*
* 类 名：WebBasicSettingsServiceImpl  
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/5/24 14:29:56  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace Official.Logic
{
    /// <summary>
    /// 管网基础设置功能
    /// </summary>
	public class WebBasicSettingsServiceImpl : OperationLogicImpl, IWebBasicSettingsService
    {
        #region 后端基础设置管理

        /// <summary>
        /// 获取基础设置
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<WebBasicSettingsEntity> LoadWebBasicInfo()
        {
            var resultInfo = new ResultJsonInfo<WebBasicSettingsEntity>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<WebBasicSettingsEntity>().Get();
                if (result != null)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = result;
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = new WebBasicSettingsEntity();
                    resultInfo.Msg = "无对应数据！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 修改基础设置
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ModifyWebBasic(WebBasicSettingsRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = 0;
                var entity = con.QuerySet<WebBasicSettingsEntity>().Get();
                if (entity != null)
                {
                    entity.id = inputInfo.id;
                    entity.log_pic = inputInfo.log_pic;
                    entity.website_title= inputInfo.website_title;
                    entity.friendship_links = inputInfo.friendship_links;
                    entity.qr_code = inputInfo.qr_code;
                    entity.telephone = inputInfo.telephone;
                    entity.email = inputInfo.email;
                    entity.fax = inputInfo.fax;
                    entity.address = inputInfo.address;
                    entity.copyright_info = inputInfo.copyright_info;
                    entity.record_number = inputInfo.record_number;
                    result = con.CommandSet<WebBasicSettingsEntity>().Update(entity);
                }
                else
                {
                    var info = inputInfo.MapTo<WebBasicSettingsEntity>();
                    info.id = GuidHelper.GetGuid();
                    result = con.CommandSet<WebBasicSettingsEntity>().Insert(info);
                }
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = result;
                    resultInfo.Msg = "修改成功！";
                    AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.WebBasicSettingManage, $"修改官网基础设置信息:{JsonHelper.ToJson(inputInfo)}");
                }
                else
                {
                    resultInfo.Data = 0;
                    resultInfo.Msg = "修改失败！";
                }
            }
            return resultInfo;
        }

        #endregion      
    }
}
