﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

/*
* 命名空间:  Mall.Logic
*
* 功 能：用户地址编辑传输实体
*
* 类 名：ClientAddressModifyRequest  
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/8/6 10:25:25  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace Mall.Logic
{
    /// <summary>
    ///用户地址编辑传输实体 
    /// </summary>
	public class ClientAddressModifyRequest
    {

        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 用户唯一标识符
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 32, Description = "用户唯一标识符")]
        public string client_id
        {
            get; set;
        }

        /// <summary>
        /// 联系人
        /// </summary>
        public string user_name
        {
            get; set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// 省
        /// </summary>
        public string province
        {
            get; set;
        }

        /// <summary>
        /// 市
        /// </summary>
        public string city
        {
            get; set;
        }

        /// <summary>
        /// 区县
        /// </summary>
        public string county
        {
            get; set;
        }

        /// <summary>
        /// 具体地址
        /// </summary>
        public string address
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        public long sort
        {
            get; set;
        }

        /// <summary>
        /// 100默认地址 110非默认地址
        /// </summary>
        public int is_default
        {
            get; set;
        }
    }
}
