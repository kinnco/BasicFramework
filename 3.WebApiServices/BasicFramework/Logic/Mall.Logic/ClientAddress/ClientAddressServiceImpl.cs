using Common.Library;
using Common.Model;
using Container.Library;
using Dapper.Library;
using Mall.Model;
using Serialize.Library;
using System;
using System.Collections.Generic;

namespace Mall.Logic
{

    /// <summary>
    /// ClientAddress操作逻辑类
    /// </summary>
    public class ClientAddressServiceImpl : OperationLogicImpl, IClientAddressService
    {

        #region 查询

        /// <summary>
        ///根据关键字分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<ClientAddressInfoEntity>> LoadPageList(ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<ClientAddressInfoEntity>>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<ClientAddressInfoEntity>();
                if (inputInfo.parameters.IsNotNullOrEmpty())
                {
                    result.Where(p => p.client_id.Equals(inputInfo.parameters));
                }

                #region 排序

                if (inputInfo.field.IsNullOrEmpty())
                {
                    inputInfo.field = " sort";
                }
                if (inputInfo.order.IsNullOrEmpty())
                {
                    inputInfo.order = " asc";
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                #endregion

                var queryInfo = result.PageList(inputInfo.page, inputInfo.limit);
                if (queryInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = queryInfo.Items;
                    resultInfo.Count = queryInfo.Total;
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "无对应值存在！";
                }
            }
            return resultInfo;
        }

        #endregion

        #region 更新

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> SaveInfo(ClientAddressModifyRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = 0;
                if (inputInfo.id.IsNotNullOrEmpty())
                {
                    #region Modify
                    var modifyInfo = con.QuerySet<ClientAddressInfoEntity>().Where(p => p.id == inputInfo.id).Get();
                    if (modifyInfo != null)
                    {
                        modifyInfo.client_id = inputInfo.client_id;
                        modifyInfo.user_name = inputInfo.user_name; //联系人
                        modifyInfo.mobile_phone = inputInfo.mobile_phone; //联系电话
                        modifyInfo.province = inputInfo.province; //省
                        modifyInfo.city = inputInfo.city; //市
                        modifyInfo.county = inputInfo.county; //区县
                        modifyInfo.address = inputInfo.address; //具体地址
                        modifyInfo.sort = inputInfo.sort; //排序
                        modifyInfo.is_default = inputInfo.is_default; //排序
                        result = con.CommandSet<ClientAddressInfoEntity>().Update(modifyInfo);
                        if (result > 0)
                        {
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "修改成功！";
                            AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.ClientInfoManage, $"修改用户地址信息，修改信息：{ JsonHelper.ToJson(inputInfo)}");
                        }
                        else
                        {
                            resultInfo.Code = ActionCodes.InvalidOperation;
                            resultInfo.Msg = "修改失败！";
                        }
                    }
                    else
                    {
                        resultInfo.Code = ActionCodes.InvalidOperation;
                        resultInfo.Msg = "对应Id的信息不存在！";
                    }
                    #endregion
                }
                else
                {
                    #region Add
                    var addInfo = inputInfo.MapTo<ClientAddressInfoEntity>();
                    addInfo.id = GuidHelper.GetGuid();
                    result = con.CommandSet<ClientAddressInfoEntity>().Insert(addInfo);
                    if (result > 0)
                    {
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "添加成功！";
                        AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.ClientInfoManage, $"添加了一个用户地址信息，新增信息：{ JsonHelper.ToJson(inputInfo)}");
                    }
                    else
                    {
                        resultInfo.Code = ActionCodes.InvalidOperation;
                        resultInfo.Msg = "添加失败！";
                    }
                    #endregion
                }
            }
            return resultInfo;
        }
        #endregion

        #region 删除
        /// <summary>
        /// 删除用户地址信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                //删除con.Transaction(tran =>{ });
                //var info = tran.QuerySet<GoodsInfoEntity>().Where(p => p.id.In(ids.ToArray())).ToList();
                //info.ForEach(a => a.is_deleted = (int)IsDeleted.Deleted);
                //var result = tran.CommandSet<GoodsInfoEntity>().Update(info);
                var result = con.CommandSet<ClientAddressInfoEntity>().Where(p => p.id.In(ids.ToArray())).Delete();
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "删除成功！";
                    AddOperationLog(OperationLogType.RemoveOperation, BusinessTitleType.ClientInfoManage, $"删除用户地址信息成功，物理删除信息：{ JsonHelper.ToJson(ids)}");
                  }
                else
                {
                    resultInfo.Msg = "删除失败！";
                }
            }
            return resultInfo;
        }
        #endregion
    }
}
