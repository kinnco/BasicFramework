﻿using AutoMapper;
using Mall.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mall.Logic
{

    /// <summary>
    /// 商品分类实体映射
    /// </summary>
    public class GoodsCategoryProfile: Profile
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public GoodsCategoryProfile() {

            CreateMap<GoodsCategoriesEntity, GoodsCategoryTreeInfo>();
            CreateMap<GoodsCategoriesEntity, GoodsCategoryInfoResponse>();
            

            CreateMap<GoodsCategoryAddRequest,GoodsCategoriesEntity>();
            CreateMap<GoodsCategoryModifyRequest, GoodsCategoriesEntity>();

        }
    }
}
