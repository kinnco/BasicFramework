using Common.Library;
using Common.Model;
using Container.Library;
using Dapper.Library;
using Mall.Model;
using Serialize.Library;
using System;
using System.Collections.Generic;

namespace Mall.Logic
{

    /// <summary>
    /// ClientBalance操作逻辑类
    /// </summary>
    public class ClientBalanceServiceImpl : OperationLogicImpl, IClientBalanceService
    {

        #region 查询

        /// <summary>
        ///根据关键字分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<ClientBalanceDetailEntity>> LoadPageList(ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<ClientBalanceDetailEntity>>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {

                var result = con.QuerySet<ClientBalanceDetailEntity>();
                if (inputInfo.parameters.IsNotNullOrEmpty())
                {
                    result.Where(p => p.client_id.Equals(inputInfo.parameters));
                }

                #region 排序
                if (inputInfo.field.IsNullOrEmpty())
                {
                    inputInfo.field = " create_time";
                }
                if (inputInfo.order.IsNullOrEmpty())
                {
                    inputInfo.order = " desc";
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                #endregion

                //eg: var queryInfo = result.PageList(inputInfo.page, inputInfo.limit, p => new GoodsInfoEntity { id = p.id }); 
                var queryInfo = result.PageList(inputInfo.page, inputInfo.limit);
                if (queryInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = queryInfo.Items;
                    resultInfo.Count = queryInfo.Total;
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "无对应值存在！";
                }
            }
            return resultInfo;
        }
        #endregion


        #region 更新

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> SaveInfo(ClientBalanceModifyRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = 0;
                #region Add
                var addInfo = inputInfo.MapTo<ClientBalanceDetailEntity>();
                addInfo.id = GuidHelper.GetGuid();
                addInfo.create_time = DateTime.Now;
                result = con.CommandSet<ClientBalanceDetailEntity>().Insert(addInfo);
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "添加成功！";
                    AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.ClientInfoManage, $"添加了一个客户金额变动明细信息，新增信息：{ JsonHelper.ToJson(inputInfo)}");
                }
                else
                {
                    resultInfo.Code = ActionCodes.InvalidOperation;
                    resultInfo.Msg = "添加失败！";
                }
                #endregion
            }
            return resultInfo;
        }
        #endregion

        #region 删除
        /// <summary>
        /// 删除客户金额变动明细信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {

                var result = con.CommandSet<ClientBalanceDetailEntity>().Where(p => p.id.In(ids.ToArray())).Delete();
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "删除成功！";
                    AddOperationLog(OperationLogType.RemoveOperation, BusinessTitleType.ClientInfoManage, $"删除客户金额变动明细信息成功，物理删除信息：{ JsonHelper.ToJson(ids)}");
                }
                else
                {
                    resultInfo.Msg = "删除失败！";
                }
            }
            return resultInfo;
        }
        #endregion
    }
}
