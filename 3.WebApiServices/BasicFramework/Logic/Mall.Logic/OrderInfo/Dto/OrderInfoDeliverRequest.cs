﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace Mall.Logic
{
    /// <summary>
    /// 订单发货信息
    /// </summary>
    public class OrderInfoDeliverRequest
    {

        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 物流配送名称[类型]
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, Regex = ValidateRegex.Number, MaxLength = 10, Description = "物流配送类型")]

        public int logistics_type
        {
            get; set;
        }

        /// <summary>
        /// 物流单号
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "物流单号")]
        public string logistics_num
        {
            get; set;
        }

        /// <summary>
        /// 订单物流占用价格
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, Regex = ValidateRegex.Decimal, MaxLength = 10, Description = "订单物流占用价格")]
        public decimal logistics_price
        {
            get; set;
        }

        /// <summary>
        /// 发货时间
        /// </summary>
        public DateTime send_time
        {
            get; set;
        }

        /// <summary>
        /// 发货备注
        /// </summary>
        public string send_remarks
        {
            get; set;
        }
    }
}
