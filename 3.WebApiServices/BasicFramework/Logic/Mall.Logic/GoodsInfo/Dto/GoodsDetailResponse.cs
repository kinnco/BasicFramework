﻿using Mall.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mall.Logic
{
    /// <summary>
    /// 详情信息
    /// </summary>
    public class GoodsDetailResponse
    {

        /// <summary>
        /// 构造函数
        /// </summary>
        public GoodsDetailResponse() {
            baseInfo = new GoodsInfoResponse();
            fileInfo = new List<GoodsPictureEntity>();
        }

        /// <summary>
        /// 商品基础信息
        /// </summary>
        public GoodsInfoResponse baseInfo
        {
            get; set;
        }

        /// <summary>
        /// 商品附件信息
        /// </summary>
        public List<GoodsPictureEntity> fileInfo
        {
            get; set;
        }
    }
}
