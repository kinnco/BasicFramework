﻿using AutoMapper;
using Common.Library;
using Common.Model;
using Mall.Model;
using System;

namespace Mall.Logic
{
    /// <summary>
    /// 商品信息实体映射类
    /// </summary>
    public class GoodsInfoProfile: Profile
    {

        /// <summary>
        /// 构造函数
        /// </summary>
        public GoodsInfoProfile() {

            CreateMap<GoodsInfoEntity, GoodsInfoResponse>();
            CreateMap<GoodsInfoModifyRequest, GoodsInfoEntity>();
            
            CreateMap<View_GoodsInfoEntity, GoodsInfoResponse>()
              .ForMember(p => p.is_hot_style_name, opt => opt.MapFrom(x => (Convert.ToInt32(x.is_hot_style)).GetEnumDescriptionByValue(typeof(IfYesOrNo))))
              .ForMember(p => p.is_new_style_name, opt => opt.MapFrom(x => (Convert.ToInt32(x.is_new_style)).GetEnumDescriptionByValue(typeof(IfYesOrNo))))
              .ForMember(p => p.is_recommend_name, opt => opt.MapFrom(x => (Convert.ToInt32(x.is_recommend)).GetEnumDescriptionByValue(typeof(IfYesOrNo))))
              .ForMember(p => p.is_bargain_name, opt => opt.MapFrom(x => (Convert.ToInt32(x.is_bargain)).GetEnumDescriptionByValue(typeof(IfYesOrNo))))
              .ForMember(p => p.is_shelves_name, opt => opt.MapFrom(x => (Convert.ToInt32(x.is_shelves)).GetEnumDescriptionByValue(typeof(IfYesOrNo))))
              ;
        }
    }
}
