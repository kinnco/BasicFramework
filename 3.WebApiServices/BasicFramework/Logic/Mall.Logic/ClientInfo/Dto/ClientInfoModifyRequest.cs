﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  Mall.Logic
*
* 功 能：用户信息修改请求实体
*
* 类 名：ClientInfoModifyRequest  
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/8/5 15:41:04  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace Mall.Logic
{
    /// <summary>
    /// 用户信息修改请求实体
    /// </summary>
	public class ClientInfoModifyRequest
    {

        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 姓名
        /// </summary>
        public string name
        {
            get; set;
        }


        /// <summary>
        /// 电话号码
        /// </summary>
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// 性别 0未知 1男 2女
        /// </summary>
        public int sex
        {
            get; set;
        }

        /// <summary>
        /// 是否有效  100有效 110无效
        /// </summary>
        public int is_valid
        {
            get; set;
        }

        /// <summary>
        /// 积分
        /// </summary>
        public int integral
        {
            get; set;
        }

        /// <summary>
        /// 消费总金额
        /// </summary>
        public decimal monetary
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }

    }
}
