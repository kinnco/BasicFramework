﻿using Common.Model;
using DevOps.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 流程按钮信息管理模块
    /// </summary>
    public interface IFlowButtonInfoService
    {
        #region 按钮信息管理模块

        #region 查询

        /// <summary>
        /// 根据关键字 分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<FlowButtonInfoEntity>> LoadPageList(ParametersInfo<string> inputInfo);

        /// <summary>
        /// 流程步骤Id获取拥有的按钮
        /// </summary>
        /// <param name="stepId">流程步骤Id</param>
        /// <returns></returns>
        ResultJsonInfo<List<FlowOwnButtonResponse>> LoadFlowOwnButtonList(string stepId);

        #endregion

        #region 更新

        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> Save(FlowButtonInfoEntity inputInfo);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> ids);
        #endregion

        #endregion
    }
}
