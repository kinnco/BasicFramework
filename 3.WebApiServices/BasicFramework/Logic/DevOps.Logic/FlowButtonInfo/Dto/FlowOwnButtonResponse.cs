﻿using DevOps.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 流程节点拥有按钮返回
    /// </summary>
    public class FlowOwnButtonResponse: FlowButtonInfoEntity
    {
        /// <summary>
        /// 是否已经拥有
        /// </summary>
        public bool is_checked
        {
            get; set;
        }
    }
}
