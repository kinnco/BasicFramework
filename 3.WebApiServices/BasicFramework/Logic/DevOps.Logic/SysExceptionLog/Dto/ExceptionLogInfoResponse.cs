﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    ///异常 日志返回类
    /// </summary>
    public class ExceptionLogInfoResponse
    {

        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 异常类型 100：警告  101：严重警告
        /// </summary>
        public int exception_type
        {
            get; set;
        }
        /// <summary>
        /// 异常类型 100：警告  101：严重警告
        /// </summary>
        public string exception_typename
        {
            get; set;
        }
        /// <summary>
        /// 服务名称
        /// </summary>
        public string service_name
        {
            get; set;
        }

        /// <summary>
        /// 异常标题
        /// </summary>
        public string exception_title
        {
            get; set;
        }

        /// <summary>
        /// 操作明细
        /// </summary>
        public string detail
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 状态 100-未解决，101-已解决
        /// </summary>
        public int status
        {
            get; set;
        }
        /// <summary>
        /// 状态 100-未解决，101-已解决
        /// </summary>
        public string statusname
        {
            get; set;
        }
    }
}
