﻿using AutoMapper;
using Common.Library;
using Common.Model;
using DevOps.Model;
using System;

namespace DevOps.Logic
{
    /// <summary>
    /// 应用管理实体映射管理类
    /// </summary>
    public class SafeApplicationInfoProfile: Profile
    {
        /// <summary>
        /// 应用管理实体映射管理类 构造函数
        /// </summary>
        public SafeApplicationInfoProfile() {

            CreateMap<SafeApplicationInfoAddRequest, SafeApplicationInfoEntity>();

            CreateMap<SafeApplicationInfoEntity, SafeApplicationInfoResponse>()
            .ForMember(p => p.applicatian_typename, opt => opt.MapFrom(x => (Convert.ToInt32(x.applicatian_type)).GetEnumDescriptionByValue(typeof(ApplicationType))));
        }
    }
}
