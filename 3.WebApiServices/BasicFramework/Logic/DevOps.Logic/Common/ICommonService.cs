﻿using Common.Model;
using Microsoft.AspNetCore.Http;

namespace DevOps.Logic
{
    /// <summary>
    /// 公用逻辑操作类接口
    /// </summary>
    public interface ICommonService
    {
        /// <summary>
        /// 上传发布文件
        /// </summary>
        /// <param name="formFile"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        ResultJsonInfo<string> UploadPublishFile(IFormFile formFile, string filePath);
    }
}
