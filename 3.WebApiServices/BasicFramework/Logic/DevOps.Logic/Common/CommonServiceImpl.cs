﻿using Common.Model;
using Document.Library;
using Microsoft.AspNetCore.Http;
/*
* 命名空间:DevOps.Logic
*
* 功 能： CommonServiceImpl
*
* 类 名： SysPostServiceImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.2    2020/04/02 17:12:00 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 公用逻辑操作类
    /// </summary>
    public class CommonServiceImpl : ICommonService
    {

        /// <summary>
        /// 上传发布文件
        /// </summary>
        /// <param name="file"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public ResultJsonInfo<string> UploadPublishFile(IFormFile file, string filePath)
        {
            var result = new ResultJsonInfo<string>();

            if (!string.IsNullOrEmpty(filePath))
            {
                if (file.Length<=50*1000*1000)
                {
                    var saveResult = FileHelper.SaveFile(file, filePath);
                    if (!string.IsNullOrEmpty(saveResult))
                    {
                        ZipConfig config = ZipConfig.InitZipConifg();

                        IZipFileService fileService = ZipServiceBySharpZipLib.InitZipService(config);

                        bool filesResult = fileService.UnZipFile(saveResult, filePath);

                        if (filesResult)
                        {
                            result.Msg = "完成上传!";
                            result.Code = ActionCodes.Success;
                        }
                        else
                        {
                            result.Msg = "文件解压失败!";
                        }
                    }
                    else
                    {
                        result.Msg = "保存文件失败!";
                    }
                }
                else
                {
                    result.Msg = "发布文件大于50M，请通过其他渠道上传!";
                }
            }
            else
            {
                result.Msg = "请填写发布文件存放目录!";
            }
            return result;
        }
    }
}
