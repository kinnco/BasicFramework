﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 流程步骤基础操作信息
    /// </summary>
    public class FlowStepInfoOperation
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 步骤名称
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 上一个步骤ID
        /// </summary>
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 流程ID
        /// </summary>
        public string flow_id
        {
            get; set;
        }

        /// <summary>
        /// 流程名称
        /// </summary>
        public string flow_name
        {
            get; set;
        }

        /// <summary>
        /// 类型 100起始节点 110过程节点 120结束节点
        /// </summary>
        public int type
        {
            get; set;
        }

        /// <summary>
        /// 是否可以选择 100 可选择 110不可选
        /// </summary>
        public int is_choosable
        {
            get; set;
        }

        /// <summary>
        /// 可操作人员
        /// </summary>
        public string choose_person
        {
            get; set;
        }

        /// <summary>
        /// 可操作人员名称
        /// </summary>
        public string person_name
        {
            get; set;
        }

        /// <summary>
        /// 实际审批/操作人员
        /// </summary>
        public string examine_person
        {
            get; set;
        }

        /// <summary>
        /// 实际审批/操作人员名称
        /// </summary>
        public string examine_name
        {
            get; set;
        }

        /// <summary>
        /// 处理者类型 100所有人员 110选择人员 120发起者部门领导 130发起者上级部门领导  140发起者所有上级部门领导 
        /// </summary>
        public int handler_type
        {
            get; set;
        }

        /// <summary>
        /// 默认处理者[只有一个人]
        /// </summary>
        public string default_handler
        {
            get; set;
        }

        /// <summary>
        /// 默认处理者名称
        /// </summary>
        public string handler_name
        {
            get; set;
        }

        /// <summary>
        /// 处理策略 100所有人必须同意  110一人同意即可 120依据人数比例
        /// </summary>
        public int processing_strategy
        {
            get; set;
        }

        /// <summary>
        /// 处理策略百分比
        /// </summary>
        public double processing_percent
        {
            get; set;
        }


        /// <summary>
        /// 会签策略 100不会签 110所有步骤同意 120一个步骤同意即可 130依据比例
        /// </summary>
        public int sign_strategy
        {
            get; set;
        }

        /// <summary>
        /// 会签比例
        /// </summary>
        public double sign_percent
        {
            get; set;
        }

        /// <summary>
        /// 退回类型100退回上一步 110退回第一步 120退回指定步骤
        /// </summary>
        public int return_type
        {
            get; set;
        }

        /// <summary>
        /// 退回至步骤ID
        /// </summary>
        public string return_step
        {
            get; set;
        }

        /// <summary>
        /// 步骤按钮
        /// </summary>
        public string button_ids
        {
            get; set;
        }

        /// <summary>
        /// 步骤按钮
        /// </summary>
        public string button_names
        {
            get; set;
        }

        /// <summary>
        /// 步骤按钮json
        /// </summary>
        public string button_json
        {
            get; set;
        }
    }
}
