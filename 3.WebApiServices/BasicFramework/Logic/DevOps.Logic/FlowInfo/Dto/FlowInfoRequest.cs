﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  DevOps.Logic
*
* 功 能：流程编辑信息
*
* 类 名： FlowInfoRequest
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/1/15 9:41:25               Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 流程编辑信息
    /// </summary>
    public class FlowInfoRequest
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 名称
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 分类
        /// </summary>
        public string flow_category
        {
            get; set;
        }

        /// <summary>
        /// 管理人员
        /// </summary>
        public string manager
        {
            get; set;
        }

        /// <summary>
        /// 管理人员名称
        /// </summary>
        public string managername
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 表单关键字
        /// </summary>
        public string form_key
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        public float sort
        {
            get; set;
        }

    }
}
