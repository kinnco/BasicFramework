﻿using Common.Library;
using Common.Model;
using System.Collections.Generic;

namespace DevOps.Logic
{
    /// <summary>
    /// 角色逻辑操作接口类
    /// </summary>
    public interface ISysRoleService
    {
        #region 角色基础信息管理操作

        #region 查询

        /// <summary>
        /// 根据条件分页查询角色数据
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        ResultJsonInfo<List<RoleInfoResponse>> LoadList(ParametersInfo<RoleInfoQuery> parameters);
        /// <summary>
        /// 查询单个角色的数据
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        ResultJsonInfo<RoleInfoResponse> LoadSingle(string roleId);

        #endregion

        #region 添加
        /// <summary>
        /// 新增角色
        /// </summary>
        /// <param name="roleAdd"></param>
        /// <returns></returns>
        ResultJsonInfo<int> AddRole(RoleInfoAddRequest roleAdd);



        #endregion

        #region 修改
        /// <summary>
        /// 修改角色数据
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Modify(RoleInfoModifyRequest request);

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="roleIds"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> roleIds);


        /// <summary>
        /// 禁用/启用角色
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ForbidOrEnable(string roleId);

        #endregion

        #endregion

        #region 权限相关操作

        /// <summary>
        /// 获取所有角色Select数据
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<SelectListInfo>> LoadAllSelectList();

        #endregion

        #region 批量导入导出操作
        /// <summary>
        /// 事务批量导入角色信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> TranBulkImportRole(List<RoleInfoAddRequest> selects);

        /// <summary>
        /// 非事务批量导入角色信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> BulkImportRole(List<RoleInfoAddRequest> selects);

        /// <summary>
        /// 批量导出所有
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<RoleInfoResponse>> ListAll();
        #endregion
    }
}
