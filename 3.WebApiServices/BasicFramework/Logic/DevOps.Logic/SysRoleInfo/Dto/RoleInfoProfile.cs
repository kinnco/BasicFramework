﻿
using DevOps.Logic;
using AutoMapper;
using Common.Library;
using DevOps.Model;

namespace DevOps.Logic
{
    /// <summary>
    /// 权限相关映射
    /// </summary>
    public class RoleInfoProfile : Profile
    {
        /// <summary>
        /// 权限数据实体与传输实体映射
        /// </summary>
        public RoleInfoProfile()
        {
            CreateMap<RoleInfoAddRequest,SysRoleInfoEntity>();
            CreateMap<RoleInfoModifyRequest, SysRoleInfoEntity>();


            CreateMap<SysRoleInfoEntity,RoleInfoResponse >();

            CreateMap<SysRoleInfoEntity, SelectListInfo>()
                .ForMember(p => p.value, opt => { opt.MapFrom(t => t.id); })
                .ForMember(p => p.name, opt => { opt.MapFrom(t => t.name); });

        }
    }
}
