﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 角色查询返回类
    /// </summary>
    public class RoleInfoResponse
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 角色名称
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 用户组描述
        /// </summary>
        public string describe
        {
            get; set;
        }


        /// <summary>
        /// 是否有效
        /// </summary>
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否被逻辑删除
        /// </summary>
        public bool is_deleted
        {
            get; set;
        }
        /// <summary>
        /// 是否启用
        /// </summary>
        public string ifis_valid
        {
            get; set;
        }
    }
}
