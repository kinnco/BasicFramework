﻿using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 岗位修改操作
    /// </summary>
    public  class PostModifyRequest
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 32, Description = "岗位Id")]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 名称
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 100, Description = "名称")]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        [Validate(ValidateType.NotEmpty, Regex = ValidateRegex.Number, Description = "排序字段")]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 简码，CEO,CTO
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 100, Description = "简码，CEO,CTO")]
        public string brevity_code
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 200, Description = "备注")]
        public string remarks
        {
            get; set;
        }
    }
}
