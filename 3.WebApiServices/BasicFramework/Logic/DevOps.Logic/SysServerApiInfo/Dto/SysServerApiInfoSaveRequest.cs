﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

/*
* 命名空间:  DevOps.Logic
*
* 功 能：接口服务相关逻辑保存请求实体
*
* 类 名： SysServerApiInfoSaveRequest
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/12/30 15:14:04               Harvey      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 接口服务相关逻辑保存请求实体
    /// </summary>
    public class SysServerApiInfoSaveRequest
    {

        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 接口服务名称
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "接口服务名称")]
        public string server_name
        {
            get; set;
        }

        /// <summary>
        /// swagger地址
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 100, Description = "swagger地址")]
        public string swagger_url
        {
            get; set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime create_time
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }
    }
}
