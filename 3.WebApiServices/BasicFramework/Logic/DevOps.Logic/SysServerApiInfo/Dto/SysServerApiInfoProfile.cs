﻿using AutoMapper;
using DevOps.Model;

/*
* 命名空间:  DevOps.Logic.SysServerApiInfo.Dto
*
* 功 能：接口服务相关映射操作类
*
* 类 名： SysServerApiInfoProfile
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/12/30 14:59:26               Harvey      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 接口服务相关映射操作类
    /// </summary>
    public class SysServerApiInfoProfile: Profile
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SysServerApiInfoProfile()
        {
            //返回信息映射
            CreateMap<SysServerApiInfoEntity, SysServerApiInfoResponse>();

            //请求信息映射
            CreateMap<SysServerApiInfoSaveRequest, SysServerApiInfoEntity>();

        }
    }
}
