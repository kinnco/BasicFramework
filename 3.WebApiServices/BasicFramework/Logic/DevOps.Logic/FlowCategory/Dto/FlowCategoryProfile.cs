﻿using AutoMapper;
using DevOps.Model;
using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  DevOps.Logic
*
* 功 能：XXX
*
* 类 名： Profile
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/1/14 9:39:35               Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 流程类型映射帮助类
    /// </summary>
    public class FlowCategoryProfile : Profile
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowCategoryProfile()
        {

        }
    }
}
