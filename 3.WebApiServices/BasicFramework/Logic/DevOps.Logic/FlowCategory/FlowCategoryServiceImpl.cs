﻿using Common.Library;
using Common.Model;
using Container.Library;
using Dapper.Library;
using DevOps.Model;
using Serialize.Library;
using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  DevOps.Logic
*
* 功 能：流程类型信息管理模块
*
* 类 名： FlowCategoryServiceImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/1/14 9:34:35               Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 流程类型信息管理模块
    /// </summary>
    public class FlowCategoryServiceImpl : OperationLogicImpl, IFlowCategoryService
    {
        #region 类型信息管理模块

        #region 查询

        /// <summary>
        /// 根据关键字 分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<FlowCategoryInfoEntity>> LoadPageList(ParametersInfo<string> inputInfo)
        {

            var resultInfo = new ResultJsonInfo<List<FlowCategoryInfoEntity>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<FlowCategoryInfoEntity>();

                if (inputInfo.parameters.IsNotNullOrEmpty())
                {
                    result.Where(p => p.name.Contains(inputInfo.parameters) || p.code.Contains(inputInfo.parameters));
                }
                var queryInfo = result.OrderBy(p => p.sort).PageList(inputInfo.page, inputInfo.limit);

                if (queryInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = queryInfo.Items;
                    resultInfo.Count = queryInfo.Total;
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "无对应值存在！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        ///获取所有列表
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<FlowCategoryInfoEntity>> LoadList()
        {

            var resultInfo = new ResultJsonInfo<List<FlowCategoryInfoEntity>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<FlowCategoryInfoEntity>().OrderBy(a => a.sort).ToList();
                if (result.Count>0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = result;
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }

        #endregion

        #region 更新

        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<int> Save(FlowCategoryInfoEntity inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            var user = GetLoginUserInfo();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {

                con.Transaction(tran =>
                {
                    //修改
                    if (inputInfo.id.IsNotNullOrEmpty())
                    {
                        #region 修改

                        var info = tran.QuerySet<FlowCategoryInfoEntity>().Where(p => p.id.Equals(inputInfo.id)).Get();
                        if (info != null)
                        {
                            info.name = inputInfo.name;
                            info.code = inputInfo.code;
                            info.sort = inputInfo.sort;
                            info.remark = inputInfo.remark;
                            info.parent_id = inputInfo.parent_id;
                            var result = tran.CommandSet<FlowCategoryInfoEntity>().Update(info);
                            if (result > 0)
                            {

                                resultInfo.Code = ActionCodes.Success;
                                resultInfo.Msg = "修改成功！";

                                AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.TypeManag, $"修改类型信息，修改信息：{JsonHelper.ToJson(inputInfo)}");
                            }
                            else
                            {
                                resultInfo.Code = ActionCodes.InvalidOperation;
                                resultInfo.Msg = "修改失败！";
                            }
                        }
                        else
                        {
                            resultInfo.Code = ActionCodes.InvalidOperation;
                            resultInfo.Msg = "Id无对应信息！";
                        }
                        #endregion
                    }
                    else
                    {
                        #region 新增
                        inputInfo.id = GuidHelper.GetGuid();
                        var result = tran.CommandSet<FlowCategoryInfoEntity>().Insert(inputInfo);

                        if (result > 0)
                        {


                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "添加成功！";

                            AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.TypeManag, $"新增类型信息，新增信息：{JsonHelper.ToJson(inputInfo)}");
                        }
                        else
                        {
                            resultInfo.Code = ActionCodes.InvalidOperation;
                            resultInfo.Msg = "修改失败！";
                        }
                        #endregion
                    }
                });
            }
            return resultInfo;
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                //删除
                var result = con.CommandSet<FlowCategoryInfoEntity>().Where(p => p.id.In(ids.ToArray())).Delete();
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "删除成功！";
                    AddOperationLog(OperationLogType.RemoveOperation, BusinessTitleType.TypeManag, $"删除类型信息!{JsonHelper.ToJson(ids)}");
                }
                else
                {
                    resultInfo.Msg = "删除失败！";
                }
            }
            return resultInfo;
        }
        #endregion

        #endregion
    }
}
