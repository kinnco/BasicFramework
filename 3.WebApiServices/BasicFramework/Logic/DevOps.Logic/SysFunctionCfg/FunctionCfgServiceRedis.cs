﻿using Common.Model;
using DevOps.Model;
using Redis.Library;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DevOps.Logic
{
    /// <summary>
    /// 菜单功能管理Redis操作逻辑类
    /// </summary>
    internal static class FunctionCfgServiceRedis
    {

        #region 操作用户的菜单信息

        /// <summary>
        /// 将对应用户可操作性菜单保存redis
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SaveFunctionCfgOfUser(string key, List<SysFunctionCfgEntity> value)
        {
             RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWebOfPermission).HashSet(RedisAuthorityInfo.RedisMenusCode,key, value);
        }

        /// <summary>
        /// 获取用户可操作性菜单信息
        /// </summary>
        /// <param name="key"></param>
        /// <param name="successAction"></param>
        /// <param name="failAction"></param>
        public static void GetFunctionCfgOfUser(string key,Action<List<SysFunctionCfgEntity>> successAction = null, Action failAction = null)
        {
            var resultInfo = RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWebOfPermission).HashGet<List<SysFunctionCfgEntity>>(RedisAuthorityInfo.RedisMenusCode, key);
            if (resultInfo!=null)
            {
                successAction?.Invoke(resultInfo);
            }
            else
            {
                failAction?.Invoke();
            }
        }

        /// <summary>
        /// 将对应用户可操作性菜单保存redis
        /// </summary>
        public static void RemoveFunctionCfg()
        {
            var keys = RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWebOfPermission).HashGetAllKeys(RedisAuthorityInfo.RedisMenusCode);
            if (keys != null&& keys.Count>0)
            {
                RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWebOfPermission).HashDeletes(RedisAuthorityInfo.RedisMenusCode, keys);
            }
        }
        #endregion
    }
}
