﻿using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 菜单移动操作传参
    /// </summary>
    public class FunctionCfgMoveRequest
    {
        /// <summary>
        /// 操作类型 上/下移（1：上移/2：下移）
        /// </summary>
        [Validate(ValidateType.NotEmpty, Description = "操作类型")]
        public int flag { get; set; }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [Validate(ValidateType.NotEmpty, Description = "唯一标识符")]
        public string id { get; set; }
    }
}
