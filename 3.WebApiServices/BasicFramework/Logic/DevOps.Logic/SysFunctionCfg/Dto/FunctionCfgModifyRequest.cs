﻿using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 应用修改传输实体类
    /// </summary>
    public class FunctionCfgModifyRequest
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "唯一标识符")]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 应用Id【不可空】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 1000, Description = "应用Id")]
        public string application_ids
        {
            get; set;
        }

        /// <summary>
        /// 应用名称【不可空】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 1000, Description = "应用名称")]
        public string application_name
        {
            get; set;
        }

        /// <summary>
        /// 功能类型【不可空，最大长度50】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "功能类型")]
        public string function_type
        {
            get; set;
        }

        /// <summary>
        /// 菜单名称【不可空，最大长度25】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 25, Description = "菜单名称")]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 菜单标签【不可空，最大长度50】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "菜单标签")]
        public string icon_font
        {
            get; set;
        }

        /// <summary>
        /// 菜单链接【可空】
        /// </summary>
        public string link_url
        {
            get; set;
        }

        /// <summary>
        /// 是否启用
        /// </summary>
        [Validate(ValidateType.NotEmpty, Description = "是否启用")]
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 备注【可空】
        /// </summary>
        public string describe
        {
            get; set;
        }

    }
}
