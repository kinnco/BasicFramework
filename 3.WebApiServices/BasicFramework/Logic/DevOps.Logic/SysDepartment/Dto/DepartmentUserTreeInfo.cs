﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 部门用户综合树
    /// </summary>
    public class DepartmentUserTreeInfo
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 唯一标识符[guid]
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string label { get; set; }

        /// <summary>
        /// 父级id
        /// </summary>
        public int pid { get; set; }

        /// <summary>
        ///节点是否为禁用状态。默认 false
        /// </summary>
        public bool disabled { get; set; }

        /// <summary>
        ///类型100是部门  110是人员
        /// </summary>
        public int type { get; set; }

        /// <summary>
        /// 子节点
        /// </summary>
        public List<DepartmentUserTreeInfo> children { get; set; } = new List<DepartmentUserTreeInfo>();
    }
}
