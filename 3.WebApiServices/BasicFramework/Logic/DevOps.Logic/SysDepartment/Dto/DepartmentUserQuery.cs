﻿using Validate.Library;

namespace DevOps.Logic
{
  
    /// <summary>
    /// 部门返回信息
    /// </summary>
    public class DepartmentUserQuery
    {

        /// <summary>
        ///部门唯一标识符【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "角色唯一标识符")]
        public string departmentid
        {
            get; set;
        }
        /// <summary>
        /// 关键字
        /// </summary>
        public string keyword
        {
            get; set;
        }
    }
}
