﻿using AutoMapper;
using DevOps.Model;

namespace DevOps.Logic
{
    /// <summary>
    /// 权限相关映射
    /// </summary>
    public class DepartmentProfile : Profile
    {
        /// <summary>
        /// 权限数据实体与传输实体映射
        /// </summary>
        public DepartmentProfile()
        {

            CreateMap<SysDepartmentInfoEntity, DepartmentResponse>();
            CreateMap<SysDepartmentInfoEntity, DepartmentTreeResponse>()
               .ForMember(p => p.id, opt => opt.MapFrom(x => 0))
               .ForMember(p => p.key, opt => opt.MapFrom(x => x.id)); ;

            CreateMap<SysUserInfoEntity,DepartmentUserResponse>();

            CreateMap<DepartmentRootAddRequest, SysDepartmentInfoEntity>();

            CreateMap<DepartmentChildAddRequest, SysDepartmentInfoEntity>();

            CreateMap<DepartmentModifyRequest, SysDepartmentInfoEntity>();

        }
    }
}
