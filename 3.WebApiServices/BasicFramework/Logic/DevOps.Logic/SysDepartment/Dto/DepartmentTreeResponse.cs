﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 部门树状菜单返回信息[id变成数字，以前的id变成key]
    /// </summary>
    public class DepartmentTreeResponse
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public int id
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string key
        {
            get; set;
        }

        /// <summary>
        /// 父节点唯一标识符
        /// </summary>
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 部门名称
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 领导人名称
        /// </summary>
        public string leader
        {
            get; set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string phone
        {
            get; set;
        }

        /// <summary>
        /// 邮件地址
        /// </summary>
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 创建者
        /// </summary>
        public string creator_id
        {
            get; set;
        }

        /// <summary>
        /// 创建者姓名
        /// </summary>
        public string creator_name
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 修改者
        /// </summary>
        public string modifier_id
        {
            get; set;
        }

        /// <summary>
        /// 修改者姓名
        /// </summary>
        public string modifier_name
        {
            get; set;
        }

        /// <summary>
        /// 修改者日期
        /// </summary>
        public DateTime modifier_date
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        public bool is_valid
        {
            get; set;
        }
    }
}
