﻿using AutoMapper;
using DevOps.Model;

namespace DevOps.Logic
{
    /// <summary>
    /// 权限相关映射
    /// </summary>
    public class ApiMonitorLogProfile : Profile
    {
        /// <summary>
        /// 数据实体与传输实体映射
        /// </summary>
        public ApiMonitorLogProfile()
        {
            CreateMap<SafeApiMonitorLogEntity, ApiMonitorLogResponse>();

            CreateMap<SafeApiMonitorLogEntity, SafeApiInfoEntity>();

        }
    }
}
