﻿/*
* 命名空间: DevOps.Logic
*
* 功 能： 接口访问次数实体
*
* 类 名： ApiMonitorNumberResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/5/15 11:14:12 	Harvey    创建
* ─────────────────────────────────────────────────
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Logic
{
    /// <summary>
    /// 接口访问次数实体
    /// </summary>
    public class ApiMonitorNumberResponse
    {


        /// <summary>
        /// 服务名称
        /// </summary>
        public string service_name
        {
            get; set;
        }

        /// <summary>
        /// 控制器名称
        /// </summary>
        public string controller_name
        {
            get; set;
        }

        /// <summary>
        /// Action名称
        /// </summary>
        public string action_name
        {
            get; set;
        }

        /// <summary>
        /// action参数
        /// </summary>
        public string action_params
        {
            get; set;
        }

        /// <summary>
        /// 请求路径
        /// </summary>
        public string request_path
        {
            get; set;
        }

        /// <summary>
        /// 请求路径
        /// </summary>
        public string http_method
        {
            get; set;
        }

        /// <summary>
        /// Http请求总的次数
        /// </summary>
        public long request_number
        {
            get; set;
        }

    }
}
