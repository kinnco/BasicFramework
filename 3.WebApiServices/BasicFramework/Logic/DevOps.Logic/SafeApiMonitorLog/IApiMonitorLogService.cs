﻿using Common.Model;
using System;
using System.Collections.Generic;
/*
* 命名空间: DevOps.Logic
*
* 功 能： 接口访问监控日志Redis操作类接口
*
* 类 名： IApiMonitorLogService
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 接口访问监控日志Redis操作类接口
    /// </summary>
    public interface IApiMonitorLogService
    {

        #region 接口访问监控日志明细操作

        /// <summary>
        /// 根据访问监控日志条件分页获取列表 关键字【控制器名称】【方法名】  开始时间
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<ApiMonitorLogResponse>> LoadApiMonitorLogList(ParametersInfo<ApiMonitorLogQuery> inputInfo);

        /// <summary>
        /// 删除记录信息
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(ApiMonitorLogRequest request);

        #endregion

        #region 接口访问次数操作

        /// <summary>
        /// 根据条件分页获取接口访问次数列表 关键字【请求路由名称】
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<ApiMonitorNumberResponse>> LoadApiMonitorNumberList(ParametersInfo<string> inputInfo);

        #endregion

        #region 接口访问监控日志统计操作

        /// <summary>
        /// 统计接口请求信息
        /// </summary>
        /// <param name="seconds">向后偏移秒数</param>
        void StatisticsInterfaceRequest(int seconds);

        /// <summary>
        /// 获取接口实时访问统计信息
        /// </summary>
        /// <param name="timed"></param>
        /// <returns></returns>
        ResultJsonInfo<Echarts2DInfo<string, string>> loadRequestInfo(DateTime timed);

        /// <summary>
        /// 获取某个时间段内的接口实时访问统计信息
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        ResultJsonInfo<Echarts2DInfo<string, string>> loadRequestInfoByTime(DateTime beginTime, DateTime endTime);

        /// <summary>
        /// 获取高频访问接口信息
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        ResultJsonInfo<Echarts2DInfo<Dictionary<string, object>,string>> LoadHighFrequencyInterface(DateTime beginTime, DateTime endTime);

        /// <summary>
        /// 获取接口预警信息
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<ApiMonitorLogPathIpGroup>> LoadInterfaceWarning(ParametersInfo<string> inputInfo);

        /// <summary>
        /// 获取黑名单信息[警告名单+黑名单]
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<ApiMonitorLogNameList>> LoadBlacklist(ParametersInfo<string> inputInfo);

        /// <summary>
        /// 加入黑名单
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        ResultJsonInfo<bool> AddBalckList(string ip);

        /// <summary>
        /// 删除黑名单失败
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        ResultJsonInfo<bool> RemoveBalckList(string ip);

        /// <summary>
        /// 获取接口预警和黑名单个数信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<ApiMonitorLogCount> LoadApiMonitorCount();

        #endregion
    }
}
