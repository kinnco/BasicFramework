﻿using System;
using System.Collections.Generic;
using System.Text;
/*
* 命名空间: DevOps.Logic
*
* 功 能： 消息查询传输实体
*
* 类 名： MessageQueryRequest
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/8 13:06:59 				Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 消息查询传输实体
    /// </summary>
    public class MessageQuery
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string keyWord
        {
            get; set;
        }

        /// <summary>
        /// 信息类型 100：对话信息 101：系统消息:
        /// </summary>
        public int msg_type
        {
            get; set;
        }
    }
}
