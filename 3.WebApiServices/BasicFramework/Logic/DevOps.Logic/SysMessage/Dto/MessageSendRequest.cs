﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;
/*
* 命名空间: DevOps.Logic
*
* 功 能： 消息发送请求传输实体
*
* 类 名： MessageSendRequest
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/8 15:55:19 	Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 消息发送请求传输实体
    /// </summary>
    public class MessageSendRequest
    {
        ///// <summary>
        ///// 接收对象类型 100：所有用户  101：列表用户 102：选中用户
        ///// </summary>
        //public int receiver_type { get; set; }

        /// <summary>
        /// 接收用户id，如果没有，就是除自己的所有用户
        /// </summary>
        public List<string> user_ids { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "标题")]
        public string title { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 200, Description = "内容")]
        public string content { get; set; }

    }
}
