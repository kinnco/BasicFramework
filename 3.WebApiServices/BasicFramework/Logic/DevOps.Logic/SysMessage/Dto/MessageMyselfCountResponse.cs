﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间: DevOps.Logic
*
* 功 能： 总的消息，未读消息数量返回传输实体信息
*
* 类 名： MessageMyselfCountResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/9 9:52:24 	Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 总的消息，未读消息数量返回传输实体信息
    /// </summary>
    public class MessageMyselfCountResponse
    {

        /// <summary>
        /// 总的消息数量
        /// </summary>
        public int total
        {
            get; set;
        }

        /// <summary>
        /// 未读消息数量
        /// </summary>
        public int unread_count
        {
            get; set;
        }

    }
}
