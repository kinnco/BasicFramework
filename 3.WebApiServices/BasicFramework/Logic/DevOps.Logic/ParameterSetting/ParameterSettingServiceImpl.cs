﻿/*
* 命名空间: DevOps.Logic
*
* 功 能： 参数管理逻辑
*
* 类 名： ParameterSettingServiceImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/06/15 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
using System.Collections.Generic;
using Common.Model;
using Common.Library;
using Container.Library;
using System;
using Dapper.Library;
using Serialize.Library;
using DevOps.Model;
using System.Linq;

namespace DevOps.Logic
{
    /// <summary>
    /// 参数管理逻辑
    /// </summary>
    public class ParameterSettingServiceImpl : OperationLogicImpl, IParameterSettingService
    {
        #region 基础信息管理模块

        #region 查询

        /// <summary>
        /// 根据关键字 分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<SysParameterSettingEntity>> LoadPageList(ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<SysParameterSettingEntity>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<SysParameterSettingEntity>();
                if (inputInfo.parameters.IsNotNullOrEmpty())
                {
                    result.Where(a => a.name.Contains(inputInfo.parameters) || a.parameters_key.Contains(inputInfo.parameters));
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                var listInfo = result.PageList(inputInfo.page, inputInfo.limit);

                if (listInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.Items;
                    resultInfo.Count = listInfo.Total;
                }
                else
                {
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 根据参数键名获取参数信息
        /// </summary>
        /// <param name="parametersKey"></param>
        /// <returns></returns>
        public ResultJsonInfo<SysParameterSettingEntity> LoadInfoByKey(string parametersKey)
        {
            var resultInfo = new ResultJsonInfo<SysParameterSettingEntity>();

            ParameterSettingServiceRedis.GetParameterSetting((result) =>
            {

                SysParameterSettingEntity ParameterSettingInfo = new SysParameterSettingEntity();

                if (result != null)
                {
                    ParameterSettingInfo = result.Find(p => p.parameters_key.Equals(parametersKey));

                    if (ParameterSettingInfo != null)
                    {
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Data = ParameterSettingInfo;
                        resultInfo.Msg = "获取成功！";
                    }
                    else
                    {
                        resultInfo.Msg = "无对应信息！";
                    }
                }
                else
                {
                    using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
                    {
                        var ParameterSettingResult = con.QuerySet<SysParameterSettingEntity>().ToList();

                        //将参数信息，全部存入redis
                        ParameterSettingServiceRedis.SaveAllParameterSetting(ParameterSettingResult.Select(p => p.parameters_key).ToList(), ParameterSettingResult);

                        ParameterSettingInfo = ParameterSettingResult.Find(p => p.parameters_key.Equals(parametersKey));

                        if (ParameterSettingInfo != null)
                        {
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Data = ParameterSettingInfo;
                            resultInfo.Msg = "获取成功！";
                        }
                        else
                        {
                            resultInfo.Msg = "无对应信息！";
                        }
                    }
                }
            });
            return resultInfo;
        }

        #endregion

        #region 更新

        /// <summary>
        /// 更新
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<int> Modify(ParameterSettingOptRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = 0;
                if (inputInfo.id.IsNotNullOrEmpty())
                {
                    #region Modify

                    //判断是否 有键值重复
                    var infoCount = con.QuerySet<SysParameterSettingEntity>().Where(p => p.id != inputInfo.id && p.parameters_key.Equals(inputInfo.parameters_key)).Count();
                    if (infoCount == 0)
                    {
                        var modifyInfo = con.QuerySet<SysParameterSettingEntity>().Where(p => p.id == inputInfo.id).Get();
                        if (modifyInfo != null)
                        {
                            modifyInfo.name = inputInfo.name;
                            modifyInfo.parameters_key = inputInfo.parameters_key;
                            modifyInfo.parameters_value = inputInfo.parameters_value;
                            modifyInfo.remarks = inputInfo.remarks;
                            modifyInfo.sort = inputInfo.sort;
                            result = con.CommandSet<SysParameterSettingEntity>().Update(modifyInfo);

                            if (result > 0)
                            {
                                //保存Redis
                                ParameterSettingServiceRedis.SaveParameterSetting(modifyInfo);

                                resultInfo.Code = ActionCodes.Success;
                                resultInfo.Msg = "修改成功！";
                                AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.ParameterSettingManage, $"修改参数设置信息，修改信息：{JsonHelper.ToJson(inputInfo)}");
                            }
                            else
                            {
                                resultInfo.Code = ActionCodes.InvalidOperation;
                                resultInfo.Msg = "修改失败！";
                            }
                        }
                        else
                        {
                            resultInfo.Code = ActionCodes.InvalidOperation;
                            resultInfo.Msg = "对应Id的信息不存在！";
                        }
                    }
                    else
                    {
                        resultInfo.Code = ActionCodes.InvalidOperation;
                        resultInfo.Msg = "参数键名已存在！";
                    }
                    #endregion
                }
                else
                {
                    var sysParameterSetting = inputInfo.MapTo<SysParameterSettingEntity>();
                    sysParameterSetting.create_date = DateTime.Now;
                    sysParameterSetting.id = GuidHelper.GetGuid();

                    var infoCount = con.QuerySet<SysParameterSettingEntity>().Where(p => p.parameters_key.Equals(inputInfo.parameters_key)).Count();
                    if (infoCount == 0)
                    {
                        result = con.CommandSet<SysParameterSettingEntity>()
                              .Insert(sysParameterSetting);

                        if (result > 0)
                        {
                            //保存Redis
                            ParameterSettingServiceRedis.SaveParameterSetting(sysParameterSetting);

                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "添加成功！";
                            AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.ParameterSettingManage, $"添加了一个参数，新增信息：{JsonHelper.ToJson(inputInfo)}");
                        }
                        else
                        {
                            resultInfo.Code = ActionCodes.InvalidOperation;
                            resultInfo.Msg = "添加失败！";
                        }
                    }
                    else
                    {
                        resultInfo.Code = ActionCodes.InvalidOperation;
                        resultInfo.Msg = "参数键名已存在！";
                    }
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 删除参数设置
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<int> Delete(List<string> ids)
        {

            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                //删除
                var result = con.CommandSet<SysParameterSettingEntity>().Where(p => p.id.In(ids.ToArray())).Delete();
                if (result > 0)
                {   
                    //保存Redis
                    ParameterSettingServiceRedis.RemoveParameterSetting(ids);

                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "删除成功！";
                    AddOperationLog(OperationLogType.RemoveOperation, BusinessTitleType.ParameterSettingManage, $"物理删除参数设置成功，物理删除参数设置信息：{JsonHelper.ToJson(ids)}");
                }
                else
                {
                    resultInfo.Msg = "删除失败！";
                }
            }
            return resultInfo;
        }

        #endregion

        #endregion
    }
}
