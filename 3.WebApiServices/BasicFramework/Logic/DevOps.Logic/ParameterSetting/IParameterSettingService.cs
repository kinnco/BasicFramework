﻿using System.Collections.Generic;
using Common.Model;
using DevOps.Model;

/*
* 命名空间: DevOps.Logic
*
* 功 能： 参数管理逻辑接口
*
* 类 名： IParameterSettingService
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/06/15 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 参数管理逻辑接口
    /// </summary>
    public interface IParameterSettingService
    {
        #region 基础信息管理模块

        #region 查询

        /// <summary>
        /// 根据关键字 分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<SysParameterSettingEntity>> LoadPageList(ParametersInfo<string> inputInfo);


        /// <summary>
        /// 根据参数键名获取参数信息
        /// </summary>
        /// <param name="parametersKey"></param>
        /// <returns></returns>
        ResultJsonInfo<SysParameterSettingEntity> LoadInfoByKey(string parametersKey);

        #endregion

        #region 更新

        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> Modify(ParameterSettingOptRequest inputInfo);

        /// <summary>
        /// 删除岗位
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> Delete(List<string> postIds);

        #endregion

        #endregion
    }
}
