﻿using Common.Library;
using Common.Model;
using Redis.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


/*
* 命名空间:  DevOps.Logic
*
* 功 能：考勤管理Redis操作
*
* 类 名：AttendanceServiceRedis
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/8 17:08:10       LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 考勤管理Redis操作
    /// </summary>
    internal static class AttendanceServiceRedis
    {

        /// <summary>
        /// 获取钉钉access_token
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="successAction">成功执行</param>
        /// <param name="failAction">失败执行</param>
        /// <returns></returns>
        public static void GetDindinAccessToken(string key, Action<string> successAction, Action failAction = null)
        {
            try
            {
                var resultInfo = RedisClient.GetRedisDb(RedisCommonInfo.RedisCommonDbLog).StringGet(RedisCommonInfo.RedisAttendance + key);
                successAction?.Invoke(resultInfo);
            }
            catch
            {
                failAction?.Invoke();
            }
        }

        /// <summary>
        /// 保存钉钉access_token
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        /// <param name="successAction">成功执行</param>
        /// <param name="failAction">失败执行</param>
        /// <returns></returns>
        public static void SaveDindinAccessToken(string key,string value, Action successAction = null, Action failAction = null)
        {
            bool result = RedisClient.GetRedisDb(RedisCommonInfo.RedisCommonDbLog).StringSet(RedisCommonInfo.RedisAttendance, value, RedisCommonInfo.AccessTokenTimeOut);
            if (result)
            {
                successAction?.Invoke();
            }
            else
            {
                failAction?.Invoke();
            }
        }

        /// <summary>
        /// 保存钉钉员工信息到Redis
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="successAction">成功执行</param>
        /// <param name="failAction">失败执行</param>
        /// <returns></returns>
        public static void SaveEmployeeInfo(List<DingEmployeeInfoResponse> value, Action successAction = null, Action failAction = null)
        {
            var ids = value.Select(p=>p.userid).ToList();
            bool result = RedisClient.GetRedisDb(RedisCommonInfo.RedisCommonDbLog).HashSets(RedisCommonInfo.RedisDingEmployees,ids,value);
            if (result)
            {
                successAction?.Invoke();
            }
            else
            {
                failAction?.Invoke();
            }
        }

        /// <summary>
        /// 获取钉钉员工信息
        /// </summary>
        /// <param name="successAction">成功执行</param>
        /// <param name="failAction">失败执行</param>
        /// <returns></returns>
        public static void GetEmployeeInfo(Action<List<DingEmployeeInfoResponse>> successAction = null, Action failAction = null)
        {
            var resultInfo = RedisClient.GetRedisDb(RedisCommonInfo.RedisCommonDbLog).HashGetAll<DingEmployeeInfoResponse>(RedisCommonInfo.RedisDingEmployees);
            if (resultInfo!=null&& resultInfo.Count>0)
            {
                successAction?.Invoke(resultInfo);
            }
            else
            {
                failAction?.Invoke();
            }
        }


        /// <summary>
        /// 保存钉钉员工补卡申请通过信息信息到Redis
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="successAction">成功执行</param>
        /// <param name="failAction">失败执行</param>
        /// <returns></returns>
        public static void SaveEmployeeFillCardApplyInfo(ProcessInstanceTopVo value, Action successAction = null, Action failAction = null)
        {
            bool result = RedisClient.GetRedisDb(RedisCommonInfo.RedisCommonDbLog).HashSet(RedisCommonInfo.RedisDingEmployeeFillCardApply, value.process_instance_id, value);
            if (result)
            {
                successAction?.Invoke();
            }
            else
            {
                failAction?.Invoke();
            }
        }


        /// <summary>
        /// 获取钉钉员工补卡申请通过信息信息到Redis
        /// </summary>
        /// <param name="successAction">成功执行</param>
        /// <param name="failAction">失败执行</param>
        /// <returns></returns>
        public static void GetEmployeeFillCardApplyInfo(Action<List<ProcessInstanceTopVo>> successAction = null, Action failAction = null)
        {

            var resultInfo = RedisClient.GetRedisDb(RedisCommonInfo.RedisCommonDbLog).HashGetAll<ProcessInstanceTopVo>(RedisCommonInfo.RedisDingEmployeeFillCardApply);
            if (resultInfo != null && resultInfo.Count > 0)
            {
                successAction?.Invoke(resultInfo);
            }
            else
            {
                failAction?.Invoke();
            }
        }

    }
}
