﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 添加用户数据类
    /// </summary>
    public class UserInfoAddRequest
    {
        /// <summary>
        /// 用户名【登录名】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "用户名【登录名】")]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 用户描述
        /// </summary>
        public string user_describe
        {
            get; set;
        }

        /// <summary>
        /// 用户家庭电话
        /// </summary>
        public string telphone
        {
            get; set;
        }

        /// <summary>
        /// 用户手机号码
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "用户手机号码")]
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// 性别
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "性别")]
        public string gender
        {
            get; set;
        }

        /// <summary>
        /// 密码
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "密码")]
        public string password
        {
            get; set;
        }

        /// <summary>
        /// 重复密码
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "重复密码")]
        public string repassword
        {
            get; set;
        }


        /// <summary>
        /// QQ号
        /// </summary>
        public string qq
        {
            get; set;
        }

        /// <summary>
        /// 用户昵称
        /// </summary>
        public string nickname
        {
            get; set;
        }

        /// <summary>
        /// 用户真实名
        /// </summary>
        public string realname
        {
            get; set;
        }

        /// <summary>
        /// 用户邮箱
        /// </summary>
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 办公电话
        /// </summary>
        public string office_phone
        {
            get; set;
        }

        /// <summary>
        /// 用户身份号码
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "用户身份号码")]
        public string idnumber
        {
            get; set;
        }

        /// <summary>
        /// 用户头像地址
        /// </summary>
        //[Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 255, Description = "用户头像地址")]
        public string picture_url
        {
            get; set;
        }

        /// <summary>
        /// 部门id组[使用“,”符号，将之分开]
        /// </summary>
        //[Validate(ValidateType.NotEmpty)]
        public List<string> departmentList
        {
            get; set;
        }
        /// <summary>
        /// 岗位id组[使用“,” 符号，将之分开]
        /// </summary>
        //[Validate(ValidateType.NotEmpty)]
        public List<string> postList
        {
            get; set;
        }
        /// <summary>
        /// 角色id组[使用“,” 符号，将之分开]
        /// </summary>
        //[Validate(ValidateType.NotEmpty)]
        public List<string> roleList
        {
            get; set;
        }
    }
}
