﻿/*
* 命名空间: DevOps.Logic
*
* 功 能： 服务信息返回实体
*
* 类 名： ServiceInfoResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/5/9 14:31:23 	Harvey    创建
* ─────────────────────────────────────────────────
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Logic
{
    /// <summary>
    /// 服务信息返回实体
    /// </summary>
    public class ServiceInfoResponse
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 服务名称【ip地址和服务名称加起来，必须唯一】
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 显示名称
        /// </summary>
        public string display_name
        {
            get; set;
        }

        /// <summary>
        /// 寄宿方式【iis，windows，Linux】
        /// </summary>
        public string boarding_mode
        {
            get; set;
        }

        /// <summary>
        /// IP地址
        /// </summary>
        public string ip
        {
            get; set;
        }

        /// <summary>
        /// 服务器登录账号【必须加码】
        /// </summary>
        public string port
        {
            get; set;
        }

        ///// <summary>
        ///// 服务器登录账号【必须加码】
        ///// </summary>
        //public string login_account
        //{
        //    get; set;
        //}

        ///// <summary>
        ///// 服务器登录密码【必须加码】
        ///// </summary>
        //public string login_password
        //{
        //    get; set;
        //}

        /// <summary>
        /// 文件地址【必须加码,iis指文件夹，服务指EXE地址】
        /// </summary>
        public string file_path
        {
            get; set;
        }

        /// <summary>
        /// IIS托管模式【集成、经典】
        /// </summary>
        public string hosted_mode
        {
            get; set;
        }

        /// <summary>
        /// 框架版本【4,2,0 表示无托管】
        /// </summary>
        public int framework_version
        {
            get; set;
        }

        /// <summary>
        /// 框架版本【4,2,0 表示无托管】
        /// </summary>
        public string framework_version_name
        {
            get; set;
        }

        /// <summary>
        /// 状态【无状态0，开始或运行100，停止200，删除或卸载300】
        /// </summary>
        public int status
        {
            get; set;
        }

        /// <summary>
        /// 状态【无状态0，开始或运行100，停止200，删除或卸载300】
        /// </summary>
        public string status_name
        {
            get; set;
        }

        /// <summary>
        /// 权限码【没有这个权限码，无法操作服务】
        /// </summary>
        public string authority_code
        {
            get; set;
        }

        /// <summary>
        /// 描述
        /// </summary>
        public string describe
        {
            get; set;
        }

        /// <summary>
        /// 是否已经上传发布文件
        /// </summary>
        public bool is_upload_file
        {
            get; set;
        }

        /// <summary>
        /// 可执行文件名称
        /// </summary>
        public string exe_file
        {
            get; set;
        }

        /// <summary>
        /// 伺服端口
        /// </summary>
        public string servo_port
        {
            get; set;
        }

        /// <summary>
        /// 服务类型
        /// </summary>
        public string service_type
        {
            get; set;
        }
    }
}
