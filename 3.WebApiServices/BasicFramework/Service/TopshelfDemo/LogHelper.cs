﻿using NLog;
using NLog.Config;
using System;
using System.Collections.Generic;
using System.Text;

namespace TopshelfDemo
{
    public class LogHelper
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 静态构造函数
        /// </summary>
        static LogHelper()
        {
            //初始化配置日志
            LogManager.Configuration = new XmlLoggingConfiguration(AppDomain.CurrentDomain.BaseDirectory.ToString() + "/nlog.config");
        }

        /// <summary>
        /// 调试信息，详尽信息次于 Trace ，在生产环境中通常不启用
        /// </summary>
        /// <param name="message"></param>
        public static void Info(string message)
        {
            try
            {
                logger?.Info($"{Environment.NewLine}------------------------------------------------------------{Environment.NewLine}{message}");
            }
            catch (Exception exception)
            {
                //NLog:catch setup errors
                logger.Error(exception, "Stopped program because of exception");
            }
        }

        /// <summary>
        /// 调试信息，详尽信息次于 Trace ，在生产环境中通常不启用
        /// </summary>
        /// <param name="message"></param>
        public static void Error(string message)
        {
            try
            {
                logger?.Debug($"{Environment.NewLine}------------------------------------------------------------{Environment.NewLine}{"Debug异常"}:{message}");
            }
            catch (Exception exception)
            {
                //NLog:catch setup errors
                logger.Error(exception, "Stopped program because of exception");
            }
        }
    }
}
