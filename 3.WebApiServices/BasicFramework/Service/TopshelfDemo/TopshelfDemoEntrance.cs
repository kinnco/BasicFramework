﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using Topshelf.Logging;

namespace TopshelfDemo
{
    public class TopshelfDemoEntrance 
    {

        /// <summary>
        /// 释放托管资源接口
        /// </summary>
        private IDisposable dispose = null;

        /// <summary>
        /// 启动服务
        /// </summary>
        public void Start(string[] args)
        {
            try
            {
                LogHelper.Info("服务启动...");

                //TODO: 在此处添加代码以启动服务。
                string baseAddress = string.Empty;

                //if (args.Length > 0)
                //    baseAddress = args[0];
                //else
                baseAddress = "http://localhost:9528/";
                SetAllowUnsafeHeaderParsing(true);
                //dispose = WebApp.Start(baseAddress, Startup =>
                //{
                //    try
                //    {
                //        HttpConfiguration config = new HttpConfiguration();

                //        // 注册特性配置路由
                //        config.MapHttpAttributeRoutes();

                //        // 清除XML响应
                //        config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

                //        //启用跨域
                //        /*
                //         * 启用跨域
                //         * 请注意：
                //         * EnableCorsAttribute 第一个参数 origins是配置允许
                //         * 跨域访问的服务地址，如果允许多个服务地址跨域访问
                //         * 本服务，请使用“,”隔开，如：
                //         * 
                //         *【http://192.138.1.1:9527,http://192.168.1.2:9527 】
                //         *      
                //         * 既表示允许上述两个地址跨域访问本服务，另，如果是
                //         * ie浏览器，使用的是jquery的话，为了接入浏览器支持
                //         * （主要是ie8，9）请设置 $.support.cors = true;
                //         */
                //        config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
                //        // 应用配置
                //        Startup.UseWebApi(config);
                //    }
                //    catch (Exception ex)
                //    {
                //        log.Error("服务出错", ex);
                //    }
                //});
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
            }
        }

        /// <summary>
        /// 设置允许不安全的响应头解析
        /// </summary>
        /// <param name="useUnsafe"></param>
        /// <returns></returns>
        private bool SetAllowUnsafeHeaderParsing(bool useUnsafe)
        {
            ////获取包含内部类System.Net.Configuration.SettingsSection的程序集
            //System.Reflection.Assembly aNetAssembly = System.Reflection.Assembly.GetAssembly(typeof(System.Net.Configuration.SettingsSection));
            //if (aNetAssembly != null)
            //{
            //    // 使用程序集获取内部类型SettingsSectionInternal
            //    Type aSettingsType = aNetAssembly.GetType("System.Net.Configuration.SettingsSectionInternal");

            //    if (aSettingsType != null)
            //    {
            //        /**
            //         * 使用内部静态属性Section来获取内部设置类的实例                     
            //         */
            //        object anInstance = aSettingsType.InvokeMember("Section",
            //          System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.GetProperty | System.Reflection.BindingFlags.NonPublic, null, null, new object[] { });

            //        if (anInstance != null)
            //        {
            //            /**
            //             * 定位到私有属性useUnsafeHeaderParsing设置其值告诉框架不安全的头解析是否被允许
            //             */
            //            System.Reflection.FieldInfo aUseUnsafeHeaderParsing = aSettingsType.GetField("useUnsafeHeaderParsing", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            //            if (aUseUnsafeHeaderParsing != null)
            //            {
            //                aUseUnsafeHeaderParsing.SetValue(anInstance, useUnsafe);
            //                return true;
            //            }
            //        }
            //    }
            //}
            return false;
        }

        /// <summary>
        /// 停止服务
        /// </summary>
        public void Stop()
        {
            try
            {
                LogHelper.Info("停止服务");
                StopWindowsService("ApiServiceTest");
                dispose.Dispose();
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
            }
        }

        /// <summary>
        /// 停止服务
        /// </summary>
        /// <param name="serviceName"></param>
        public void StopWindowsService(string serviceName)
        {
            ServiceController[] scs = ServiceController.GetServices();
            foreach (ServiceController sc in scs)
            {
                if (sc.DisplayName == serviceName)
                {
                    try
                    {
                        sc.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(30));
                        sc.Stop();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}
