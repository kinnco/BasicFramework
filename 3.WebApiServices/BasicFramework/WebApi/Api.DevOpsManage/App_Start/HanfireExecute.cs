﻿using DevOps.Logic;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.DevOpsManage.App_Start
{
    /// <summary>
    /// Hanfire执行
    /// </summary>
    public static class HanfireExecute
    {
        /// <summary>
        /// 开启Hanfire定时任务
        /// </summary>
        public static void HanfireStart()
        {
            //清空Hangfire信息
            ApiMonitorLogServiceRedis.EmptyHangfire();

            //删除过期接口访问记录信息
            RemoveExpiredApiLog();

            //删除过期接口访问记录统计信息
            RemoveExpiredGroupInfo();
        }

        #region Hangfire

        /// <summary>
        /// 删除过期接口访问记录信息【失效时间10分钟，有配置】
        /// </summary>
        private static void RemoveExpiredApiLog()
        {
            string JobId = "RemoveExpiredApiMonitorLog";
            RecurringJob.AddOrUpdate(JobId, () => ApiMonitorLogServiceRedis.RemoveExpiredApiMonitorLog(), Cron.Minutely());
        }

        /// <summary>
        /// 删除过期接口访问记录统计信息【失效时间10分钟，有配置】
        /// </summary>
        private static void RemoveExpiredGroupInfo()
        {
            string JobId = "RemoveExpiredGroupInfo";
            RecurringJob.AddOrUpdate(JobId, () => ApiMonitorLogServiceRedis.RemoveExpiredGroupInfo(), Cron.Minutely());
        }
        #endregion
    }
}
