﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/*
* 命名空间: Api.Manage.App_Start.Headers
*
* 功 能： 响应头的增删集合
*
* 类 名： SecurityHeadersPolicy
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/15 10:41:02 				Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Api.DevOpsManage.App_Start
{
    /// <summary>
    /// 响应头的增删集合
    /// </summary>
    public class SecurityHeadersPolicy
    {
        /// <summary>
        /// 设置头部
        /// </summary>
        public IDictionary<string, string> SetHeaders { get; }
             = new Dictionary<string, string>();

        /// <summary>
        /// 删除头部
        /// </summary>
        public ISet<string> RemoveHeaders { get; }
            = new HashSet<string>();
    }

}
