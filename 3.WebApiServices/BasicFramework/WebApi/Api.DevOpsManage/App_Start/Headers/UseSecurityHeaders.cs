﻿using Microsoft.AspNetCore.Builder;

/*
* 命名空间: Api.Manage.App_Start.Headers
*
* 功 能： 中间件拓展方法
*
* 类 名： UseSecurityHeaders
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/15 10:42:21 				Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Api.DevOpsManage.App_Start
{
    /// <summary>
    /// 中间件拓展方法
    /// </summary>
    public static class UseSecurityHeaders
    {
        /// <summary>
        /// 使用中间件
        /// </summary>
        /// <param name="app"></param>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseSecurityHeadersMiddleware(this IApplicationBuilder app, SecurityHeadersBuilder builder)
        {
            SecurityHeadersPolicy policy = builder.Build();
            return app.UseMiddleware<SecurityHeadersMiddleware>(policy);
        }
    }
}
