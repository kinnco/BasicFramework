﻿using Api.DevOpsManage.App_Start;
using DevOps.Logic;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Mvc;
using System.Web;

namespace Api.DevOpsManage.Areas.Admin.Controllers
{

    /// <summary>
    /// 公用操作接口
    /// </summary>
    [ApiExplorerSettings(GroupName = "Common")]
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : BaseApiController
    {
        /// <summary>
        /// 参数管理逻辑
        /// </summary>
        private readonly IParameterSettingService parameterSettingService = null;

        private readonly ICommonService safeCommonService = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        public CommonController()
        {
            safeCommonService = UnityCIContainer.Instance.GetService<ICommonService>();

            parameterSettingService = UnityCIContainer.Instance.GetService<IParameterSettingService>();
        }

        /// <summary>
        /// 上传发布文件
        /// </summary>
        /// <returns></returns>
        [HttpPost("UploadPublishFile")]
        [HiddenApi]
        public ResultJsonInfo<string> UploadPublishFile()
        {
            var resultInfo = new ResultJsonInfo<string>();

            var files = HttpContext.Request.Form.Files;

            string filePath = HttpUtility.UrlDecode(HttpContext.Request.Form["filePath"]);

            TryCatch(() =>
            {
                resultInfo = safeCommonService.UploadPublishFile(files[0], filePath);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "上传发布文件失败");

            }, $"系统错误，上传发布文件失败");

            return resultInfo;
        }
    }
}