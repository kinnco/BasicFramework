﻿using System.Collections.Generic;
using Api.DevOpsManage.App_Start;
using Common.Library;
using Common.Model;
using Container.Library;
using DevOps.Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.DevOpsManage.Areas.Flow.Controllers
{
    /// <summary>
    /// 流程类型接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Flow")]
    [Route("api/[controller]")]
    [ApiController]
    public class FlowInfoController : BaseApiController
    {

        /// <summary>
        /// 流程基础信息管理逻辑
        /// </summary>
        public IFlowInfoService flowInfoService = null;

        /// <summary>
        /// 流程按钮逻辑
        /// </summary>
        public IFlowButtonInfoService flowButtonInfoService = null;

        
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowInfoController()
        {
            flowInfoService = UnityCIContainer.Instance.GetService<IFlowInfoService>();
            flowButtonInfoService = UnityCIContainer.Instance.GetService<IFlowButtonInfoService>();
        }

        #region 流程信息管理模块

        #region 查询

        /// <summary>
        /// 根据关键字 分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<FlowInfoResponse>> LoadPageList(ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<FlowInfoResponse>>();

            TryCatch(() =>
            {

                resultInfo = flowInfoService.LoadPageList(inputInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "分页获取列表流程失败");

            }, $"系统错误，流程管理-分页获取列表流程失败");
            return resultInfo;
        }

        /// <summary>
        /// 根据Id值获取单条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("LoadSingleById/{id}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<FlowDetailInfoResponse> LoadSingleById(string id)
        {
            var resultInfo = new ResultJsonInfo<FlowDetailInfoResponse>();

            TryCatch(() =>
            {
                resultInfo = flowInfoService.LoadSingleById(id);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "根据Id值获取单条信息失败");

            }, $"系统错误，流程管理-根据Id值获取单条信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 获取所有用户机构树状信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("LoadDepartmentUserTreeList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<DepartmentUserTreeInfo>> LoadDepartmentUserTreeList()
        {
            var resultInfo = new ResultJsonInfo<List<DepartmentUserTreeInfo>>();

            TryCatch(() =>
            {

                resultInfo = flowInfoService.LoadDepartmentUserTreeList();

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "获取所有用户机构树状信息失败");

            }, $"系统错误，流程管理-获取所有用户机构树状信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 获取流程处理者类型
        /// </summary>
        /// <returns></returns>
        [HttpGet("LoadFlowHandlerTypeList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadFlowHandlerTypeList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            TryCatch(() => {

                resultInfo = flowInfoService.LoadFlowHandlerTypeList();

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "获取流程处理者类型失败");

            }, $"系统错误，流程管理-获取流程处理者类型失败");

            return resultInfo;
        }


        /// <summary>
        /// 获取会签策略
        /// </summary>
        /// <returns></returns>
        [HttpGet("LoadFlowSignStrategyList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadFlowSignStrategyList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            TryCatch(() => {

                resultInfo = flowInfoService.LoadFlowSignStrategyList();

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "获取会签策略失败");

            }, $"系统错误，流程管理-获取会签策略失败");

            return resultInfo;
        }
        

        /// <summary>
        /// 获取流程退回类型
        /// </summary>
        /// <returns></returns>
        [HttpGet("LoadFlowReturnTypeList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadFlowReturnTypeList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            TryCatch(() => {

                resultInfo = flowInfoService.LoadFlowReturnTypeList();

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "获取流程退回类型失败");

            }, $"系统错误，流程管理-获取流程退回类型失败");

            return resultInfo;
        }

        /// <summary>
        /// 获取流程处理策略
        /// </summary>
        /// <returns></returns>
        [HttpGet("LoadFlowHandleStrategyList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadFlowHandleStrategyList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            TryCatch(() => {

                resultInfo = flowInfoService.LoadFlowHandleStrategyList();

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "获取流程处理策略失败");

            }, $"系统错误，流程管理-获取流程处理策略失败");

            return resultInfo;
        }


        /// <summary>
        /// 获取流程步骤包含按钮列表【所有和已经有了的】
        /// </summary>
        /// <returns></returns>
        [HttpGet("LoadFlowOwnButtonList/{stepId}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<FlowOwnButtonResponse>> LoadFlowOwnButtonList(string stepId)
        {
            var resultInfo = new ResultJsonInfo<List<FlowOwnButtonResponse>>();

            TryCatch(() => {

                resultInfo = flowButtonInfoService.LoadFlowOwnButtonList(stepId);

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "获取流程步骤包含按钮列表失败");

            }, $"系统错误，流程管理-获取流程步骤包含按钮列表失败");

            return resultInfo;
        }
        #endregion

        #region 更新

        /// <summary>
        /// 编辑流程【不包括流程图】
        /// </summary>
        /// <returns></returns>
        [HttpPost("Save")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Save(FlowInfoRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = flowInfoService.Save(inputInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "编辑流程失败");

            }, $"系统错误，流程管理-编辑流程失败");
            return resultInfo;
        }

        /// <summary>
        /// 编辑流程详情信息【包括流程图】
        /// </summary>
        /// <returns></returns>
        [HttpPost("SaveFlowMapl")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> SaveFlowMapl(FlowDetailInfoRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = flowInfoService.SaveFlowMapl(inputInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "编辑流程详情信息失败");

            }, $"系统错误，流程管理-编辑流程详情信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 安装流程【包括流程图】
        /// </summary>
        /// <returns></returns>
        [HttpPost("InstallFlowMapl")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> InstallFlowMapl(FlowDetailInfoRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = flowInfoService.InstallFlowMapl(inputInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "安装流程【包括流程图】失败");

            }, $"系统错误，流程管理-安装流程【包括流程图】失败");
            return resultInfo;
        }

        /// <summary>
        /// 安装/卸载
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("InstallUninstall/{id}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> InstallUninstall(string id)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = flowInfoService.InstallUninstall(id);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "安装/卸载流程失败");

            }, $"系统错误，流程管理-安装/卸载流程失败");
            return resultInfo;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = flowInfoService.Remove(ids);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "删除流程失败");

            }, $"系统错误，流程管理-删除流程失败");
            return resultInfo;
        }

        #endregion

        #endregion
    }
}