﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.DevOpsManage.App_Start;
using Common.Model;
using Container.Library;
using DevOps.Logic;
using DevOps.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.DevOpsManage.Areas.Flow.Controllers
{
    /// <summary>
    /// 流程按钮接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Flow")]
    [Route("api/[controller]")]
    [ApiController]
    public class FlowButtonInfoController : BaseApiController
    {

        /// <summary>
        /// 按钮管理逻辑
        /// </summary>
        public IFlowButtonInfoService flowButtonInfoService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowButtonInfoController()
        {
            flowButtonInfoService = UnityCIContainer.Instance.GetService<IFlowButtonInfoService>();
        }



        #region 按钮信息管理模块

        #region 查询

        /// <summary>
        /// 根据关键字 分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<FlowButtonInfoEntity>> LoadPageList(ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<FlowButtonInfoEntity>>();

            TryCatch(() =>
            {

                resultInfo = flowButtonInfoService.LoadPageList(inputInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "根据关键字分页获取列表失败");

            }, $"系统错误，按钮管理-根据关键字分页获取列表失败");
            return resultInfo;
        }


        #endregion

        #region 更新

        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        [HttpPost("Save")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Save(FlowButtonInfoEntity inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = flowButtonInfoService.Save(inputInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "编辑按钮信息失败");

            }, $"系统错误，按钮管理-编辑按钮信息失败");
            return resultInfo;

        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = flowButtonInfoService.Remove(ids);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "删除按钮信息失败");

            }, $"系统错误，按钮管理-删除按钮信息失败");
            return resultInfo;
        }
        #endregion

        #endregion
    }
}