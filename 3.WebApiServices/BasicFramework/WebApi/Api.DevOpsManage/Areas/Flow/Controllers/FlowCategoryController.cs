﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.DevOpsManage.App_Start;
using Common.Model;
using Container.Library;
using DevOps.Logic;
using DevOps.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.DevOpsManage.Areas.Flow.Controllers
{
    /// <summary>
    /// 流程类型接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Flow")]
    [Route("api/[controller]")]
    [ApiController]
    public class FlowCategoryController : BaseApiController
    {
        /// <summary>
        /// 类型管理逻辑
        /// </summary>
        public IFlowCategoryService flowCategoryService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowCategoryController()
        {
            flowCategoryService = UnityCIContainer.Instance.GetService<IFlowCategoryService>();
        }

        #region 流程类型信息管理模块

        #region 查询

        /// <summary>
        /// 根据关键字 分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<FlowCategoryInfoEntity>> LoadPageList(ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<FlowCategoryInfoEntity>>();

            TryCatch(() =>
            {

                resultInfo = flowCategoryService.LoadPageList(inputInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "根据关键字分页获取列表失败");

            }, $"系统错误，类型管理-根据关键字分页获取列表失败");
            return resultInfo;
        }

        /// <summary>
        ///获取所有列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("LoadList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<FlowCategoryInfoEntity>> LoadList()
        {
            var resultInfo = new ResultJsonInfo<List<FlowCategoryInfoEntity>>();

            TryCatch(() =>
            {

                resultInfo = flowCategoryService.LoadList();

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "获取所有列表失败");

            }, $"系统错误，类型管理-获取所有列表失败");
            return resultInfo;
        }
        #endregion

        #region 更新

        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        [HttpPost("Save")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Save(FlowCategoryInfoEntity inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = flowCategoryService.Save(inputInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "编辑类型信息失败");

            }, $"系统错误，类型管理-编辑类型信息失败");
            return resultInfo;

        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = flowCategoryService.Remove(ids);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "删除类型信息失败");

            }, $"系统错误，类型管理-删除类型信息失败");
            return resultInfo;
        }
        #endregion

        #endregion
    }
}