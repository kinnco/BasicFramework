﻿using System.Collections.Generic;
using Api.DevOpsManage.App_Start;
using DevOps.Logic;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Validate.Library;
using DevOps.Model;

/*
* 命名空间: Api.DevOpsManage.Areas.Business.Controllers
*
* 功 能：参数管理接口控制器 
*
* 类 名： ParameterSettingController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/06/15 14:34:43 Harvey       创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Api.DevOpsManage.Areas.Business.Controllers
{
    /// <summary>
    /// 参数管理接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Business")]
    [Route("api/Business/[controller]")]
    [ApiController]
    public class ParameterSettingController : BaseApiController
    {
        /// <summary>
        /// 参数管理逻辑
        /// </summary>
        public IParameterSettingService parameterSettingService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ParameterSettingController()
        {
            parameterSettingService = UnityCIContainer.Instance.GetService<IParameterSettingService>();
        }


        #region 基础信息管理模块

        #region 查询
        /// <summary>
        ///根据关键字分页获取列表
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<SysParameterSettingEntity>> LoadPageList([FromBody]ParametersInfo<string> queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<SysParameterSettingEntity>>();

            TryCatch(() =>
            {

                resultInfo = parameterSettingService.LoadPageList(queryInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "获取参数设置分页列表失败");

            }, $"系统错误，参数管理-获取参数设置分页列表失败");
            return resultInfo;
        }

        #endregion

        #region 更新

        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        [HttpPost("Add")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Add([FromBody]ParameterSettingOptRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                inputInfo.Validate();
                resultInfo = parameterSettingService.Modify(inputInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "新增参数信息失败");

            }, $"系统错误，参数管理-新增参数信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <returns></returns>
        [HttpPost("Modify")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Modify([FromBody]ParameterSettingOptRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                inputInfo.Validate();
                resultInfo = parameterSettingService.Modify(inputInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "更新参数信息失败");

            }, $"系统错误，参数管理-更新参数信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Remove(List<string> Ids)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = parameterSettingService.Delete(Ids);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "删除参数信息失败");

            }, $"系统错误，参数管理-删除参数信息失败");
            return resultInfo;
        }

        #endregion

        #endregion
    }
}