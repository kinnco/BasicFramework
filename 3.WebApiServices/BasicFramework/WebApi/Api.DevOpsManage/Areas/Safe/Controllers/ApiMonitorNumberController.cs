﻿using Api.DevOpsManage.App_Start;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using DevOps.Logic;
using System.Collections.Generic;

namespace Api.Manage.Areas.Safe.Controllers
{
    /// <summary>
    /// 接口监控访问次数
    /// </summary>
    [ApiExplorerSettings(GroupName = "Safe")]
    [Route("api/Safe/[controller]")]
    [ApiController]
    public class ApiMonitorNumberController : BaseApiController
    {
        /// <summary>
        /// 访问明细和访问次数，都是用的IApiMonitorLogService 这个接口
        ///【业务区别不大，都是接口访问相关的，不用再去增加分支】
        /// </summary>
        private readonly IApiMonitorLogService apiMonitorLogService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ApiMonitorNumberController()
        {
            apiMonitorLogService = UnityCIContainer.Instance.GetService<IApiMonitorLogService>();
        }

        #region 接口访问次数基础操作

        /// <summary>
        /// 根据条件分页获取接口访问次数列表 关键字【请求路由名称】
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<ApiMonitorNumberResponse>> LoadPageList([FromBody]ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<ApiMonitorNumberResponse>>();

            TryCatch(() =>
            {
                resultInfo = apiMonitorLogService.LoadApiMonitorNumberList(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "根据条件分页获取接口访问次数列表失败");

            }, $"系统错误，接口访问次数-根据条件分页获取接口访问次数列表失败");

            return resultInfo;
        }

        #endregion
    }
}