﻿using Api.DevOpsManage.App_Start;
using DevOps.Logic;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Validate.Library;
/*
* 命名空间: Api.DevOpsManage.Areas.Authority.Controllers
*
* 功 能： 岗位接口控制器
*
* 类 名： PostController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Api.DevOpsManage.Areas.Authority.Controllers
{
    /// <summary>
    /// 岗位接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Authority")]
    [Route("api/Authority/[controller]")]
    [ApiController]
    public class PostController : BaseApiController
    {
        /// <summary>
        /// 岗位逻辑
        /// </summary>
        public ISysPostService postService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public PostController()
        {
            postService = UnityCIContainer.Instance.GetService<ISysPostService>();
        }

        #region 岗位基础信息管理模块

        #region 查询
        /// <summary>
        ///根据岗位名称关键字 分页获取岗位列表
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<PostResponse>> LoadList([FromBody]ParametersInfo<string> queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<PostResponse>>();

            TryCatch(() =>
            {

                resultInfo = postService.LoadSysPostList(queryInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "获取岗位分页列表失败");

            }, $"系统错误，岗位管理-获取岗位分页列表失败");
            return resultInfo;
        }
        /// <summary>
        /// 获取所有岗位数据
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadAllList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<PostResponse>> LoadAllList()
        {
            var resultInfo = new ResultJsonInfo<List<PostResponse>>();

            TryCatch(() =>
            {

                resultInfo = postService.LoadSysPostAllList();

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "获取岗位列表失败");

            }, $"系统错误，岗位管理-获取岗位列表失败");
            return resultInfo;

        }
        /// <summary>
        /// 根据岗位ID，获取所有可操作用户情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadListInfoByPostId")]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [Authorize]
        public ResultJsonInfo<List<PostUserResponse>> LoadListInfoByPostId([FromBody]PostUserQuery queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<PostUserResponse>>();

            TryCatch(() =>
            {
                resultInfo = postService.LoadListInfoByPostId(queryInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "根据岗位ID，获取所有可操作用户情况信息失败");

            }, $"系统错误，岗位管理- 根据岗位ID，获取所有可操作用户情况信息失败");

            return resultInfo;
        }


        /// <summary>
        /// 获取所有岗位Select树状数据
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadAllSelectList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<SelectListInfo>> LoadAllSelectList()
        {
            var resultInfo = new ResultJsonInfo<List<SelectListInfo>>();
            TryCatch(() =>
            {
                resultInfo = postService.LoadSysPostTreeList();
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取所有岗位树状数据失败");

            }, $"系统错误，岗位管理-获取所有岗位树状数据失败");
            return resultInfo;
        }


        /// <summary>
        /// 获取单个岗位的信息
        /// </summary>
        /// <param name="postId">岗位ID</param>
        /// <returns></returns>
        [HttpGet("LoadSingle/{postId}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<PostResponse> LoadSingle(string postId)
        {
            var resultInfo = new ResultJsonInfo<PostResponse>();

            TryCatch(() =>
            {

                resultInfo = postService.LoadSysPostListOne(postId);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "获取单个岗位失败");

            }, $"系统错误，岗位管理-获取单个岗位失败");
            return resultInfo;
        }
        #endregion

        #region 新增
        /// <summary>
        /// 增加岗位信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("AddPost")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> AddPost([FromBody]PostAddRequest addPost)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                addPost.Validate();

                resultInfo = postService.AddSysPost(addPost);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "增加岗位信息失败");

            }, $"系统错误，岗位管理-增加岗位信息失败");
            return resultInfo;
        }
        #endregion

        #region 修改

        /// <summary>
        /// 修改岗位
        /// </summary>
        /// <returns></returns>
        [HttpPost("Modify")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Modify([FromBody]PostModifyRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                inputInfo.Validate();
                resultInfo = postService.ModifySysPost(inputInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "修改岗位失败");

            }, $"系统错误，岗位管理-修改岗位失败");
            return resultInfo;
        }

        /// <summary>
        /// 删除岗位
        /// </summary>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Remove(List<string> postIds)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = postService.DeleteSysPost(postIds);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "删除岗位失败");

            }, $"系统错误，岗位管理-删除岗位失败");
            return resultInfo;
        }
        /// <summary>
        /// 禁用/启用岗位
        /// </summary>
        /// <returns></returns>
        [HttpGet("ForbidOrEnable/{postId}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> ForbidOrEnable(string postId)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = postService.ForbiddenSysPost(postId);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "禁用/启用岗位失败");

            }, $"系统错误，岗位管理-禁用/启用岗位失败");
            return resultInfo;
        }
        /// <summary>
        /// 修改对应部门的用户
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost("ModifyPostUserInfo")]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [Authorize]
        public ResultJsonInfo<int> ModifyPostUserInfo([FromBody]PostUserModifyRequest post)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = postService.ModifyPostUserInfo(post);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "修改对应岗位的用户信息失败");

            }, $"系统错误，岗位-修改对应岗位的用户信息失败");

            return resultInfo;
        }

        #endregion

        #endregion

    }
}