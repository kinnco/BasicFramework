﻿using System.Collections.Generic;
using Api.DevOpsManage.App_Start;
using DevOps.Logic;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Validate.Library;

/*
* 命名空间: Api.DevOpsManage.Areas.Authority.Controllers
*
* 功 能： 角色相关接口控制器
*
* 类 名： RoleInfoController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/01 10:30:12 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Api.DevOpsManage.Areas.Authority.Controllers
{
    /// <summary>
    /// 角色相关接口
    /// </summary>
    [ApiExplorerSettings(GroupName = "Authority")]
    [Route("api/Authority/[controller]")]
    [ApiController]
    public class RoleInfoController : BaseApiController
    {

        private readonly ISysRoleService roleService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public RoleInfoController()
        {
            roleService = UnityCIContainer.Instance.GetService<ISysRoleService>();
        }

        #region 角色基础信息管理操作

        #region 查询
        /// <summary>
        /// 根据条件分页查询用户数据
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpPost("LoadList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<RoleInfoResponse>> LoadList([FromBody]ParametersInfo<RoleInfoQuery> parameters)
        {
            var resultInfo = new ResultJsonInfo<List<RoleInfoResponse>>();

            TryCatch(() =>
            {

                resultInfo = roleService.LoadList(parameters);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "分页查询角色数据失败");

            }, $"系统错误，角色管理-分页查询角色数据失败");
            return resultInfo;
        }
        /// <summary>
        /// 查询单个用户的数据
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet("LoadSingle/{roleId}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<RoleInfoResponse> LoadSingle(string roleId)
        {
            var resultInfo = new ResultJsonInfo<RoleInfoResponse>();

            TryCatch(() =>
            {

                resultInfo = roleService.LoadSingle(roleId);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "单个角色的数据失败");

            }, $"系统错误，角色管理-单个角色的数据失败");
            return resultInfo;
        }
        #endregion

        #region 添加

        /// <summary>
        /// 新增角色
        /// </summary>
        /// <param name="addInfo"></param>
        /// <returns></returns>
        [HttpPost("AddInfo")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> AddInfo([FromBody]RoleInfoAddRequest addInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                addInfo.Validate();

                resultInfo = roleService.AddRole(addInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "新增角色失败");

            }, $"系统错误，角色管理-新增角色失败");
            return resultInfo;
        }

        #endregion

        #region 修改

        /// <summary>
        /// 修改角色数据
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("Modify")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Modify([FromBody]RoleInfoModifyRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                request.Validate();
                resultInfo = roleService.Modify(request);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "修改角色数据失败");

            }, $"系统错误，角色管理-修改角色数据失败");
            return resultInfo;
        }

        /// <summary>
        /// 禁用/启用角色
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("ForbidOrEnable/{id}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> ForbidOrEnable(string id)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = roleService.ForbidOrEnable(id);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "禁用/启用角色失败");

            }, $"系统错误，角色管理-禁用/启用角色失败");
            return resultInfo;
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = roleService.Remove(ids);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "删除角色失败");

            }, $"系统错误，角色管理-删除角色失败");
            return resultInfo;
        }

        #endregion

        #endregion

        #region 权限相关操作

        /// <summary>
        /// 获取所有角色Select数据
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadAllSelectList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<SelectListInfo>> LoadAllSelectList()
        {
            var resultInfo = new ResultJsonInfo<List<SelectListInfo>>();
            TryCatch(() =>
            {
                resultInfo = roleService.LoadAllSelectList();
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取所有角色Select数据失败");

            }, $"系统错误，角色管理-获取所有角色Select数据失败");
            return resultInfo;
        }

        #endregion

        #region 批量导入导出
        /// <summary>
        /// 事务批量导入角色信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("TranBulkImportRole")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> TranBulkImportRole([FromBody]List<RoleInfoAddRequest> selects)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                resultInfo = roleService.TranBulkImportRole(selects);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "事务批量导入角色信息失败");

            }, $"系统错误，角色管理-事务批量导入角色信息失败");
            return resultInfo;
        }
        /// <summary>
        /// 非事务批量导入角色信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("BulkImportRole")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> BulkImportRole([FromBody]List<RoleInfoAddRequest> selects)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                resultInfo = roleService.BulkImportRole(selects);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "非事务批量导入角色信息失败");

            }, $"系统错误，角色管理-非事务批量导入角色信息失败");
            return resultInfo;
        }
        /// <summary>
        /// 批量导出所有
        /// </summary>
        /// <returns></returns>
        [HttpGet("ListAll")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<RoleInfoResponse>> ListAll()
        {
            var resultInfo = new ResultJsonInfo<List<RoleInfoResponse>>();
            TryCatch(() =>
            {
                resultInfo = roleService.ListAll();
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "批量导出角色信息失败");

            }, $"系统错误，角色管理-批量导出角色信息失败");
            return resultInfo;
        }
        #endregion


    }
}