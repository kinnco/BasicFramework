﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间:  Common.Model
*
* 功 能：报表列信息。
*
* 类 名：DingColumnForTopVo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/10 14:55:16       LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{

    /// <summary>
    /// 钉钉通用接口调用返回信息
    /// </summary>
    public class DingColumnsResponse
    {
        /// <summary>
        /// result
        /// </summary>
        public DingColumnForTopVoShell result { get; set; }

        /// <summary>
        /// errcode
        /// </summary>
        public long errcode { get; set; }


        /// <summary>
        /// 请求ID
        /// </summary>
        public string request_id { get; set; }
    }

    /// <summary>
    /// 报表列信息壳子
    /// </summary>
    public class DingColumnForTopVoShell
    {
        /// <summary>
        /// columns
        /// </summary>
        public List<DingColumnForTopVo> columns { get; set; }
        
    }

    /// <summary>
    /// 报表列信息
    /// </summary>
    public class DingColumnForTopVo
    {

        /// <summary>
        /// 报表列ID。
        /// </summary>
        public long id
        {
            get; set;
        }

        /// <summary>
        /// 报表列类型
        /// </summary>
        public int type
        {
            get; set;
        }

        /// <summary>
        /// 报表列名
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 列报表
        /// </summary>
        public string alias
        {
            get; set;
        }

        /// <summary>
        /// 报表列的状态。
        /// </summary>
        public int status
        {
            get; set;
        }

        /// <summary>
        /// 子类型
        /// </summary>
        public int sub_type
        {
            get; set;
        }
    }
}
