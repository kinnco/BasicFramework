﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  Common.Model
*
* 功 能：员工详细信息
*
* 类 名：DingEmployeeInfoDetail
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/11 14:14:40       LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 员工详细信息
    /// </summary>
    public class DingEmployeeInfoResponse
    {

        /// <summary>
        /// 是否为企业的老板
        /// </summary>
        public bool boss { get; set; }

        /// <summary>
        /// 员工的userid
        /// </summary>
        public string userid { get; set; }

        /// <summary>
        /// 内务系统员工的userid-----与内务系统关联
        /// </summary>
        public string employee_id { get; set; }

        /// <summary>
        /// 员工在当前开发者企业账号范围内的唯一标识
        /// </summary>
        public string unionid { get; set; }

        /// <summary>
        /// 员工名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string avatar { get; set; }

        /// <summary>
        /// 国际电话区号
        /// </summary>
        public string state_code { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        public string mobile { get; set; }

        /// <summary>
        /// 是否号码隐藏： true：隐藏 false：不隐藏
        /// </summary>
        public bool hide_mobile { get; set; }

        /// <summary>
        /// 分机号。
        /// </summary>
        public string telephone { get; set; }

        /// <summary>
        /// 员工工号
        /// </summary>
        public string job_number { get; set; }

        /// <summary>
        /// 职位
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 办公地点
        /// </summary>
        public string work_place { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// 所属部门ID列表
        /// </summary>
        public List<string> dept_id_list { get; set; }

        /// <summary>
        /// 所属部门
        /// </summary>
        public List<string> deptList { get; set; }

        /// <summary>
        /// 员工在对应的部门中的排序。
        /// </summary>
        public List<DeptOrderInfo> dept_order_list { get; set; }

        /// <summary>
        /// 扩展属性，最大长度2000个字符。
        /// </summary>
        public string extension { get; set; }

        /// <summary>
        /// 入职时间，Unix时间戳，单位毫秒。
        /// </summary>
        public long hired_date { get; set; }

        /// <summary>
        /// 是否激活了钉钉： true：已激活 false：未激活
        /// </summary>
        public bool active { get; set; }

        /// <summary>
        /// 是否完成了实名认证
        /// </summary>
        public bool real_authed { get; set; }

        /// <summary>
        /// 是否为企业的高管
        /// </summary>
        public bool senior { get; set; }

        /// <summary>
        /// 是否为企业的管理员
        /// </summary>
        public bool admin { get; set; }

        /// <summary>
        /// 员工在对应的部门中是否领导
        /// </summary>
        public List<DeptLeaderInfo> leader_in_dept { get; set; }

        /// <summary>
        /// 角色列表
        /// </summary>
        public List<DeptOrderInfo> role_list { get; set; }

    }

    /// <summary>
    /// 员工在对应的部门中的排序
    /// </summary>
    public class DeptOrderInfo
    {
        /// <summary>
        /// 部门ID
        /// </summary>
        public long dept_id { get; set; }

        /// <summary>
        /// 员工在部门中的排序。
        /// </summary>
        public long order { get; set; }

    }

    /// <summary>
    /// 员工在对应的部门中是否领导
    /// </summary>
    public class DeptLeaderInfo
    {
        /// <summary>
        /// 部门ID
        /// </summary>
        public long dept_id { get; set; }

        /// <summary>
        /// 是否是领导
        /// </summary>
        public bool leader { get; set; }

    }


    /// <summary>
    /// 角色列表信息
    /// </summary>
    public class UserRoleInfo
    {
        /// <summary>
        /// 角色ID。
        /// </summary>
        public long id { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 角色组名称
        /// </summary>
        public string group_name { get; set; }

    }
}
