﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间:  Common.Model
*
* 功 能：获取员工IDs信息请求信息
*
* 类 名：DingEmployeeIdsRequest
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/9 11:16:45       LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 获取员工IDs信息请求信息
    /// </summary>
    public class DingEmployeeIdsRequest
    {

        /// <summary>
        /// 在职员工子状态筛选，可以查询多个状态。不同状态之间使用英文逗号分隔。2：试用期3：正式5：待离职 -1：无状态
        /// </summary>
        public string status_list { get; set; }

        /// <summary>
        /// 分页游标，从0开始。根据返回结果里的next_cursor是否为空来判断是否还有下一页，且再次调用时offset设置成next_cursor的值。
        /// </summary>
        public int offset { get; set; }

        /// <summary>
        /// 分页大小，最大50。
        /// </summary>
        public int size { get; set; }
    }
}
