﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间:  Common.Model
*
* 功 能：单个审批实例详情
*
* 类 名：DingProcessInstanceResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/15 10:58:35       LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 单个审批实例详情
    /// </summary>
    public class DingProcessInstanceResponse
    {

        /// <summary>
        /// errcode
        /// </summary>
        public long errcode { get; set; }

        /// <summary>
        /// errmsg
        /// </summary>
        public string errmsg { get; set; }

        /// <summary>
        /// 请求ID
        /// </summary>
        public string request_id { get; set; }

        /// <summary>
        /// process_instance
        /// </summary>
        public ProcessInstanceTopVo process_instance { get; set; }

    }

    /// <summary>
    /// 审批实例详情
    /// </summary>
    public class ProcessInstanceTopVo
    {

        /// <summary>
        /// 审批实例ID
        /// </summary>
        public string process_instance_id { get; set; }

        /// <summary>
        /// 审批实例标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 开始时间。
        /// </summary>
        public DateTime create_time { get; set; }

        /// <summary>
        /// 结束时间。。
        /// </summary>
        public DateTime finish_time { get; set; }

        /// <summary>
        /// 发起人的userid。
        /// </summary>
        public string originator_userid { get; set; }

        /// <summary>
        /// 发起人的部门。-1表示根部门。
        /// </summary>
        public string originator_dept_id { get; set; }

        /// <summary>
        /// 审批状态：NEW：新创建 RUNNING：审批中TERMINATED：被终止 COMPLETED：完成  CANCELED：取消
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// agree：同意 refuse：拒绝
        /// </summary>
        public string result { get; set; }

        /// <summary>
        /// 审批实例业务编号
        /// </summary>
        public string business_id { get; set; }

        /// <summary>
        /// 表单详情列表
        /// </summary>
        public List<FormComponentValueVo> form_component_values { get; set; }

        /// <summary>
        /// 缺卡时间
        /// </summary>
        public DateTime lack_time { get; set; }

        /// <summary>
        /// 是否有效【申请补卡7天之内的有效】100有效 110无效
        /// </summary>
        public int is_enabled { get; set; }

        /// <summary>
        /// 缺卡类型  上班缺卡 下班缺卡
        /// </summary>
        public List<string> lack_time_type { get; set; }

    }


    /// <summary>
    /// 表单详情列表。
    /// </summary>
    public class FormComponentValueVo
    {

        /// <summary>
        /// 组件ID
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 组件类型 【DDDateField 缺卡日期 id=DDDateField-K9UYIB01】  【DDMultiSelectField 缺卡情况 下班缺卡 id=DDMultiSelectField-K9UY388K】
        /// </summary>
        public string component_type { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 标签值
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// 标签扩展值
        /// </summary>
        public string ext_value { get; set; }

    }
}
