﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 空间数据库数据Vectorgraph处理信息
    /// </summary>
    public class SdeVectorgraphInfo
    {
        /// <summary>
        /// 操作类型 Insert Modify  Remove
        /// </summary>
        public int operationType { get; set; }

        /// <summary>
        /// 几何图形类型 Point Line Polygon 
        /// </summary>
        public int geometricType { get; set; }

        /// <summary>
        /// 要素类名称
        /// </summary>
        public string strFeatureClassName { get; set; }

        /// <summary>
        /// 空间矢量文件全路径文件夹
        /// </summary>
        public string filePath { get; set; }

        /// <summary>
        /// 空间矢量文件全路径名称
        /// </summary>
        public string fileName { get; set; }


        
    }
}
