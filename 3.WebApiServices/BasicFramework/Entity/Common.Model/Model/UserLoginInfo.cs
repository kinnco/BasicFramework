﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 用户登录信息
    /// </summary>
    public class UserLoginInfo
    {
        /// <summary>
        /// 唯一编码
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string name
        {
            get; set;
        }


        /// <summary>
        /// 性别
        /// </summary>
        public string gender
        {
            get; set;
        }

        /// <summary>
        /// 用户家庭电话
        /// </summary>
        public string telphone
        {
            get; set;
        }

        /// <summary>
        /// 用户手机号码
        /// </summary>
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// qq
        /// </summary>
        public string qq
        {
            get; set;
        }

        /// <summary>
        /// 用户昵称
        /// </summary>
        public string nickname
        {
            get; set;
        }

        /// <summary>
        /// 用户真实名
        /// </summary>
        public string realname
        {
            get; set;
        }

        /// <summary>
        /// email
        /// </summary>
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 办公电话
        /// </summary>
        public string office_phone
        {
            get; set;
        }

        /// <summary>
        /// 用户身份号码
        /// </summary>
        public string idnumber
        {
            get; set;
        }

        /// <summary>
        /// 用户头像地址
        /// </summary>
        public string picture_url
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 所属角色唯一标识符
        /// </summary>
        public string role_ids { get; set; }

        /// <summary>
        /// 所属角色
        /// </summary>
        public string role_names { get; set; }

        /// <summary>
        /// 所属岗位编码
        /// </summary>
        public string post_ids { get; set; }

        /// <summary>
        /// 所属岗位
        /// </summary>
        public string post_names { get; set; }

        /// <summary>
        /// 所属部门编码
        /// </summary>
        public string department_ids { get; set; }

        /// <summary>
        /// 所属部门
        /// </summary>
        public string department_names { get; set; }

        /// <summary>
        /// 状态  
        /// </summary>
        public bool is_valid { get; set; }

        /// <summary>
        /// 授权token
        /// </summary>
        public string token { get; set; }

        /// <summary>
        /// 是否是超级管理员  
        /// </summary>
        public bool is_super_user { get; set; }

        /// <summary>
        /// 用户对应主题
        /// </summary>
        public string theme { get; set; }

        /// <summary>
        /// 用户签章图片地址
        /// </summary>
        public string signature_url { get; set; }

    }
}
