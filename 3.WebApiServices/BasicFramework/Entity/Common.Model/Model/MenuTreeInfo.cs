﻿/*
* 命名空间: Authority.Logic
*
* 功 能： 菜单树结构实体类
*
* 类 名： MenuInfo
*
* Version   变更日期            负责人     变更内容
* ────────────────────────────
* V1.0.1    2019/08/17 19:48:31 Harvey     创建
*
* Copyright (c) 2019 Harvey Corporation. All rights reserved.
*/
using System.Collections.Generic;

namespace Common.Model
{
    /// <summary>
    /// 菜单树结构
    /// </summary>
    public class MenuTreeInfo
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 链接
        /// </summary>
        public string link_url { get; set; }

        /// <summary>
        /// 展开方式
        /// </summary>
        public string target{ get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public double sort { get; set; }

        /// <summary>
        /// IconFont
        /// </summary>
        public string icon_font { get; set; }

        /// <summary>
        /// 是否展开
        /// </summary>
        public bool is_spread{ get; set; }

        /// <summary>
        /// 所属应用【有很多应用，通过","区分】
        /// </summary>
        public string application_ids {  get; set; }

        /// <summary>
        /// 所属应用名称
        /// </summary>
        public string application_name { get; set; }

        /// <summary>
        /// 功能类型
        /// </summary>
        public string function_type { get; set; }

        /// <summary>
        /// 功能类型
        /// </summary>
        public string function_name { get; set; }

        /// <summary>
        /// 是否已经赋值可操作权限
        /// </summary>
        public bool lay_is_checked{ get; set; }

        /// <summary>
        /// 子节点
        /// </summary>
        public List<MenuTreeInfo> children { get; set; }

    }
}
