﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 订单状态名称  100:待付款  110:已取消 120:已付款/待发货 130:已发货 140:已完成（待评价） 150:已完成（已经评价）160:售后申请中
    /// </summary>
    public enum OrderStatus
    {
        /// <summary>
        /// 待付款
        /// </summary>
        [Description("待付款")]
        ForPayment = 100,

        /// <summary>
        /// 已取消
        /// </summary>
        [Description("已取消")]
        Canceled = 110,

        /// <summary>
        /// 待发货
        /// </summary>
        [Description("待发货")]
        ForShipments = 120,

        /// <summary>
        /// 已发货
        /// </summary>
        [Description("已发货")]
        Delivered = 130,

        /// <summary>
        /// 已完成
        /// </summary>
        [Description("已完成")]
        Completed = 140,

        /// <summary>
        /// 已经评价
        /// </summary>
        [Description("已经评价")]
        HaveEvaluation = 150,

        /// <summary>
        /// 售后申请中
        /// </summary>
        [Description("售后申请中")]
        AfterSale = 160,

    }
}
