﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 节点是否可选审批人员
    /// </summary>
    public enum FlowStepChooseType
    {
        /// <summary>
        /// 100可选
        /// </summary>
        [Description("可选")]
        Choosable = 100,

        /// <summary>
        /// 110固定
        /// </summary>
        [Description("固定")]
        Immobilization = 110,

    }
}
