﻿/*
* 命名空间: Common.Model
*
* 功 能： 服务操作类型
*
* 类 名： ServiceOperationType
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/5/11 17:09:42 	Harvey    创建
* ─────────────────────────────────────────────────
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 服务操作类型 【10安装、20启动、30停止、40卸载】
    /// </summary>
    public enum ServiceOperationType
    {
        /// <summary>
        /// 安装
        /// </summary>
        [Description("安装")]
        Install = 10,

        /// <summary>
        /// 20 启动
        /// </summary>
        [Description("启动")]
        Running = 20,

        /// <summary>
        /// 30 停止
        /// </summary>
        [Description("停止")]
        Stop = 30,

        /// <summary>
        /// 40卸载 
        /// </summary>
        [Description("卸载 ")]
        Uninstall = 40,
    }
}
