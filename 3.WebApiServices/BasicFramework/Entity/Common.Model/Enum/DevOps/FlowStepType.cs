﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 流程步骤类型
    /// </summary>
    public enum FlowStepType
    {

        /// <summary>
        /// 100起始步骤
        /// </summary>
        [Description("起始步骤")]
        Initialized = 100,

        /// <summary>
        /// 110审批步骤
        /// </summary>
        [Description("审批步骤")]
        Examine = 110,

        /// <summary>
        /// 120结束步骤
        /// </summary>
        [Description("结束步骤")]
         Finish = 120,
    }
}
