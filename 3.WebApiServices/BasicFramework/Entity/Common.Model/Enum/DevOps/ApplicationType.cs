﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 应用类型
    /// </summary>
    public enum ApplicationType
    {
        /// <summary>
        /// APP
        /// </summary>
        [Description("APP")]
        App = 100,

        /// <summary>
        /// 微信
        /// </summary>
        [Description("微信")]
        Wx = 200,

        /// <summary>
        /// Web
        /// </summary>
        [Description("Web")]
        Web = 300,
        /// <summary>
        /// 桌面
        /// </summary>
        [Description("桌面")]
        PC = 400,
    }
}
