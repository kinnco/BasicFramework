﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 流程任务状态
    /// </summary>
    public enum FlowTaskStatus
    {
        //任务状态 100等待中 110未处理 120处理中 130已完成
        /// <summary>
        /// 100等待中
        /// </summary>
        [Description("等待中")]
        Waiting = 100,

        /// <summary>
        /// 110未处理
        /// </summary>
        [Description("未处理")]
        Untreated = 110,

        /// <summary>
        /// 120处理中
        /// </summary>
        [Description("处理中")]
        Processing = 120,

        /// <summary>
        /// 130已完成
        /// </summary>
        [Description("已完成")]
        Completed = 130,

        /// <summary>
        /// 140未生效
        /// </summary>
        [Description("未生效")]
        Invalid = 140,
        
    }
}
