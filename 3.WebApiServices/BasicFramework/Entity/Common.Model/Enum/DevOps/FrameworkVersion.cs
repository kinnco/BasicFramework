﻿/*
* 命名空间: Common.Model
*
* 功 能： 框架版本
*
* 类 名： FrameworkVersion
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/5/9 14:37:29 	Harvey    创建
* ─────────────────────────────────────────────────
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 框架版本 框架版本【4,2,0 表示无托管】
    /// </summary>
    public enum FrameworkVersion
    {
        /// <summary>
        /// 无托管代码
        /// </summary>
        [Description("无托管代码")]
        VersionZero = 0,

        /// <summary>
        /// Framework 2.0
        /// </summary>
        [Description("v2.0")]
        VersionTwo = 2,

        /// <summary>
        /// Framework 4.0
        /// </summary>
        [Description("v4.0")]
        RoleManage = 4,
    }
}
