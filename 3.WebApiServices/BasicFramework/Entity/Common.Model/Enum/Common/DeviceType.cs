﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 设备类型
    /// </summary>
    public enum DeviceType
    {
        /// <summary>
        ///微信端
        /// </summary>
        [Description("微信端")]
        WeChat = 100,

        /// <summary>
        ///APP端
        /// </summary>
        [Description("APP端")]
        APP = 110,

        /// <summary>
        ///PC前端
        /// </summary>
        [Description("PC前端")]
        PCFront = 120,

        /// <summary>
        ///PC后端
        /// </summary>
        [Description("PC后端")]
        PCBack = 130,
    }
}
