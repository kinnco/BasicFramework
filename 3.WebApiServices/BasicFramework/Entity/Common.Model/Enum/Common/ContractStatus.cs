﻿

using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 合同状态  未生效/执行中、结束、意外中止。
    /// </summary>
    public enum ContractStatus
    {
        /// <summary>
        /// 未生效
        /// </summary>
        [Description("未生效")]
        Invalid = 100,

        /// <summary>
        /// 执行中
        /// </summary>
        [Description("执行中")]
        InEecution = 110,

        /// <summary>
        /// 意外终止
        /// </summary>
        [Description("意外终止")]
        Terminate = 120,

        /// <summary>
        /// 结束
        /// </summary>
        [Description("结束")]
        Finish = 130,

    }
}