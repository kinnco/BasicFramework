﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// Sde空间数据库表操作类型
    /// </summary>
    public enum SdeProcessingType
    {

        /// <summary>
        /// 插入
        /// </summary>
        [Description("插入")]
        Insert = 100,

        /// <summary>
        /// 更新
        /// </summary>
        [Description("更新")]
        Modify = 110,


        /// <summary>
        /// 删除
        /// </summary>
        [Description("删除")]
        Remove = 120,


    }
}
