﻿using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 操作日志中的业务标题
    /// </summary>
    public enum BusinessTitleType
    {

        #region 运维服务的功能模块

        /// <summary>
        /// 参数设置管理
        /// </summary>
        [Description("参数设置管理")]
        ParameterSettingManage = 100,

        /// <summary>
        /// 日志信息管理
        /// </summary>
        [Description("日志信息管理")]
        LogManage = 101,

        /// <summary>
        /// 公用逻辑操作
        /// </summary>
        [Description("公用逻辑操作")]
        CommonLogicManage = 102,

        /// <summary>
        /// 消息逻辑操作
        /// </summary>
        [Description("消息逻辑操作")]
        MessageManage = 103,

        /// <summary>
        /// 消息逻辑操作
        /// </summary>
        [Description("接口访问记录逻辑操作")]
        ApiLogManage = 104,


        /// <summary>
        /// 功能信息管理
        /// </summary>
        [Description("功能信息管理")]
        FunctionManage = 110,

        /// <summary>
        /// 角色信息管理
        /// </summary>
        [Description("角色信息管理")]
        RoleManage = 111,

        /// <summary>
        /// 组织信息管理
        /// </summary>
        [Description("组织信息管理")]
        DepartmentManage =112,

        /// <summary>
        /// 岗位信息管理
        /// </summary>
        [Description("岗位信息管理")]
        PostManage = 113,

        /// <summary>
        /// 用户信息管理
        /// </summary>
        [Description("用户信息管理")]
        UserManage = 114,


        /// <summary>
        /// 服务管理操作
        /// </summary>
        [Description("服务管理操作")]
        ServiceManage =120,

        /// <summary>
        /// 应用管理
        /// </summary>
        [Description("应用管理逻辑操作")]
        Application = 121,

        /// <summary>
        /// 接口信息逻辑操作
        /// </summary>
        [Description("接口信息逻辑操作")]
        SysServerApiInfo = 123,

        /// <summary>
        ///流程管理-类型管理逻辑操作
        /// </summary>
        [Description("流程管理-类型管理逻辑操作")]
        TypeManag = 130,

        /// <summary>
        ///流程管理-按钮管理逻辑操作
        /// </summary>
        [Description("流程管理-按钮管理逻辑操作")]
        ButtonManag = 131,

        /// <summary>
        ///流程管理-流程管理逻辑操作
        /// </summary>
        [Description("流程管理-流程管理逻辑操作")]
        ProcessManagement = 132,

        #endregion

        #region 官网服务的功能模块

        /// <summary>
        /// 管网基础设置管理
        /// </summary>
        [Description("管网基础设置管理")]
        WebBasicSettingManage = 200,


        #endregion

        #region 商城服务的功能模块

        /// <summary>
        /// 商城用户管理
        /// </summary>
        [Description("商城用户管理")]
        MallClientManage = 300,

        /// <summary>
        /// 商品分类管理
        /// </summary>
        [Description("商品分类管理")]
        GoodsCategoriesManage = 301,

        /// <summary>
        /// 商品信息管理
        /// </summary>
        [Description("商品信息管理")]
        GoodsInfoManage = 302,


        /// <summary>
        /// 客户信息管理
        /// </summary>
        [Description("客户信息管理")]
        ClientInfoManage = 303,

        /// <summary>
        /// 订单信息管理
        /// </summary>
        [Description("订单信息管理")]
        OrderInfoManage = 304,
        #endregion
    }
}
