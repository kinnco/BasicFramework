﻿/*
* 命名空间: Common.Model
*
* 功 能： 接口请求返回码
*
* 类 名： ActionCodes
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/13 15:57:22 Harvey     创建
*
* Copyright (c) 2020 SCZH Corporation. All rights reserved.
*/
using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 接口请求返回码
    /// </summary>
    public enum ActionCodes : int
    {
        /// <summary>
        /// 成功
        /// </summary>
        [Description("成功")]
        Success = 1000,

        /// <summary>
        /// token无效【暂时无用】
        /// </summary>
        [Description("token无效")]
        TokenInvalid = 1101,

        /// <summary>
        /// RefreshTokeToken无效【Jwt使用】
        /// </summary>
        [Description("RefreshTokeToken无效")]
        RefreshTokeTokenInvalid = 1102,

        /// <summary>
        /// 信息过期【缓存过期，Jwt过期】
        /// </summary>
        [Description("信息过期")]
        InfoExpiration = 1103,

        /// <summary>
        /// 对象不存在
        /// </summary>
        [Description("对象不存在")]
        ObjectNotFound = 1201,

        /// <summary>
        /// 对象已存在
        /// </summary>
        [Description("对象已存在")]
        ObjectExists = 1203,

        /// <summary>
        /// 无效操作
        /// </summary>
        [Description("无效操作")]
        InvalidOperation = 1205,

        /// <summary>
        /// 参数为空
        /// </summary>
        [Description("参数为空")]
        ArgumentNull = 2101,

        /// <summary>
        /// 参数无效
        /// </summary>
        [Description("参数无效")]
        ArgumentInvalid = 2102,

        /// <summary>
        /// 系统错误【不能用在业务返回中】
        /// </summary>
        [Description("系统错误")]
        SystemError = 2103,

        /// <summary>
        ///缓存失败【不能用在业务返回中】
        /// </summary>
        [Description("缓存失败")]
        CacheFailure = 3000,

        /// <summary>
        ///账户被冻结【不能用在业务返回中】
        /// </summary>
        [Description("账户被冻结")]
        AccountBlocked = 4000,

        /// <summary>
        ///账户不存在【不能用在业务返回中】
        /// </summary>
        [Description("账户不存在")]
        AccountNotExist = 4001,

        /// <summary>
        ///非法操作
        /// </summary>
        [Description("非法操作")]
        IllegalOperation = 5000,
    }
}
