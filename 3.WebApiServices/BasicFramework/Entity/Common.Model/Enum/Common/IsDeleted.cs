﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;


/*
* 命名空间:  Common.Model
*
* 功 能：是否被删除
*
* 类 名： IsDeleted
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/11/27 14:40:40  Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 是否被删除
    /// </summary>
    public enum IsDeleted
    {
        /// <summary>
        ///正常
        /// </summary>
        [Description("正常")]
        NotDeleted = 100,

        /// <summary>
        /// 被删除
        /// </summary>
        [Description("被删除")]
        Deleted = 110,
    }
}
