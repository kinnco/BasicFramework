/*
* 命名空间: Mall.Model
*
* 功 能： MallAboutUs实体类
*
* 类 名： MallAboutUsEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/10/30 11:36:42 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 关于我们
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "MALL_ABOUT_US")]
    public class MallAboutUsEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public MallAboutUsEntity()
        {
            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 关于我们的信息
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARKS", Required = true, IsPrimarykey = false)]
        public string remarks
        {
            get; set;
        }
    }
}
