/*
* 命名空间:DevOps.Model
*
* 功 能： SysApiMonitorLog实体类
*
* 类 名： SysApiMonitorLogEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/10 15:55:39 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 接口服务  接口请求记录信息表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SAFE_API_MONITOR_LOG")]
    public class SafeApiMonitorLogEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SafeApiMonitorLogEntity()
     {
            //服务名称
            this.service_name = string.Empty;

            //控制器名称
            this.controller_name = string.Empty;

           //Action名称
           this.action_name = string.Empty;

           //action参数
           this.action_params = string.Empty;

           //唯一标识符
           this.id = Guid.NewGuid().ToString().Replace("-", "");

            //请求开始时间
            this.start_time = DateTime.Now;

           //请求结束时间
           this.end_time = DateTime.Now;

           //Http请求头
           this.http_header = string.Empty;

           //请求的IP地址
           this.request_ip = string.Empty;

           //请求结果返回码
           this.result_code = string.Empty;

           //请求路径
           this.request_path = string.Empty;

           //Http请求方式
           this.http_method = string.Empty;
        }

        /// <summary>
        /// 控制器名称
        /// </summary>
        [DBFieldInfo(ColumnName = "SERVICE_NAME", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string service_name
        {
            get; set;
        }

        /// <summary>
        /// 控制器名称
        /// </summary>
        [DBFieldInfo(ColumnName = "CONTROLLER_NAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string controller_name
        {
            get; set;
        }

        /// <summary>
        /// Action名称
        /// </summary>
        [DBFieldInfo(ColumnName = "ACTION_NAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string action_name
        {
            get; set;
        }

        /// <summary>
        /// action参数
        /// </summary>
        [DBFieldInfo(ColumnName = "ACTION_PARAMS",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string action_params
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 请求开始时间
        /// </summary>
        [DBFieldInfo(ColumnName = "START_TIME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime start_time
        {
            get; set;
        }

        /// <summary>
        /// 请求结束时间
        /// </summary>
        [DBFieldInfo(ColumnName = "END_TIME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime end_time
        {
            get; set;
        }

        /// <summary>
        /// Http请求头
        /// </summary>
        [DBFieldInfo(ColumnName = "HTTP_HEADER",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string http_header
        {
            get; set;
        }

        /// <summary>
        /// 请求的IP地址
        /// </summary>
        [DBFieldInfo(ColumnName = "REQUEST_IP",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string request_ip
        {
            get; set;
        }

        /// <summary>
        /// 请求结果返回码
        /// </summary>
        [DBFieldInfo(ColumnName = "RESULT_CODE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string result_code
        {
            get; set;
        }

        /// <summary>
        /// 请求路径
        /// </summary>
        [DBFieldInfo(ColumnName = "REQUEST_PATH",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string request_path
        {
            get; set;
        }

        /// <summary>
        /// Http请求方式
        /// </summary>
        [DBFieldInfo(ColumnName = "HTTP_METHOD",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string http_method
        {
            get; set;
        }
    }
}
