/*
* 命名空间: DevOps.Model
*
* 功 能： SysUserPostRe实体类
*
* 类 名： SysUserPostReEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 用户-岗位系表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_USER_POST_RE")]
    public class SysUserPostReEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysUserPostReEntity()
     {

           //用户唯一标识符号
           this.user_id = string.Empty;

           //职位唯一标识符
           this.post_id = string.Empty;

           //唯一标识符
           this.id = Guid.NewGuid().ToString().Replace("-", "");
        }

        /// <summary>
        /// 用户唯一标识符号
        /// </summary>
        [DBFieldInfo(ColumnName = "USER_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string user_id
        {
            get; set;
        }

        /// <summary>
        /// 职位唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "POST_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string post_id
        {
            get; set;
        }

        /// <summary>
       ///岗位唯一标识符        /// </summary>
        [DBFieldInfo(ColumnName = "ID",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }
    }
}
