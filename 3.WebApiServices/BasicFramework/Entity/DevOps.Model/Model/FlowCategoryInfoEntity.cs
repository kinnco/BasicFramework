/*
* 命名空间: DevOps.Model
*
* 功 能： FlowFlowCategory实体类
*
* 类 名： FlowFlowCategoryEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/01/14 09:25:28 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using Dapper.Library;
    using System;

    /// <summary>
    /// 流程分类
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "FLOW_CATEGORY_INFO")]
    public class FlowCategoryInfoEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public FlowCategoryInfoEntity()
     {

           //唯一标识符
           this.id = string.Empty;

           //名称
           this.name = string.Empty;

           //代码
           this.code = string.Empty;

           //排序
           this.sort = 0;

           //备注
           this.remark = string.Empty;

           //父类id
           this.parent_id = string.Empty;
      }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 名称
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME",Required = false,IsPrimarykey = false,IsIncrease =false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 代码
        /// </summary>
        [DBFieldInfo(ColumnName = "CODE",Required = false,IsPrimarykey = false,IsIncrease =false)]
        public string code
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT",Required = false,IsPrimarykey = false,IsIncrease =false)]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARK",Required = false,IsPrimarykey = false,IsIncrease =false)]
        public string remark
        {
            get; set;
        }

        /// <summary>
        /// 父类id
        /// </summary>
        [DBFieldInfo(ColumnName = "PARENT_ID",Required = false,IsPrimarykey = false,IsIncrease =false)]
        public string parent_id
        {
            get; set;
        }
    }
}
