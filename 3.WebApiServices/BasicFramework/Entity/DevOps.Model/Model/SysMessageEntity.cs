/*
* 命名空间: DevOps.Model
*
* 功 能： SysMessage实体类
*
* 类 名： SysMessageEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/08 09:51:39 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 消息实体类
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_MESSAGE")]
    public class SysMessageEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysMessageEntity()
     {

           //信息标题
           this.title = string.Empty;

           //信息类型 100：对话信息 101：系统消息:
           this.msg_type = 100;

           //信息类容
           this.content = string.Empty;

           //状态 100-未读，101-已读 ，102-删除
           this.status = 100;

           //创建日期
           this.create_date = DateTime.Now;

           //发送者名称
           this.sender_name = string.Empty;

           //发送者ID
           this.sender_id = string.Empty;

           //接受者名称
           this.receiver_name = string.Empty;

           //接受者唯一编码
           this.receiver_id = string.Empty;

           //唯一编码
           this.id = Guid.NewGuid().ToString().Replace("-", "");

            //是否被逻辑删除
            this.is_deleted = false;
      }

        /// <summary>
        /// 信息标题
        /// </summary>
        [DBFieldInfo(ColumnName = "TITLE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string title
        {
            get; set;
        }

        /// <summary>
        /// 信息类型 100：对话信息 101：系统消息:
        /// </summary>
        [DBFieldInfo(ColumnName = "MSG_TYPE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public int msg_type
        {
            get; set;
        }

        /// <summary>
        /// 信息类容
        /// </summary>
        [DBFieldInfo( ColumnName = "CONTENT",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string content
        {
            get; set;
        }

        /// <summary>
        /// 状态 100-未读，101-已读 ，102-删除
        /// </summary>
        [DBFieldInfo(ColumnName = "STATUS",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public int status
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo( ColumnName = "CREATE_DATE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 发送者名称
        /// </summary>
        [DBFieldInfo(ColumnName = "SENDER_NAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string sender_name
        {
            get; set;
        }

        /// <summary>
        /// 发送者ID
        /// </summary>
        [DBFieldInfo(ColumnName = "SENDER_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string sender_id
        {
            get; set;
        }

        /// <summary>
        /// 接受者名称
        /// </summary>
        [DBFieldInfo(ColumnName = "RECEIVER_NAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string receiver_name
        {
            get; set;
        }

        /// <summary>
        /// 接受者唯一编码
        /// </summary>
        [DBFieldInfo(ColumnName = "RECEIVER_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string receiver_id
        {
            get; set;
        }

        /// <summary>
        /// 唯一编码
        /// </summary>
        [DBFieldInfo(ColumnName = "ID",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 是否被逻辑删除
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_DELETED",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_deleted
        {
            get; set;
        }
    }
}
