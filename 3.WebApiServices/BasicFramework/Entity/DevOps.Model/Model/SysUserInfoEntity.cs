/*
* 命名空间: DevOps.Model
*
* 功 能： SysUserInfo实体类
*
* 类 名： SysUserInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 用户信息表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_USER_INFO")]
    public class SysUserInfoEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysUserInfoEntity()
     {

           //用户描述
           this.user_describe = string.Empty;

           //唯一标识符
           this.id = Guid.NewGuid().ToString().Replace("-", "");

            //用户名【登录名】
            this.name = string.Empty;

           //密码
           this.password = string.Empty;

           //性别
           this.gender = string.Empty;

           //用户头像地址
           this.picture_url = string.Empty;

           //修改者
           this.modifier_id = string.Empty;

           //用户家庭电话
           this.telphone = string.Empty;

           //用户手机号码
           this.mobile_phone = string.Empty;

           //排序编号
           this.sort = 0;

           //QQ号
           this.qq = string.Empty;

           //用户昵称
           this.nickname = string.Empty;

           //用户真实名
           this.realname = string.Empty;

           //用户邮箱
           this.email = string.Empty;

           //办公电话
           this.office_phone = string.Empty;

           //用户身份号码
           this.idnumber = string.Empty;

           //系统隐藏备注
           this.remarks = string.Empty;

           //创建者
           this.creator_id = string.Empty;

           //创建者姓名
           this.creator_name = string.Empty;

           //创建日期
           this.create_date = DateTime.Now;

           //修改者姓名
           this.modifier_name = string.Empty;

           //修改者日期
           this.modifier_date = DateTime.Now;

           //是否有效
           this.is_valid = true;

           //是否登录
           this.is_login = false;

           //是否被逻辑删除
           this.is_deleted = false;

           //是否是超级管理员
           this.is_super_user = false;

           //用户对应主题
           this.theme = string.Empty;

            //用户签章图片地址
            this.signature_url = string.Empty;

        }

        /// <summary>
        /// 用户描述
        /// </summary>
        [DBFieldInfo(ColumnName = "USER_DESCRIBE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string user_describe
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 用户名【登录名】
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string name
        {
            get; set;
        }


        /// <summary>
        /// 密码
        /// </summary>
        [DBFieldInfo(ColumnName = "PASSWORD",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string password
        {
            get; set;
        }

        /// <summary>
        /// 性别
        /// </summary>
        [DBFieldInfo(ColumnName = "GENDER",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string gender
        {
            get; set;
        }

        /// <summary>
        /// 用户头像地址
        /// </summary>
        [DBFieldInfo(ColumnName = "PICTURE_URL",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string picture_url
        {
            get; set;
        }

        /// <summary>
        /// 修改者
        /// </summary>
        [DBFieldInfo(ColumnName = "MODIFIER_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string modifier_id
        {
            get; set;
        }

        /// <summary>
        /// 用户家庭电话
        /// </summary>
        [DBFieldInfo(ColumnName = "TELPHONE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string telphone
        {
            get; set;
        }

        /// <summary>
        /// 用户手机号码
        /// </summary>
        [DBFieldInfo(ColumnName = "MOBILE_PHONE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// QQ号
        /// </summary>
        [DBFieldInfo(ColumnName = "QQ",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string qq
        {
            get; set;
        }

        /// <summary>
        /// 用户昵称
        /// </summary>
        [DBFieldInfo(ColumnName = "NICKNAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string nickname
        {
            get; set;
        }

        /// <summary>
        /// 用户真实名
        /// </summary>
        [DBFieldInfo(ColumnName = "REALNAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string realname
        {
            get; set;
        }

        /// <summary>
        /// 用户邮箱
        /// </summary>
        [DBFieldInfo(ColumnName = "EMAIL",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 办公电话
        /// </summary>
        [DBFieldInfo(ColumnName = "OFFICE_PHONE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string office_phone
        {
            get; set;
        }

        /// <summary>
        /// 用户身份号码
        /// </summary>
        [DBFieldInfo(ColumnName = "IDNUMBER",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string idnumber
        {
            get; set;
        }

        /// <summary>
        /// 系统隐藏备注
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARKS",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 创建者
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATOR_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string creator_id
        {
            get; set;
        }

        /// <summary>
        /// 创建者姓名
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATOR_NAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string creator_name
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_DATE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 修改者姓名
        /// </summary>
        [DBFieldInfo(ColumnName = "MODIFIER_NAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string modifier_name
        {
            get; set;
        }

        /// <summary>
        /// 修改者日期
        /// </summary>
        [DBFieldInfo(ColumnName = "MODIFIER_DATE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime? modifier_date
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_VALID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否登录
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_LOGIN",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_login
        {
            get; set;
        }

        /// <summary>
        /// 是否被逻辑删除
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_DELETED",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_deleted
        {
            get; set;
        }

        /// <summary>
        /// 是否是超级管理员
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_SUPER_USER",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_super_user
        {
            get; set;
        }

        /// <summary>
        /// 用户对应主题
        /// </summary>
        [DBFieldInfo(ColumnName = "THEME", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string theme
        {
            get; set;
        }

        /// <summary>
        /// 用户签章图片地址
        /// </summary>
        [DBFieldInfo(ColumnName = "SIGNATURE_URL", Required = true, IsPrimarykey = false)]
        public string signature_url
        {
            get; set;
        }

    }
}
