/*
* 命名空间: DevOps.Model
*
* 功 能： View_SysUser视图实体类
*
* 类 名： View_SysUserEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/19 11:05:58 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
     using Dapper.Library;

    /// <summary>
    /// 后端管理员视图
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "VIEW_SYS_USER")]
    public class View_SysUserEntity
    {

        /// <summary>
        /// 用户ID
        /// </summary>
        [DBFieldInfo(ColumnName = "ID")]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 用户名称
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME")]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 密码
        /// </summary>
        [DBFieldInfo(ColumnName = "PASSWORD")]
        public string password
        {
            get; set;
        }

        /// <summary>
        /// 性别
        /// </summary>
        [DBFieldInfo(ColumnName = "GENDER")]
        public string gender
        {
            get; set;
        }

        /// <summary>
        /// 用户家庭电话
        /// </summary>
        [DBFieldInfo(ColumnName = "TELPHONE")]
        public string telphone
        {
            get; set;
        }

        /// <summary>
        /// 用户手机号码
        /// </summary>
        [DBFieldInfo(ColumnName = "MOBILE_PHONE")]
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// qq
        /// </summary>
        [DBFieldInfo(ColumnName = "QQ")]
        public string qq
        {
            get; set;
        }

        /// <summary>
        /// 用户昵称
        /// </summary>
        [DBFieldInfo(ColumnName = "NICKNAME")]
        public string nickname
        {
            get; set;
        }

        /// <summary>
        /// 用户真实名
        /// </summary>
        [DBFieldInfo(ColumnName = "REALNAME")]
        public string realname
        {
            get; set;
        }

        /// <summary>
        /// email
        /// </summary>
        [DBFieldInfo(ColumnName = "EMAIL")]
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 办公电话
        /// </summary>
        [DBFieldInfo(ColumnName = "OFFICE_PHONE")]
        public string office_phone
        {
            get; set;
        }

        /// <summary>
        /// 用户身份号码
        /// </summary>
        [DBFieldInfo(ColumnName = "IDNUMBER")]
        public string idnumber
        {
            get; set;
        }

        /// <summary>
        /// 用户头像地址
        /// </summary>
        [DBFieldInfo(ColumnName = "PICTURE_URL")]
        public string picture_url
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_DATE")]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT")]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 描述
        /// </summary>
        [DBFieldInfo(ColumnName = "USER_DESCRIBE")]
        public string user_describe
        {
            get; set;
        }

        /// <summary>
        /// 所属角色
        /// </summary>
        [DBFieldInfo(ColumnName = "ROLE_IDS")]
        public string role_ids
        {
            get; set;
        }

        /// <summary>
        /// 所属角色名称
        /// </summary>
        [DBFieldInfo(ColumnName = "ROLE_NAMES")]
        public string role_names
        {
            get; set;
        }

        /// <summary>
        /// 所属岗位
        /// </summary>
        [DBFieldInfo(ColumnName = "POST_IDS")]
        public string post_ids
        {
            get; set;
        }

        /// <summary>
        /// 所属岗位名称
        /// </summary>
        [DBFieldInfo(ColumnName = "POST_NAMES")]
        public string post_names
        {
            get; set;
        }

        /// <summary>
        /// 所属部门
        /// </summary>
        [DBFieldInfo(ColumnName = "DEPARTMENT_IDS")]
        public string department_ids
        {
            get; set;
        }

        /// <summary>
        /// 所属部门名称
        /// </summary>
        [DBFieldInfo(ColumnName = "DEPARTMENT_NAMES")]
        public string department_names
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_VALID")]
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否是超级管理员
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_SUPER_USER")]
        public bool is_super_user
        {
            get; set;
        }

        /// <summary>
        /// 用户对应主题
        /// </summary>
        [DBFieldInfo(ColumnName = "THEME")]
        public string theme
        {
            get; set;
        }
    }
}
