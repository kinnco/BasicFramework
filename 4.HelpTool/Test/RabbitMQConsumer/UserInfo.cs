﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQConsumer
{
    public class UserInfo
    {
        public string ID
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
        public int Age
        {
            get; set;
        }
        public DateTime Created
        {
            get; set;
        }
    }
}
