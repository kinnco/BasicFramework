﻿using MongoDB.Library;
using MongoDBTest.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDBTest.Logic
{
    public class MongoDBImpl
    {
        /// <summary>
        /// 获取所有
        /// </summary>
        /// <returns></returns>
        public IList<UserInfo> TestSearchAll()
        {
            var mongoBase = new MongoDBHelper<UserInfo>();
            var result = mongoBase.FindAll();
            return result;
        }

        /// <summary>
        /// 排序查询测试
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IList<UserInfo> TestSearchDataBase(UserInfo user)
        {
            var mongoBase = new MongoDBHelper<UserInfo>();
            var result = mongoBase.FindOrderDataByExpress(p => p.Age == user.Age && p.Name.Contains(user.Name), p => p.Friends, 2);
            return result;
        }


        /// <summary>
        /// 列表查询测试
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IList<UserInfo> TestSearchList(UserInfo user)
        {
            var mongoBase = new MongoDBHelper<UserInfo>();
            var result = mongoBase.FindDataByExpress(p => p.Age == user.Age && p.Name.Contains(user.Name));
            return result;
        }

        /// <summary>
        /// 单个查询
        /// </summary>
        /// <returns></returns>
        public UserInfo TestSearchSingle(UserInfo user)
        {
            var mongoBase = new MongoDBHelper<UserInfo>();
            var result = mongoBase.FindOne<double>(p => p.Friends, user.Friends);
            return result;
        }

        /// <summary>
        /// 单个插入测试
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public UserInfo TestInsert(UserInfo user)
        {
            var mongoBase = new MongoDBHelper<UserInfo>();
            var result = mongoBase.InsertData(user);
            return result;
        }


        /// <summary>
        /// 单个插入测试
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<UserInfo> TestInsertMany(List<UserInfo> user)
        {
            var mongoBase = new MongoDBHelper<UserInfo>();
            var result = mongoBase.InsertManyData(user);
            return result;
        }

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public long TestUpdateOne(string oldName, string newName)
        {
            var mongoBase = new MongoDBHelper<UserInfo>();
            var result = mongoBase.UpdateMany<string, string>(p => p.Name, oldName, p => p.Name, newName);
            return result;
        }

        /// <summary>
        /// 根据条件删除
        /// </summary>
        /// <returns></returns>
        public long TestDeleteMany(string name)
        {
            var mongoBase = new MongoDBHelper<UserInfo>();
            var result = mongoBase.DeleteMany(p => p.Name.Contains(name));
            return result;

        }

        /// <summary>
        /// 插入自增表
        /// </summary>
        /// <param name="autoIncrement"></param>
        /// <returns></returns>
        internal object TestInsertIncrement(AutoIncrement autoIncrement)
        {
            var mongoBase = new MongoDBHelper<AutoIncrement>();
            var result = mongoBase.InsertData(autoIncrement);
            return result;
        }

        /// <summary>
        /// 插入自增列
        /// </summary>
        /// <param name="autoIncrement"></param>
        /// <returns></returns>
        public AutoIncrement TestInsertModify(AutoIncrement autoIncrement)
        {
            var mongoBase = new MongoDBHelper<AutoIncrement>();
            var result = mongoBase.InsertNextSequence<string, int>(
                p => p.TableMark, "TableMark",
                p => p.seq,
                p =>
                {
                    autoIncrement.seq=p.seq;
                    return autoIncrement;
                });
            return result;
        }
    }
}
