﻿namespace MongoDBTest
{
    partial class MongoDB
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_modified = new System.Windows.Forms.Button();
            this.btn_searchAll = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btn_remove = new System.Windows.Forms.Button();
            this.txt_newCondition = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_condition = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_conditionUpdate = new System.Windows.Forms.Button();
            this.btn_manyInsert = new System.Windows.Forms.Button();
            this.btn_searchSingle = new System.Windows.Forms.Button();
            this.btn_searchlist = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_insert = new System.Windows.Forms.Button();
            this.txt_age = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.lab_name = new System.Windows.Forms.Label();
            this.btn_link = new System.Windows.Forms.Button();
            this.txt_show = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_modified
            // 
            this.btn_modified.Location = new System.Drawing.Point(113, 246);
            this.btn_modified.Name = "btn_modified";
            this.btn_modified.Size = new System.Drawing.Size(100, 23);
            this.btn_modified.TabIndex = 44;
            this.btn_modified.Text = "实现ID自增插入";
            this.btn_modified.UseVisualStyleBackColor = true;
            this.btn_modified.Click += new System.EventHandler(this.btn_modified_Click_1);
            // 
            // btn_searchAll
            // 
            this.btn_searchAll.Location = new System.Drawing.Point(14, 188);
            this.btn_searchAll.Name = "btn_searchAll";
            this.btn_searchAll.Size = new System.Drawing.Size(76, 23);
            this.btn_searchAll.TabIndex = 43;
            this.btn_searchAll.Text = "查询所有";
            this.btn_searchAll.UseVisualStyleBackColor = true;
            this.btn_searchAll.Click += new System.EventHandler(this.btn_searchAll_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(386, 13);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(137, 272);
            this.textBox2.TabIndex = 42;
            this.textBox2.Text = "1、排序查询、列表查询根据姓名和年龄精确匹配；\r\n2、单个查询根据姓名、年龄和朋友个数精确匹配；\r\n3、批量插入不需要获取信息，直接6个随机值；\r\n4、条件更新根" +
    "据姓名条件和姓名新信息操作；\r\n5、条件删除根据姓名操作。\r\n";
            // 
            // btn_remove
            // 
            this.btn_remove.Location = new System.Drawing.Point(304, 217);
            this.btn_remove.Name = "btn_remove";
            this.btn_remove.Size = new System.Drawing.Size(76, 23);
            this.btn_remove.TabIndex = 41;
            this.btn_remove.Text = "条件删除";
            this.btn_remove.UseVisualStyleBackColor = true;
            this.btn_remove.Click += new System.EventHandler(this.btn_remove_Click_1);
            // 
            // txt_newCondition
            // 
            this.txt_newCondition.Location = new System.Drawing.Point(281, 161);
            this.txt_newCondition.Name = "txt_newCondition";
            this.txt_newCondition.Size = new System.Drawing.Size(99, 21);
            this.txt_newCondition.TabIndex = 40;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(205, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 39;
            this.label4.Text = "姓名新信息：";
            // 
            // txt_condition
            // 
            this.txt_condition.Location = new System.Drawing.Point(73, 159);
            this.txt_condition.Name = "txt_condition";
            this.txt_condition.Size = new System.Drawing.Size(116, 21);
            this.txt_condition.TabIndex = 38;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 37;
            this.label3.Text = "姓名条件：";
            // 
            // btn_conditionUpdate
            // 
            this.btn_conditionUpdate.Location = new System.Drawing.Point(208, 217);
            this.btn_conditionUpdate.Name = "btn_conditionUpdate";
            this.btn_conditionUpdate.Size = new System.Drawing.Size(76, 23);
            this.btn_conditionUpdate.TabIndex = 36;
            this.btn_conditionUpdate.Text = "条件更新";
            this.btn_conditionUpdate.UseVisualStyleBackColor = true;
            this.btn_conditionUpdate.Click += new System.EventHandler(this.btn_conditionUpdate_Click_1);
            // 
            // btn_manyInsert
            // 
            this.btn_manyInsert.Location = new System.Drawing.Point(113, 217);
            this.btn_manyInsert.Name = "btn_manyInsert";
            this.btn_manyInsert.Size = new System.Drawing.Size(76, 23);
            this.btn_manyInsert.TabIndex = 35;
            this.btn_manyInsert.Text = "批量插入";
            this.btn_manyInsert.UseVisualStyleBackColor = true;
            this.btn_manyInsert.Click += new System.EventHandler(this.btn_manyInsert_Click_1);
            // 
            // btn_searchSingle
            // 
            this.btn_searchSingle.Location = new System.Drawing.Point(304, 188);
            this.btn_searchSingle.Name = "btn_searchSingle";
            this.btn_searchSingle.Size = new System.Drawing.Size(76, 23);
            this.btn_searchSingle.TabIndex = 34;
            this.btn_searchSingle.Text = "单个查询";
            this.btn_searchSingle.UseVisualStyleBackColor = true;
            this.btn_searchSingle.Click += new System.EventHandler(this.btn_searchSingle_Click_1);
            // 
            // btn_searchlist
            // 
            this.btn_searchlist.Location = new System.Drawing.Point(208, 188);
            this.btn_searchlist.Name = "btn_searchlist";
            this.btn_searchlist.Size = new System.Drawing.Size(76, 23);
            this.btn_searchlist.TabIndex = 33;
            this.btn_searchlist.Text = "列表查询";
            this.btn_searchlist.UseVisualStyleBackColor = true;
            this.btn_searchlist.Click += new System.EventHandler(this.btn_searchlist_Click_1);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(327, 132);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(53, 21);
            this.textBox1.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(265, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 31;
            this.label2.Text = "朋友个数：";
            // 
            // btn_insert
            // 
            this.btn_insert.Location = new System.Drawing.Point(14, 217);
            this.btn_insert.Name = "btn_insert";
            this.btn_insert.Size = new System.Drawing.Size(76, 23);
            this.btn_insert.TabIndex = 30;
            this.btn_insert.Text = "单个插入";
            this.btn_insert.UseVisualStyleBackColor = true;
            this.btn_insert.Click += new System.EventHandler(this.btn_insert_Click_1);
            // 
            // txt_age
            // 
            this.txt_age.Location = new System.Drawing.Point(195, 132);
            this.txt_age.Name = "txt_age";
            this.txt_age.Size = new System.Drawing.Size(53, 21);
            this.txt_age.TabIndex = 29;
            this.txt_age.Text = "22";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(160, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 28;
            this.label1.Text = "年龄：";
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(49, 132);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(88, 21);
            this.txt_name.TabIndex = 27;
            this.txt_name.Text = "LW";
            // 
            // lab_name
            // 
            this.lab_name.AutoSize = true;
            this.lab_name.Location = new System.Drawing.Point(12, 135);
            this.lab_name.Name = "lab_name";
            this.lab_name.Size = new System.Drawing.Size(41, 12);
            this.lab_name.TabIndex = 26;
            this.lab_name.Text = "姓名：";
            // 
            // btn_link
            // 
            this.btn_link.Location = new System.Drawing.Point(113, 188);
            this.btn_link.Name = "btn_link";
            this.btn_link.Size = new System.Drawing.Size(76, 23);
            this.btn_link.TabIndex = 24;
            this.btn_link.Text = "排序查询";
            this.btn_link.UseVisualStyleBackColor = true;
            // 
            // txt_show
            // 
            this.txt_show.Location = new System.Drawing.Point(12, 12);
            this.txt_show.Multiline = true;
            this.txt_show.Name = "txt_show";
            this.txt_show.Size = new System.Drawing.Size(368, 114);
            this.txt_show.TabIndex = 23;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(14, 246);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(76, 23);
            this.button1.TabIndex = 45;
            this.button1.Text = "创建自增表";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MongoDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 297);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_modified);
            this.Controls.Add(this.btn_searchAll);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.btn_remove);
            this.Controls.Add(this.txt_newCondition);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_condition);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_conditionUpdate);
            this.Controls.Add(this.btn_manyInsert);
            this.Controls.Add(this.btn_searchSingle);
            this.Controls.Add(this.btn_searchlist);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_insert);
            this.Controls.Add(this.txt_age);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_name);
            this.Controls.Add(this.lab_name);
            this.Controls.Add(this.btn_link);
            this.Controls.Add(this.txt_show);
            this.Name = "MongoDB";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_modified;
        private System.Windows.Forms.Button btn_searchAll;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btn_remove;
        private System.Windows.Forms.TextBox txt_newCondition;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_condition;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_conditionUpdate;
        private System.Windows.Forms.Button btn_manyInsert;
        private System.Windows.Forms.Button btn_searchSingle;
        private System.Windows.Forms.Button btn_searchlist;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_insert;
        private System.Windows.Forms.TextBox txt_age;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label lab_name;
        private System.Windows.Forms.Button btn_link;
        private System.Windows.Forms.TextBox txt_show;
        private System.Windows.Forms.Button button1;
    }
}

