﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDBTest.Entity
{
    [Description("UserInfo")]
    public class UserInfo
    {
        public BsonObjectId _id;//这个对应了 MongoDB.Bson.ObjectId 


        [BsonDefaultValue(defaultValue: "0")]
        [BsonElement(elementName: "name")]
        public string Name { get; set; }

        [BsonDefaultValue(defaultValue: "0")]
        [BsonElement(elementName: "age")]
        public double Age { set; get; }

        [BsonDefaultValue(defaultValue: "0")]
        [BsonElement(elementName: "friends")]
        public double Friends { set; get; }
    }
}
