﻿
using System.Collections.Generic;

namespace Document.Library
{
    /// <summary>
    /// 压缩配置
    /// </summary>
    public class ZipConfig
    {
        /// <summary>
        /// 不公开默认构造函数，不能通过new来创建，请使用InitZipConifg创建对象实例
        /// </summary>
        private ZipConfig() { }

        /// <summary>
        /// 想要压缩的目录路径
        /// </summary>
        public string DirectoryPath { get; set; } = string.Empty;

        /// <summary>
        /// 压缩好的文件的输出路径
        /// </summary>
        public string OutputFilePath { get; set; } = string.Empty;

        /// <summary>
        /// 是否压缩子目录：默认为true
        /// </summary>
        public bool IsIncludeSubDir { get; set; } = true;

        /// <summary>
        /// 压缩等级：默认6
        /// <para>0-9 仅针对SharpZipLib的实现有效。0表示存储 9表示质量最好</para>
        /// </summary>
        public int ZipLevel { get; set; } = 6;

        /// <summary>
        /// 想要压缩的单个文件路径
        /// </summary>
        public string ToZipedSingleFilePath { get; set; } = string.Empty;

        /// <summary>
        /// 解压密码
        /// </summary>
        public string Password { get; set; } = null;

        /// <summary>
        /// 忽略文件类型
        /// </summary>
        public List<string> IgnoreFiles { get; set; }

        /// <summary>
        /// 初始化配置
        /// </summary>
        /// <param name="password">解压密码</param>
        /// <param name="ignoreFiles">忽略文件类型</param>
        /// <param name="directoryPath">要压缩的目录路径</param>
        /// <param name="outputFilePath">输出文件路径</param>
        /// <param name="isIncludeSubDir">是否压缩子目录</param>
        /// <param name="zipLevel">压缩等级</param>
        /// <param name="toZipedSingleFilePath">想要压缩的单个文件路径</param>
        /// <returns>配置</returns>
        public static ZipConfig InitZipConifg(string[] ignoreFiles = null, string password = null, string directoryPath = null, string outputFilePath = null, bool? isIncludeSubDir = null, int? zipLevel = null, string toZipedSingleFilePath = null)
        {
            ZipConfig conf = new ZipConfig();
            if (ignoreFiles!=null)
            {
                conf.IgnoreFiles = new List<string>(ignoreFiles);
            }
            conf.Password = password;
            conf.DirectoryPath = directoryPath ?? string.Empty;
            conf.IsIncludeSubDir = isIncludeSubDir ?? false;
            conf.OutputFilePath = outputFilePath ?? string.Empty;
            conf.ToZipedSingleFilePath = toZipedSingleFilePath ?? string.Empty;
            conf.ZipLevel = zipLevel ?? 6;
            return conf;
        }
    }
}
