﻿using System.ComponentModel;

namespace RabbitMQ.Library.Enum
{
    /// <summary>
    /// 交换机路由类型
    /// </summary>
    public enum ExchangeType
    {
        /// <summary>
        /// Fanout
        /// 所有发送到fanout Exchange的消息，
        /// 都会被转发到与该Exchange 绑定(Binding)的所有Queue上
        /// </summary>
        [Description("fanout")]
        Fanout = 0,

        /// <summary>
        /// Topic
        /// 模糊匹配，
        /// 两种特殊字符“*”与“#”；
        /// “*”用于匹配一个单词，
        /// “#”用于匹配多个单词（可以是零个）
        /// </summary>
        [Description("topic")]
        Topic = 1,

        /// <summary>
        /// Direct
        /// 精确匹配
        /// </summary>
        [Description("direct")]
        Direct = 2,

        /// <summary>
        /// Headers
        /// 不依赖于routing key与binding key的匹配规则来路由消息，
        /// 而是根据发送的消息内容中的headers属性进行匹配
        /// </summary>
        [Description("headers")]
        Headers = 3
    }
}
