﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RabbitMQ.Library
{
    /// <summary>
    /// 消息队列相关配置的DOM。
    /// </summary>
    public class RabbitConfig
    {
        /// <summary>
        /// 消息队列的地址。
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// 端口
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 虚拟机 默认为"/"
        /// </summary>
        public string VirtualHost { get; set; }

        /// <summary>
        /// 客户端默认监听的队列名称
        /// </summary>
        public string ListenQueueName { get; set; }

        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// 交换机信息
        /// </summary>
        public List<ExchangesInfo> Exchanges { get; set; }

        /// <summary>
        /// 队列信息
        /// </summary>
        public List<QueuesInfo> Queues { get; set; }

        /// <summary>
        /// 路由规则配置
        /// </summary>
        public List<RoutingRuleInfo> RoutingRules { get; set; }


    }
}
