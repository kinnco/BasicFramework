﻿using System;

namespace RabbitMQ.Library
{
    public class MessagePublish
    {
        /// <summary>
        /// 消息ID。
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 消息的标记码  json xml
        /// </summary>
        public DataType Flag { get; set; }

        /// <summary>
        /// 消息的序列化字节流。
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// 创建消息的时间。
        /// </summary>
        public DateTime CreateDate { get; set; }
    }
}
