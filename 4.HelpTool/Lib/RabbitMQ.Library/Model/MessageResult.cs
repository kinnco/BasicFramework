﻿namespace RabbitMQ.Library
{
    /// <summary>
    /// 事件消息返回类型
    /// </summary>
    public class MessageResult: MessagePublish
    {
        /// <summary>
        /// 消息是否处理 默认为false
        /// </summary>
        public bool Handled { get; set; } = false;
    }
}
