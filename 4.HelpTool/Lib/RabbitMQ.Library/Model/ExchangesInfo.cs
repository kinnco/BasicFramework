﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RabbitMQ.Library
{
    /// <summary>
    /// 交换机配置文件
    /// </summary>
    public class ExchangesInfo
    {
        /// <summary>
        /// 交换机名称
        /// </summary>
        public string Exchange { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 是否持久化
        /// </summary>
        public bool Durable { get; set; }

        /// <summary>
        /// 如果是，则在至少将一个队列或交换绑定到此队列或交换之后，交换器将删除自己，
        /// 然后所有队列或交换器将被解除绑定。
        /// </summary>
        public bool AutoDelete { get; set; }

        /// <summary>
        /// 方法参数
        /// </summary>
        public Dictionary<string, object> Arguments { set; get; }
    }
}
