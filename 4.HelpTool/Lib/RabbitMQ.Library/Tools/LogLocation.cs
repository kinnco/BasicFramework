﻿using Common.Library;
using System;

namespace RabbitMQ.Library
{
    /// <summary>
    /// Log定位器
    /// </summary>
    public class LogLocation
    {
        /// <summary>
        /// 写入异常信息。
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="exception">Exception.</param>
        public static void WriteException(string title, Exception exception)
        {
            NLogHelper.LogError(title, exception);
        }

        /// <summary>
        /// 写入提示信息。
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="message">内容</param>
        public static void WriteInfo(string title, string message)
        {
            NLogHelper.LogInfo("RabbitMQClient", message);
        }
    }
}
