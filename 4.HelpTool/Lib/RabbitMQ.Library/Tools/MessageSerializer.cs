﻿using System;

namespace RabbitMQ.Library
{
    /// <summary>
    /// 面向XML/JSON的消息序列化。
    /// </summary>
    internal class MessageSerializer
    {
        /// <summary>
        /// 序列化成bytes
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <param name="msgType"></param>
        /// <returns></returns>
        internal static byte[] SerializerToBytes<T>(T message, DataType msgType) where T : class, new()
        {
            byte[] bytes = null;
            try
            {
                switch (msgType)
                {
                    case DataType.JsonType:
                        bytes = message.ToJsonBytes();
                        break;
                    case DataType.XmlType:
                        bytes = message.ToXmlBytes();
                        break;
                    default:
                        bytes = message.ToJsonBytes();
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bytes;
        }

        /// <summary>
        /// 序列化消息为Json/Xml字符串
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message">消息实例</param>
        /// <param name="msgType">消息类型</param>
        /// <returns></returns>
        internal static string SerializerToString<T>(T message, DataType msgType) where T : class, new()
        {
            string jsonstr = string.Empty;
            try
            {
                switch (msgType)
                {
                    case DataType.JsonType:
                        jsonstr = message.ToJson();
                        break;
                    case DataType.XmlType:
                        jsonstr = message.ToXml();
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return jsonstr;
        }

        /// <summary>
        /// 反序列化消息
        /// </summary>
        /// <typeparam name="T">消息的类型。</typeparam>
        /// <param name="bytes">bytes。</param>
        /// <param name="msgType">DataType</param>
        /// <returns></returns>
        internal static T Deserialize<T>(byte[] bytes, DataType msgType) where T : class, new()
        {
            var resultInfo = default(T);
            try
            {
                switch (msgType)
                {
                    case DataType.JsonType:
                        string msgStr = System.Text.Encoding.UTF8.GetString(bytes);
                        resultInfo = msgStr.JsonToObject<T>();
                        break;
                    case DataType.XmlType:
                        resultInfo = bytes.XmlToObject<T>();
                        break;
                }
                return resultInfo;
            }
            catch (System.Exception)
            {
                return default(T);
            }
        }

        /// <summary>
        /// Result反序列化消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="bytes"></param>
        /// <returns></returns>
        internal static T ResultDeserialize<T>(byte[] bytes) where T : class, new()
        {
            var resultInfo = default(T);
            try
            {
                string msgStr = System.Text.Encoding.UTF8.GetString(bytes);
                if (msgStr.Contains($"\"Flag\":{(int)DataType.JsonType}"))
                {
                    resultInfo = msgStr.JsonToObject<T>();
                }
                else if (msgStr.Contains($"\"Flag\":{(int)DataType.XmlType}"))
                {
                    resultInfo = XmlHelper.XmlToObject<T>(bytes);
                }
                else
                {
                    resultInfo = new T();
                }
                return resultInfo;
            }
            catch (System.Exception)
            {
                return default(T);
            }
        }
    }
}
