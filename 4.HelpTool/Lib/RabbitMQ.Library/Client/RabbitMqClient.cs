﻿using Common.Library;
using System;

namespace RabbitMQ.Library
{
    /// <summary>
    /// RabbitMQ入口
    /// </summary>
    public class RabbitClient
    {

        #region 属性

        /// <summary>
        /// 消息类型为Json
        /// </summary>
        public static DataType JsonData
        {
            get { return DataType.JsonType; }
        }

        /// <summary>
        /// 消息类型为Xml
        /// </summary>
        public static DataType XmlData
        {
            get { return DataType.XmlType; }
        }

        /// <summary>
        /// 交换机到交换机
        /// </summary>
        public static RouteRuleTypes RountingToExchage
        {
            get { return RouteRuleTypes.ExchageToExchage; }
        }

        /// <summary>
        /// 交换机到队列
        /// </summary>
        public static RouteRuleTypes RountingToQueue
        {
            get { return RouteRuleTypes.ExchageToQueue; }
        }

        /// <summary>
        /// 获取RabbitMQ配置信息
        /// </summary>
        public static RabbitConfig Config
        {
            get { return ConfigManager.config; }
        }

        #endregion

        /// <summary>
        /// 客户端实例私有字段。
        /// </summary>
        private static IRabbitExtension _instanceClient;

        /// <summary>
        /// 返回全局唯一的RabbitMqClient实例，此。
        /// </summary>
        public static IRabbitExtension Instance
        {
            get
            {
                if (_instanceClient.IsNull())
                {
                    var rabbitMqClientContext = new RabbitContext
                    {
                        ListenQueueName = ConfigManager.config.ListenQueueName,
                        InstanceCode = Guid.NewGuid().ToString()
                    };
                    _instanceClient = new RabbitExtension
                    {
                        Context = rabbitMqClientContext
                    };
                }
                return _instanceClient;
            }
            internal set { _instanceClient = value; }
        }

        /// <summary>
        /// 获取发送消息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public static MessagePublish GetPublishMessage<T>(T message, DataType dataType) where T : class, new()
        {
            MessagePublish result = null;
            if (message.IsNotNull())
            {
                result = MessageManager.BuildMessagePublish<T>(message, dataType);
            }
            return result;
        }

        /// <summary>
        /// 获取接收消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result"></param>
        /// <returns></returns>
        public static T GetResultMessage<T>(MessageResult result) where T : class, new()
        {
            var message = new T();
            if (result.Flag == DataType.JsonType
                || result.Flag == DataType.XmlType)
            {
                message = MessageSerializer.Deserialize<T>(result.Data, result.Flag);
                //result.Handled = true;
            }
            return message;
        }
    }
}
