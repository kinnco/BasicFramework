﻿using Common.Library;
using System;

namespace RabbitMQ.Library
{
    /// <summary>
    /// 创建消息实体信息。
    /// </summary>
    internal class MessageManager
    {
        /// <summary>
        /// 创建MessagePublish传输对象。
        /// </summary>
        /// <param name="originObject">原始强类型对象实例。</param>
        /// <param name="eventMessageMarkcode">消息的标记码。[当前为：0:json格式  1:xml格式]</param>
        /// <typeparam name="T">原始对象类型。</typeparam>
        /// <returns>EventMessage.</returns>
        internal static MessagePublish BuildMessagePublish<T>(T originObject, DataType dataType)
            where T : class, new()
        {
            var result = new MessagePublish()
            {
                Id = GuidHelper.GetGuid(),
                Flag = dataType,
                CreateDate = DateTime.Now
            };

            var bytes = MessageSerializer.SerializerToBytes<T>(originObject, dataType);

            result.Data = bytes;

            return result;
        }

        /// <summary>
        /// 生成EventMessageResult对象。
        /// </summary>
        /// <param name="bytes">流</param>
        /// <returns>EventMessageResult instance.</returns>
        internal static MessageResult BuildMessageResult(byte[] bytes)
        {
            var eventMessage = MessageSerializer.ResultDeserialize<MessageResult>(bytes);

            return eventMessage;
        }

    }
}
