﻿using RabbitMQ.Client;
using System;

namespace RabbitMQ.Library
{
    /// <summary>
    /// Rabbit扩展接口(如果要拓展NoWait操作，可以再起一个实现类)
    /// </summary>
    public interface IRabbitExtension : IDisposable
    {
        /// <summary>
        /// RabbitMqClient 数据上下文。
        /// </summary>
        RabbitContext Context { get; set; }

        /// <summary>
        /// 消息被本地激活事件。通过绑定该事件来获取消息队列推送过来的消息。只能绑定一个事件处理程序。
        /// </summary>
        event ActionEvent ActionEventMessage;


        #region 渠道

        /// <summary>
        /// 生成渠道
        /// </summary>
        /// <param name="config"></param>
        IModel CreateChannel();

        #endregion

        #region 交换机
        /// <summary>
        /// 增加交换机方法
        /// </summary>
        /// <param name="config"></param>
        bool CreateExchange(ExchangesInfo config);

        /// <summary>
        /// 删除交换机
        /// </summary>
        /// <param name="config"></param>
        bool RemoveExchange(string exchangeName);

        /// <summary>
        /// 创建Exchange到Exchange的路由规则
        /// </summary>
        /// <param name="config"></param>
        bool CreateExchangeToExchange(RoutingRuleInfo config);

        #endregion

        #region 队列
        /// <summary>
        /// 增加队列信息
        /// </summary>
        /// <param name="config"></param>
        QueueDeclareOk CreateQueue(QueuesInfo config);

        /// <summary>
        /// 删除队列
        /// </summary>
        /// <param name="config"></param>
        bool RemoveQueue(string queueName);

        /// <summary>
        /// 清空队列
        /// </summary>
        /// <param name="config"></param>
        uint PurgeQueue(string queueName);
        #endregion

        #region 路由规则
        /// <summary>
        /// 创建路由规则
        /// </summary>
        /// <param name="config"></param>
        bool CreateExchangeToQueue(RoutingRuleInfo config);
        #endregion

        #region 消息发送
        /// <summary>
        /// 触发一个事件，向队列推送一个事件消息。
        /// </summary>
        /// <param name="eventMessage">消息类型实例</param>
        /// <param name="exChange">Exchange名称</param>
        /// <param name="routingKey">routingKey</param>
        bool SendEventMessage(MessagePublish eventMessage, string exchange, string routingKey, byte deliveryMode = 2);
        #endregion

        #region 接受消息
        /// <summary>
        /// 开始消息队列的默认监听。
        /// </summary>
        void OnListening(string listenQueueName);

        #endregion
    }
}
