﻿/*
* 命名空间: Service.Library
*
* 功 能： 返回信息
*
* 类 名： ResultInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/5/18 14:50:46 	罗维    创建
* ─────────────────────────────────────────────────
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Library
{
    /// <summary>
    /// 返回信息
    /// </summary>
    public class ServiceResultInfo
    {

        /// <summary>
        /// 操作是否成功
        /// </summary>
        public bool Success { get; set; } = true;

        /// <summary>
        /// 返回操作码
        /// </summary>
        public int Code { get; set; } 

        /// <summary>
        /// 返回的操作消息
        /// </summary>
        public string Msg { get; set; } = "无效操作";
    }
}
