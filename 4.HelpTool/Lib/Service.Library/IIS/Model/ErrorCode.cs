﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Library
{
    public class ErrorCode
    {
        ///// <summary>
        ///// 成功
        ///// </summary>
        //public const int Succeed = 0;
        //private const string rSucceed = "成功创建站点";
        /// <summary>
        /// 设置站点成功
        /// </summary>
        public const int SetSucceed = 1;
        private const string rSetSucceed = "成功设置站点";
        /// <summary>
        /// 设置失败
        /// </summary>
        public const int SetFail = 535;
        private const string rSetFail = "设置站点失败";
        /// <summary>
        /// 参数错误
        /// </summary>
        public const int InvalidParameter = 302;
        private const string rInvalidParameter = "参数错误";
        /// <summary>
        /// 站点未找到
        /// </summary>
        public const int SiteNotFound = 400;
        private const string rSiteNotFound = "指定站点未找到";
        /// <summary>
        /// 应用程序池未找到
        /// </summary>
        public const int AppPoolNotFound = 401;
        private const string rAppPoolNotFound = "应用程序池未找到";
        /// <summary>
        /// 应用程序未找到
        /// </summary>
        public const int AppNotFound = 402;
        private const string rAppNotFound = "应用程序未找到";
        /// <summary>
        /// 根应用未找到
        /// </summary>
        public const int RootAppNotFound = 403;
        private const string rRootAppNotFound = "根应用未找到";
        /// <summary>
        /// 虚拟目录未找到
        /// </summary>
        public const int VirtualDirNotFound = 404;
        private const string rVirtualDirNotFound = "虚拟目录未找到";
        /// <summary>
        /// IIS版本未知
        /// </summary>
        public const int UnknownIISVer = 500;
        private const string rUnknownIISVer = "IIS版本未知";
        /// <summary>
        /// http端口已占用
        /// </summary>
        public const int HttpPortUsed = 501;
        private const string rHttpPortUsed = "http端口已被占用";
        /// <summary>
        /// https端口已占用
        /// </summary>
        public const int HttpsPortUsed = 502;
        private const string rHttpsPortUsed = "https端口已被占用";
        /// <summary>
        /// 站点已存在
        /// </summary>
        public const int SiteExists = 503;
        private const string rSiteExists = "指定站点已存在";
        /// <summary>
        /// 应用程序池已存在
        /// </summary>
        public const int AppPoolExists = 504;
        private const string rAppPoolExists = "应用程序池已存在";
        /// <summary>
        /// 应用程序已存在
        /// </summary>
        public const int AppExists = 505;
        private const string rAppExists = "应用程序已存在";
        /// <summary>
        /// 虚拟目录已存在
        /// </summary>
        public const int VirtualDirExists = 506;
        private const string rVirtualDirExists = "虚拟目录已存在";
        /// <summary>
        /// 用户已存在
        /// </summary>
        public const int UserExist = 507;
        private const string rUserExist = "指定用户已存在";
        /// <summary>
        /// 用户不存在
        /// </summary>
        public const int UserNotExit = 508;
        private const string rUserNotExit = "指定用户不存在";
        /// <summary>
        /// 路径存在
        /// </summary>
        public const int PathExist = 509;
        private const string rPathExist = "指定路径已存在";
        /// <summary>
        /// 路径不存在
        /// </summary>
        public const int PathNotExist = 510;
        private const string rPathNotExist = "指定路径不存在";
        /// <summary>
        /// IO异常
        /// </summary>
        public const int IOEXCEPTION = 511;
        private const string rIOEXCEPTION = "IO异常,请检查磁盘是否存在";
        /// <summary>
        /// 成功创建用户
        /// </summary>
        public const int UserAdd = 512;
        private const string rUserAdd = "成功创建用户";
        /// <summary>
        /// 用户创建失败
        /// </summary>
        public const int UserNotAdd = 513;
        private const string rUserNotAdd = "用户创建失败,请检查用户名长度和杀毒软件";
        /// <summary>
        /// 设置默认文档成功
        /// </summary>
        public const int DocumentSetOK = 514;
        private const string rDocumentSetOK = "成功设置默认文档";
        /// <summary>
        /// 设置默认文档失败
        /// </summary>
        public const int NoSetDocument = 515;
        private const string rNoSetDocument = "设置默认文档失败";
        /// <summary>
        /// 提交更改成功
        /// </summary>
        public const int CommitOK = 516;
        private const string rCommitOK = "成功提交更改";
        /// <summary>
        /// 提交更改失败
        /// </summary>
        public const int CommitFail = 517;
        private const string rCommitFail = "提交更改失败";
        /// <summary>
        /// 查找站点
        /// </summary>
        public const int FindSiteError = 518;
        private const string rFindSiteError = "查找指定站点出错";
        /// <summary>
        /// iis已经启动
        /// </summary>
        public const int iisStart = 519;
        private const string riisStart = "iis启动成功";
        /// <summary>
        /// iis已经停止
        /// </summary>
        public const int iisStop = 520;
        private const string riisStop = "iis停止成功";
        /// <summary>
        /// iis状态错误
        /// </summary>
        public const int iisStateError = 521;
        private const string riisStateError = "iis状态错误,请手动更改iis状态";
        /// <summary>
        /// 成功创建应用池
        /// </summary>
        public const int poolBuild = 522;
        private const string rpoolBuild = "成功创建应用池";
        /// <summary>
        /// 创建应用池失败
        /// </summary>
        public const int poolNotBuild = 523;
        private const string rpoolNotBuild = "创建应用池失败";
        /// <summary>
        /// iis已经启动
        /// </summary>
        public const int siteStart = 524;
        private const string rsiteStart = "iis已经启动";
        /// <summary>
        /// iis已经停止
        /// </summary>
        public const int siteStop = 525;
        private const string rsiteStop = "iis已经停止";
        /// <summary>
        /// 站点已经创建,但是iis状态错误
        /// </summary>
        public const int siteStateError = 526;
        private const string rsiteStateError = "站点已经创建,但无法启用,请检查端口是否被占用!";
        /// <summary>
        /// SQL语句执行成功
        /// </summary>
        public const int sqlExcOK = 527;
        private const string rsqlExcOK = "sql语句执行成功";
        /// <summary>
        /// SQL语句执行失败
        /// </summary>
        public const int sqlExcFail = 528;
        private const string rsqlExcFail = "sql语句执行失败";
        /// <summary>
        /// 设置默认文档
        /// </summary>
        public const int setDefaultDocOK = 529;
        private const string rsetDefaultDocOK = "成功设置默认文档";
        /// <summary>
        /// 设置默认文档
        /// </summary>
        public const int setDefaultDocFail = 530;
        private const string rsetDefaultDocFail = "设置默认文档失败";
        /// <summary>
        /// 新站点存储成功
        /// </summary>
        public const int saveNewSiteOK = 531;
        private const string rsaveNewSiteOK = "站点成功保存到数据库表";
        /// <summary>
        ///  新站点存储失败
        /// </summary>
        public const int saveNewSiteFail = 532;
        private const string rsaveNewSiteFail = "站点已经创建,但为存储到数据库";
        /// <summary>
        ///  创建FTP失败
        /// </summary>
        public const int ftpSiteFail = 533;
        private const string rftpSiteFail = "站点创建成功,但FTP站点创建失败";
        /// <summary>
        ///  FTP站点已经存在
        /// </summary>
        public const int ftpExists = 534;
        private const string rftpExists = "FTP站点已经存在";
        /// <summary>
        /// 应用池删除失败
        /// </summary>
        public const int RemovePoolFail = 535;
        private const string rRemovePoolFail = "删除应用池失败";
        /// <summary>
        /// 应用站点失败
        /// </summary>
        public const int RemoveSiteFail = 536;
        private const string rRemoveSiteFail = "删除站点失败";
        /// <summary>
        /// 删除用户失败
        /// </summary>
        public const int RemoveUserFail = 537;
        private const string rRemoveUserFail = "删除站点失败";

        /// <summary>
        /// 删除站点成功
        /// </summary>
        public const int RemoveSiteSuccess = 538;
        private const string rRemoveSiteSuccess = "删除站点成功";

        /// <summary>
        /// 未知错误
        /// </summary>
        public const int Unknown = 999;
        private const string rUnknown = "未知错误";

        public static string getResult(int errorCode)
        {

            if (errorCode == ErrorCode.Unknown) { return rUnknown; }
            if (errorCode == ErrorCode.siteStateError) { return rsiteStateError; }
            if (errorCode == ErrorCode.siteStop) { return rsiteStop; }
            if (errorCode == ErrorCode.siteStart) { return rsiteStart; }
            if (errorCode == ErrorCode.poolNotBuild) { return rpoolNotBuild; }
            if (errorCode == ErrorCode.poolBuild) { return rpoolBuild; }
            if (errorCode == ErrorCode.iisStateError) { return riisStateError; }
            if (errorCode == ErrorCode.iisStop) { return riisStop; }
            if (errorCode == ErrorCode.iisStart) { return riisStart; }
            if (errorCode == ErrorCode.FindSiteError) { return rFindSiteError; }
            if (errorCode == ErrorCode.CommitFail) { return rCommitFail; }
            if (errorCode == ErrorCode.CommitOK) { return rCommitOK; }
            if (errorCode == ErrorCode.NoSetDocument) { return rNoSetDocument; }
            if (errorCode == ErrorCode.DocumentSetOK) { return rDocumentSetOK; }
            if (errorCode == ErrorCode.UserNotAdd) { return rUserNotAdd; }
            if (errorCode == ErrorCode.UserAdd) { return rUserAdd; }
            if (errorCode == ErrorCode.IOEXCEPTION) { return rIOEXCEPTION; }
            if (errorCode == ErrorCode.PathNotExist) { return rPathNotExist; }
            if (errorCode == ErrorCode.PathExist) { return rPathExist; }
            if (errorCode == ErrorCode.UserNotExit) { return rUserNotExit; }
            if (errorCode == ErrorCode.UserExist) { return rUserExist; }
            if (errorCode == ErrorCode.VirtualDirExists) { return rVirtualDirExists; }
            if (errorCode == ErrorCode.AppExists) { return rAppExists; }
            if (errorCode == ErrorCode.AppPoolExists) { return rAppPoolExists; }
            if (errorCode == ErrorCode.SiteExists) { return rSiteExists; }
            if (errorCode == ErrorCode.HttpsPortUsed) { return rHttpsPortUsed; }
            if (errorCode == ErrorCode.HttpPortUsed) { return rHttpPortUsed; }
            if (errorCode == ErrorCode.UnknownIISVer) { return rUnknownIISVer; }
            if (errorCode == ErrorCode.VirtualDirNotFound) { return rVirtualDirNotFound; }
            if (errorCode == ErrorCode.RootAppNotFound) { return rRootAppNotFound; }
            if (errorCode == ErrorCode.AppNotFound) { return rAppNotFound; }
            if (errorCode == ErrorCode.AppPoolNotFound) { return rAppPoolNotFound; }
            if (errorCode == ErrorCode.SiteNotFound) { return rSiteNotFound; }
            if (errorCode == ErrorCode.InvalidParameter) { return rInvalidParameter; }
            //if (errorCode == ErrorCode.Succeed) { return rSucceed; }
            if (errorCode == ErrorCode.setDefaultDocFail) { return rsetDefaultDocFail; }
            if (errorCode == ErrorCode.setDefaultDocOK) { return rsetDefaultDocOK; }
            if (errorCode == ErrorCode.saveNewSiteOK) { return rsaveNewSiteOK; }
            if (errorCode == ErrorCode.saveNewSiteFail) { return rsaveNewSiteFail; }
            if (errorCode == ErrorCode.ftpSiteFail) { return rftpSiteFail; }
            if (errorCode == ErrorCode.ftpExists) { return rftpExists; }
            if (errorCode == ErrorCode.SetSucceed) { return rSetSucceed; }
            if (errorCode == ErrorCode.SetFail) { return rSetFail; }
            if (errorCode == ErrorCode.RemovePoolFail) { return rRemovePoolFail; }
            if (errorCode == ErrorCode.RemoveSiteFail) { return rRemoveSiteFail; }
            if (errorCode == ErrorCode.RemoveUserFail) { return rRemoveUserFail; }

            return rUnknown;

        }
    }
}
