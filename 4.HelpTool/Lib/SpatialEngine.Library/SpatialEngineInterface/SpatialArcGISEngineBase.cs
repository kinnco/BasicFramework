﻿/***************************************************************************************************
  	创建时间:	2018-09-10
	文件名字:	SpatialEngineBase.cs
	作    者:	周业
	版    本:	V1.000.000
	说    明:	空间数据库引擎操作的虚拟类，用于空间数据引擎操作辅助类进行继承方法。
	历史记录：	
	<作  者>		<修改时间>		  <版  本>		       <目  的>	
****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;

namespace SpatialEngine.Library
{
    public class SpatialArcGISEngineBase
    {
        #region 变量和属性

        /// <summary>
        /// 数据库工作空间
        /// </summary>
        public IWorkspace pWorkSpace = null;

        /// <summary>
        /// 数据库编辑空间
        /// </summary>
        private IWorkspaceEdit _pWorkspaceEdit = null;

        #endregion

        #region 结构体和枚举
        /// <summary>
        /// 点结构体
        /// </summary>
        public struct PointStruct
        {
            //点X坐标
            public double X;

            //点Y坐标
            public double Y;
        }

        #endregion

        #region 构造和析构函数

        public SpatialArcGISEngineBase()
        {
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="strXmlFilePath">xml文件路径</param>
        /// <param name="strXmlNodePath">xml节点路径</param>
        public SpatialArcGISEngineBase(string strXmlFilePath, string strXmlNodePath)
        {
        }

        #endregion

        #region 类事件

        #endregion

        #region 类方法

        /// <summary>
        /// 连接SDE数据库获取工作空间
        /// </summary>
        /// <returns>工作空间</returns>
        public virtual IWorkspace ConnectArcSdeAndGetWorkspace()
        {
            IWorkspace pWorkspace = null;
            return pWorkspace;
        }

        /// <summary>
        /// 给要素类新增点要素
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="oPointStruct">PointStruct类型，点的坐标值</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,要素属性</param>
        /// <returns></returns>
        public bool CreateNewPointFeature(string strFeatureClassName, PointStruct oPointStruct, Dictionary<string, object> dicFeatureAttribute)
        {
            try
            {
                bool bResult = CreateNewPointFeature(this.pWorkSpace, strFeatureClassName, oPointStruct, dicFeatureAttribute);
                return bResult;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 给要素类新增点要素
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="oPointStruct">PointStruct类型，点的坐标值</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,要素属性</param>
        /// <returns></returns>
        public bool CreateNewPointFeature(IWorkspace pWorkspace, string strFeatureClassName, PointStruct oPointStruct, Dictionary<string, object> dicFeatureAttribute)
        {
            try
            {
                //打开编辑空间
                IWorkspaceEdit pWorkspaceEdit = pWorkspace as IWorkspaceEdit;
                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();

                IFeatureClass pFeataureClass = GetFeatureClass(pWorkspace, strFeatureClassName);
                IFeatureCursor pFeatureCursor = pFeataureClass.Insert(true);
                IFeatureBuffer pFeatureBuffer = pFeataureClass.CreateFeatureBuffer();//创建新图层的新要素缓冲

                //更新要素几何图形
                IPoint pPoint = new PointClass();
                pPoint.X = oPointStruct.X;
                pPoint.Y = oPointStruct.Y;
                IZAware oIZAware = pPoint as IZAware;
                if (oIZAware.ZAware)
                {
                    oIZAware.ZAware = false;
                }
                pFeatureBuffer.Shape = pPoint;

                //要素属性赋值
                for (int i = 0; i < dicFeatureAttribute.Count; i++)
                {
                    int iFieldIndex = pFeatureBuffer.Fields.FindField(dicFeatureAttribute.ElementAt(i).Key);
                    if (dicFeatureAttribute.ElementAt(i).Value!=null&&dicFeatureAttribute.ElementAt(i).Value.ToString().Trim() != "")
                    {
                        switch (pFeatureBuffer.Fields.Field[iFieldIndex].Type)
                        {
                            case esriFieldType.esriFieldTypeString:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToString(dicFeatureAttribute.ElementAt(i).Value));
                                break;
                            case esriFieldType.esriFieldTypeDouble:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToDouble(dicFeatureAttribute.ElementAt(i).Value));
                                break;
                            case esriFieldType.esriFieldTypeInteger:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToInt32(dicFeatureAttribute.ElementAt(i).Value));
                                break;
                            case esriFieldType.esriFieldTypeDate:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToDateTime(dicFeatureAttribute.ElementAt(i).Value, System.Globalization.CultureInfo.CurrentCulture));
                                break;
                            case esriFieldType.esriFieldTypeSmallInteger:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToInt16(dicFeatureAttribute.ElementAt(i).Value));
                                break;
                        }
                    }
                    else
                    {
                        pFeatureBuffer.set_Value(iFieldIndex!=-1? iFieldIndex:i, null);
                    }
                }

                //提交更新
                pFeatureCursor.InsertFeature(pFeatureBuffer);
                pFeatureCursor.Flush();

                //关闭编辑空间
                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(true);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 更新点要素
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="oPointStruct">PointStruct类型，点的坐标值</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,要素属性</param>
        /// <param name="strWhereClause">String类型，筛选出要更新的要素的SQL条件语句</param>
        /// <returns></returns>
        public bool UpdatePointFeature(string strFeatureClassName, PointStruct oPointStruct, Dictionary<string, object> dicFeatureAttribute, string strWhereClause)
        {
            try
            {
                bool bResult = UpdatePointFeature(this.pWorkSpace, strFeatureClassName, oPointStruct, dicFeatureAttribute, strWhereClause);
                return bResult;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 更新点要素
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="oPointStruct">PointStruct类型，点的坐标值</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,要素属性</param>
        /// <param name="strWhereClause">String类型，筛选出要更新的要素的SQL条件语句</param>
        /// <returns></returns>
        public bool UpdatePointFeature(IWorkspace pWorkspace, string strFeatureClassName, PointStruct oPointStruct, Dictionary<string, object> dicFeatureAttribute, string strWhereClause)
        {
            try
            {
                IFeatureClass pFeataureClass = GetFeatureClass(pWorkspace, strFeatureClassName);
                IQueryFilter pQueryFilter = new QueryFilterClass();
                pQueryFilter.WhereClause = strWhereClause;
                IFeatureCursor pFeatureCursor = pFeataureClass.Update(pQueryFilter, true);
                IFeature pUpdateFeature = pFeatureCursor.NextFeature();
                while (pUpdateFeature != null)
                {
                    //更新要素几何图形
                    IPoint pPoint = new PointClass();
                    pPoint.X = oPointStruct.X;
                    pPoint.Y = oPointStruct.Y;
                    IZAware oIZAware = pPoint as IZAware;
                    if (oIZAware.ZAware)
                    {
                        oIZAware.ZAware = false;
                    }
                    pUpdateFeature.Shape = pPoint;

                    //要素属性赋值
                    for (int i = 0; i < dicFeatureAttribute.Count; i++)
                    {
                        int iFieldIndex = pUpdateFeature.Fields.FindField(dicFeatureAttribute.ElementAt(i).Key);
                        if (dicFeatureAttribute.ElementAt(i).Value != null && dicFeatureAttribute.ElementAt(i).Value.ToString().Trim() != "")
                        {
                            switch (pUpdateFeature.Fields.Field[iFieldIndex].Type)
                            {
                                case esriFieldType.esriFieldTypeString:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToString(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                                case esriFieldType.esriFieldTypeDouble:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToDouble(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                                case esriFieldType.esriFieldTypeInteger:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToInt32(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                                case esriFieldType.esriFieldTypeDate:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToDateTime(dicFeatureAttribute.ElementAt(i).Value, System.Globalization.CultureInfo.CurrentCulture));
                                    break;
                                case esriFieldType.esriFieldTypeSmallInteger:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToInt16(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                            }
                        }
                        else
                        {
                            pUpdateFeature.set_Value(iFieldIndex != -1 ? iFieldIndex : i, null);
                        }
                    }
                    //提交更新
                    pFeatureCursor.UpdateFeature(pUpdateFeature);

                    pUpdateFeature = pFeatureCursor.NextFeature();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 给要素类新增线要素
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="listPointStruct">List&lt;PointStruct&gt;类型，要素中点的坐标值集合</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,要素属性</param>
        /// <returns></returns>
        public bool CreateNewLineFeature(string strFeatureClassName, List<PointStruct> listPointStruct, Dictionary<string, object> dicFeatureAttribute)
        {
            try
            {
                bool bResult = CreateNewLineFeature(this.pWorkSpace, strFeatureClassName, listPointStruct, dicFeatureAttribute);
                return bResult;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 给要素类新增线要素
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="listPointStruct">List&lt;PointStruct&gt;类型，要素中点的坐标值集合</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,要素属性</param>
        /// <returns></returns>
        public bool CreateNewLineFeature(IWorkspace pWorkspace, string strFeatureClassName, List<PointStruct> listPointStruct, Dictionary<string, object> dicFeatureAttribute)
        {
            try
            {
                IWorkspaceEdit pWorkspaceEdit = pWorkspace as IWorkspaceEdit;
                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();
                IFeatureClass pFeataureClass = GetFeatureClass(pWorkspace, strFeatureClassName);
                IFeatureCursor pFeatureCursor = pFeataureClass.Insert(true);
                IFeatureBuffer pFeatureBuffer = pFeataureClass.CreateFeatureBuffer();//创建新图层的新要素缓冲
                IPointCollection pPointCollection = new PolylineClass();
                foreach (PointStruct oPointStruct in listPointStruct)
                {
                    IPoint pPoint = new PointClass();
                    pPoint.X = oPointStruct.X;
                    pPoint.Y = oPointStruct.Y;
                    pPointCollection.AddPoint(pPoint);
                }
                pFeatureBuffer.Shape = pPointCollection as ILine;
                for (int i = 0; i < dicFeatureAttribute.Count; i++)
                {
                    int iFieldIndex = pFeatureBuffer.Fields.FindField(dicFeatureAttribute.ElementAt(i).Key);
                    if (dicFeatureAttribute.ElementAt(i).Value != null && dicFeatureAttribute.ElementAt(i).Value.ToString().Trim() != "")
                    {
                        switch (pFeatureBuffer.Fields.Field[iFieldIndex].Type)
                        {
                            case esriFieldType.esriFieldTypeString:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToString(dicFeatureAttribute.ElementAt(i).Value));
                                break;
                            case esriFieldType.esriFieldTypeDouble:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToDouble(dicFeatureAttribute.ElementAt(i).Value));
                                break;
                            case esriFieldType.esriFieldTypeInteger:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToInt32(dicFeatureAttribute.ElementAt(i).Value));
                                break;
                            case esriFieldType.esriFieldTypeDate:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToDateTime(dicFeatureAttribute.ElementAt(i).Value, System.Globalization.CultureInfo.CurrentCulture));
                                break;
                            case esriFieldType.esriFieldTypeSmallInteger:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToInt16(dicFeatureAttribute.ElementAt(i).Value));
                                break;
                        }
                    }
                    else
                    {
                        pFeatureBuffer.set_Value(iFieldIndex != -1 ? iFieldIndex : i, null);
                    }
                }
                pFeatureCursor.InsertFeature(pFeatureBuffer);
                pFeatureCursor.Flush();
                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(true);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 更新线要素
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="listPointStruct">List&lt;PointStruct&gt;类型，要素中点的坐标值集合</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,要素属性</param>
        /// <param name="strWhereClause">String类型，筛选出要更新的要素的SQL条件语句</param>
        /// <returns></returns>
        public bool UpdateLineFeature(string strFeatureClassName, List<PointStruct> listPointStruct, Dictionary<string, object> dicFeatureAttribute, string strWhereClause)
        {
            try
            {
                bool bResult = UpdateLineFeature(this.pWorkSpace, strFeatureClassName, listPointStruct, dicFeatureAttribute, strWhereClause);
                return bResult;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        /// <summary>
        /// 更新线要素
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="listPointStruct">List&lt;PointStruct&gt;类型，要素中点的坐标值集合</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,要素属性</param>
        /// <param name="strWhereClause">String类型，筛选出要更新的要素的SQL条件语句</param>
        /// <returns></returns>
        public bool UpdateLineFeature(IWorkspace pWorkspace, string strFeatureClassName, List<PointStruct> listPointStruct, Dictionary<string, object> dicFeatureAttribute, string strWhereClause)
        {
            try
            {
                IFeatureClass pFeataureClass = GetFeatureClass(pWorkspace, strFeatureClassName);
                IQueryFilter pQueryFilter = new QueryFilterClass();
                pQueryFilter.WhereClause = strWhereClause;
                IFeatureCursor pFeatureCursor = pFeataureClass.Update(pQueryFilter, true);
                IFeature pUpdateFeature = pFeatureCursor.NextFeature();
                while (pUpdateFeature != null)
                {
                    //更新要素几何图形
                    IPointCollection pPointCollection = new PolylineClass(); ;
                    foreach (PointStruct oPointStruct in listPointStruct)
                    {
                        IPoint pPoint = new PointClass();
                        pPoint.X = oPointStruct.X;
                        pPoint.Y = oPointStruct.Y;
                        pPointCollection.AddPoint(pPoint);
                    }
                    IZAware oIZAware = pPointCollection as IZAware;
                    if (oIZAware.ZAware)
                    {
                        oIZAware.ZAware = false;
                    }
                    pUpdateFeature.Shape = pPointCollection as ILine;

                    //要素属性赋值
                    for (int i = 0; i < dicFeatureAttribute.Count; i++)
                    {
                        int iFieldIndex = pUpdateFeature.Fields.FindField(dicFeatureAttribute.ElementAt(i).Key);
                        if (dicFeatureAttribute.ElementAt(i).Value != null && dicFeatureAttribute.ElementAt(i).Value.ToString().Trim() != "")
                        {
                            switch (pUpdateFeature.Fields.Field[iFieldIndex].Type)
                            {
                                case esriFieldType.esriFieldTypeString:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToString(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                                case esriFieldType.esriFieldTypeDouble:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToDouble(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                                case esriFieldType.esriFieldTypeInteger:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToInt32(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                                case esriFieldType.esriFieldTypeDate:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToDateTime(dicFeatureAttribute.ElementAt(i).Value, System.Globalization.CultureInfo.CurrentCulture));
                                    break;
                                case esriFieldType.esriFieldTypeSmallInteger:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToInt16(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                            }
                        }
                        else
                        {
                            pUpdateFeature.set_Value(iFieldIndex != -1 ? iFieldIndex : i, null);
                        }
                    }
                    //提交更新
                    pFeatureCursor.UpdateFeature(pUpdateFeature);

                    pUpdateFeature = pFeatureCursor.NextFeature();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 给要素类新增面要素
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="listPointStruct">List&lt;PointStruct&gt;类型，要素中点的坐标值集合</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,要素属性</param>
        /// <returns></returns>
        public bool CreateNewPolygonFeature(string strFeatureClassName, List<PointStruct> listPointStruct, Dictionary<string, object> dicFeatureAttribute)
        {
            try
            {
                bool bResult = CreateNewPolygonFeature(this.pWorkSpace, strFeatureClassName, listPointStruct, dicFeatureAttribute);
                return bResult;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 给要素类新增面要素
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="listPointStruct">List&lt;PointStruct&gt;类型，要素中点的坐标值集合</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,要素属性</param>
        /// <returns></returns>
        public bool CreateNewPolygonFeature(IWorkspace pWorkspace, string strFeatureClassName, List<PointStruct> listPointStruct, Dictionary<string, object> dicFeatureAttribute)
        {
            try
            {
                IWorkspaceEdit pWorkspaceEdit = pWorkspace as IWorkspaceEdit;
                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();
                IFeatureClass pFeataureClass = GetFeatureClass(pWorkspace, strFeatureClassName);
                IFeatureCursor pFeatureCursor = pFeataureClass.Insert(true);
                IFeatureBuffer pFeatureBuffer = pFeataureClass.CreateFeatureBuffer();//创建新图层的新要素缓冲
                IPointCollection pPointCollection = new PolygonClass();
                foreach (PointStruct oPointStruct in listPointStruct)
                {
                    IPoint pPoint = new PointClass();
                    pPoint.X = oPointStruct.X;
                    pPoint.Y = oPointStruct.Y;
                    pPointCollection.AddPoint(pPoint);
                }
                pFeatureBuffer.Shape = pPointCollection as IPolygon;
                for (int i = 0; i < dicFeatureAttribute.Count; i++)
                {
                    int iFieldIndex = pFeatureBuffer.Fields.FindField(dicFeatureAttribute.ElementAt(i).Key);
                    if (dicFeatureAttribute.ElementAt(i).Value != null && dicFeatureAttribute.ElementAt(i).Value.ToString().Trim() != "")
                    {
                        switch (pFeatureBuffer.Fields.Field[iFieldIndex].Type)
                        {
                            case esriFieldType.esriFieldTypeString:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToString(dicFeatureAttribute.ElementAt(i).Value));
                                break;
                            case esriFieldType.esriFieldTypeDouble:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToDouble(dicFeatureAttribute.ElementAt(i).Value));
                                break;
                            case esriFieldType.esriFieldTypeInteger:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToInt32(dicFeatureAttribute.ElementAt(i).Value));
                                break;
                            case esriFieldType.esriFieldTypeDate:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToDateTime(dicFeatureAttribute.ElementAt(i).Value, System.Globalization.CultureInfo.CurrentCulture));
                                break;
                            case esriFieldType.esriFieldTypeSmallInteger:
                                pFeatureBuffer.set_Value(iFieldIndex, Convert.ToInt16(dicFeatureAttribute.ElementAt(i).Value));
                                break;
                        }
                    }
                    else
                    {
                        pFeatureBuffer.set_Value(iFieldIndex != -1 ? iFieldIndex : i, null);
                    }
                }
                pFeatureCursor.InsertFeature(pFeatureBuffer);
                pFeatureCursor.Flush();
                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(true);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 更新面要素
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="listPointStruct">List&lt;PointStruct&gt;类型，要素中点的坐标值集合</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,更新的要素属性</param>
        /// <param name="strWhereClause">String类型，筛选出要更新的要素的SQL条件语句</param>
        /// <returns></returns>
        public bool UpdatePolygonFeature(string strFeatureClassName, List<PointStruct> listPointStruct, Dictionary<string, object> dicFeatureAttribute, string strWhereClause)
        {
            try
            {
                bool bResult = UpdatePolygonFeature(this.pWorkSpace, strFeatureClassName, listPointStruct, dicFeatureAttribute, strWhereClause);
                return bResult;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 更新面要素
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="listPointStruct">List&lt;PointStruct&gt;类型，要素中点的坐标值集合</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,更新的要素属性</param>
        /// <param name="strWhereClause">String类型，筛选出要更新的要素的SQL条件语句</param>
        /// <returns></returns>
        public bool UpdatePolygonFeature(IWorkspace pWorkspace, string strFeatureClassName, List<PointStruct> listPointStruct, Dictionary<string, object> dicFeatureAttribute, string strWhereClause)
        {
            try
            {
                IFeatureClass pFeataureClass = GetFeatureClass(pWorkspace, strFeatureClassName);
                IQueryFilter pQueryFilter = new QueryFilterClass();
                pQueryFilter.WhereClause = strWhereClause;
                IFeatureCursor pFeatureCursor = pFeataureClass.Update(pQueryFilter, true);
                IFeature pUpdateFeature = pFeatureCursor.NextFeature();
                while (pUpdateFeature != null)
                {
                    //更新要素几何图形
                    IPointCollection pPointCollection = new PolygonClass();
                    foreach (PointStruct oPointStruct in listPointStruct)
                    {
                        IPoint pPoint = new PointClass();
                        pPoint.X = oPointStruct.X;
                        pPoint.Y = oPointStruct.Y;
                        pPointCollection.AddPoint(pPoint);
                    }
                    IZAware oIZAware = pPointCollection as IZAware;
                    if (oIZAware.ZAware)
                    {
                        oIZAware.ZAware = false;
                    }
                    pUpdateFeature.Shape = pPointCollection as IPolygon;

                    //要素属性赋值
                    for (int i = 0; i < dicFeatureAttribute.Count; i++)
                    {
                        int iFieldIndex = pUpdateFeature.Fields.FindField(dicFeatureAttribute.ElementAt(i).Key);
                        if (dicFeatureAttribute.ElementAt(i).Value != null && dicFeatureAttribute.ElementAt(i).Value.ToString().Trim() != "")
                        {
                            switch (pUpdateFeature.Fields.Field[iFieldIndex].Type)
                            {
                                case esriFieldType.esriFieldTypeString:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToString(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                                case esriFieldType.esriFieldTypeDouble:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToDouble(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                                case esriFieldType.esriFieldTypeInteger:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToInt32(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                                case esriFieldType.esriFieldTypeDate:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToDateTime(dicFeatureAttribute.ElementAt(i).Value, System.Globalization.CultureInfo.CurrentCulture));
                                    break;
                                case esriFieldType.esriFieldTypeSmallInteger:
                                    pUpdateFeature.set_Value(iFieldIndex, Convert.ToInt16(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                            }
                        }
                        else
                        {
                            pUpdateFeature.set_Value(iFieldIndex != -1 ? iFieldIndex : i, null);
                        }
                    }
                    //提交更新
                    pFeatureCursor.UpdateFeature(pUpdateFeature);

                    pUpdateFeature = pFeatureCursor.NextFeature();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 快速批量更新要素属性
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,更新的要素属性</param>
        /// <param name="strWhereClause">String类型，筛选出要更新的要素的SQL条件语句</param>
        /// <returns></returns>
        public bool QuickUpdateMoreFeatureAttribute(string strFeatureClassName, Dictionary<string, object> dicFeatureAttribute, string strWhereClause)
        {
            try
            {
                bool bResult = QuickUpdateMoreFeatureAttribute(this.pWorkSpace, strFeatureClassName, dicFeatureAttribute, strWhereClause);
                return bResult;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 快速批量更新要素属性
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="dicFeatureAttribute">Dictionary&lt;string,object&gt;类型,更新的要素属性</param>
        /// <param name="strWhereClause">String类型，筛选出要更新的要素的SQL条件语句</param>
        /// <returns></returns>
        public bool QuickUpdateMoreFeatureAttribute(IWorkspace pWorkspace, string strFeatureClassName, Dictionary<string, object> dicFeatureAttribute, string strWhereClause)
        {
            try
            {
                IFeatureClass pFeataureClass = GetFeatureClass(pWorkspace, strFeatureClassName);
                IQueryFilter pQueryFilter = new QueryFilterClass();
                pQueryFilter.WhereClause = strWhereClause;
                ITable pTable = pFeataureClass as ITable;
                ICursor pCursor = pTable.Update(pQueryFilter, false);
                IRow pRow = pCursor.NextRow();
                for (int i = 0; i < pTable.RowCount(pQueryFilter); i++)
                {
                    for (int j = 0; j < dicFeatureAttribute.Count; j++)
                    {
                        int iFieldIndex = pRow.Fields.FindField(dicFeatureAttribute.ElementAt(i).Key);
                        if (dicFeatureAttribute.ElementAt(i).Value != null && dicFeatureAttribute.ElementAt(i).Value.ToString().Trim() != "")
                        {
                            switch (pRow.Fields.Field[iFieldIndex].Type)
                            {
                                case esriFieldType.esriFieldTypeString:
                                    pRow.set_Value(iFieldIndex, Convert.ToString(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                                case esriFieldType.esriFieldTypeDouble:
                                    pRow.set_Value(iFieldIndex, Convert.ToDouble(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                                case esriFieldType.esriFieldTypeInteger:
                                    pRow.set_Value(iFieldIndex, Convert.ToInt32(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                                case esriFieldType.esriFieldTypeDate:
                                    pRow.set_Value(iFieldIndex, Convert.ToDateTime(dicFeatureAttribute.ElementAt(i).Value, System.Globalization.CultureInfo.CurrentCulture));
                                    break;
                                case esriFieldType.esriFieldTypeSmallInteger:
                                    pRow.set_Value(iFieldIndex, Convert.ToInt16(dicFeatureAttribute.ElementAt(i).Value));
                                    break;
                            }
                        }
                        else
                        {
                            pRow.set_Value(iFieldIndex != -1 ? iFieldIndex : i, null);
                        }
                    }
                    pCursor.UpdateRow(pRow);
                    pRow = pCursor.NextRow();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 删除要素
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="strWhereClause">String类型，筛选出要更新的要素的SQL条件语句</param>
        /// <returns></returns>
        public bool DeleteFeature(string strFeatureClassName, string strWhereClause)
        {
            try
            {
                bool bResult = DeleteFeature(this.pWorkSpace, strFeatureClassName, strWhereClause);
                return bResult;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 删除要素
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="strWhereClause">String类型，筛选出要更新的要素的SQL条件语句</param>
        /// <returns></returns>
        public bool DeleteFeature(IWorkspace pWorkspace, string strFeatureClassName, string strWhereClause)
        {
            try
            {
                IFeatureClass pFeataureClass = GetFeatureClass(pWorkspace, strFeatureClassName);
                IQueryFilter pQueryFilter = new QueryFilterClass();
                pQueryFilter.WhereClause = strWhereClause;
                IFeatureCursor pFeatureCursor = pFeataureClass.Update(pQueryFilter, true);
                IFeature pDeleteFeature = pFeatureCursor.NextFeature();
                while (pDeleteFeature != null)
                {
                    //提交更新
                    pFeatureCursor.DeleteFeature();

                    pDeleteFeature = pFeatureCursor.NextFeature();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 快速批量删除要素
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="strWhereClause">String类型，筛选出要更新的要素的SQL条件语句</param>
        /// <returns></returns>
        public bool QuickDeleteMoreFeature(string strFeatureClassName, string strWhereClause)
        {
            try
            {
                bool bResult = QuickDeleteMoreFeature(this.pWorkSpace, strFeatureClassName, strWhereClause);
                return bResult;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 快速批量删除要素
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="strWhereClause">String类型，筛选出要更新的要素的SQL条件语句</param>
        /// <returns></returns>
        public bool QuickDeleteMoreFeature(IWorkspace pWorkspace, string strFeatureClassName, string strWhereClause)
        {
            try
            {
                IFeatureClass pFeataureClass = GetFeatureClass(pWorkspace, strFeatureClassName);
                IQueryFilter pQueryFilter = new QueryFilterClass();
                pQueryFilter.WhereClause = strWhereClause;
                ITable pTable = pFeataureClass as ITable;
                pTable.DeleteSearchedRows(pQueryFilter);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 获取要素类
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <returns>IFeatureClass接口类型，返回要素类</returns>
        public IFeatureClass GetFeatureClass(string strFeatureClassName)
        {
            try
            {
                IFeatureClass pFeatureClass = GetFeatureClass(this.pWorkSpace, strFeatureClassName);
                return pFeatureClass;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 获取要素类
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <returns>IFeatureClass接口类型，返回要素类</returns>
        public IFeatureClass GetFeatureClass(IWorkspace pWorkspace, string strFeatureClassName)
        {
            IFeatureClass pFeatureClass = null;
            if (pWorkspace != null)
            {
                if (!(pWorkspace is IFeatureWorkspace))
                {
                    return pFeatureClass;
                }
                IFeatureWorkspace pFCWorkspace = pWorkspace as IFeatureWorkspace;
                try
                {
                    pFeatureClass = pFCWorkspace.OpenFeatureClass(strFeatureClassName);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return pFeatureClass;
        }

        /// <summary>
        /// 获取多边形相交的要素Objectid
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="listPointStruct">List&lt;PointStruct&gt;类型，相交图形中点的坐标值集合</param>
        /// <returns></returns>
        public List<Int32> GetIntersectFeatIdByPolygon(string strFeatureClassName, List<PointStruct> listPointStruct)
        {
            try
            {
                List<Int32> pList = GetIntersectFeatIdByPolygon(this.pWorkSpace, strFeatureClassName, listPointStruct);
                return pList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 获取多边形相交的要素Objectid
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="listPointStruct">List&lt;PointStruct&gt;类型，相交图形中点的坐标值集合</param>
        /// <returns></returns>
        public List<Int32> GetIntersectFeatIdByPolygon(IWorkspace pWorkspace, string strFeatureClassName, List<PointStruct> listPointStruct)
        {
            try
            {
                List<Int32> pList = new List<Int32>();
                IFeatureClass pFeataureClass = GetFeatureClass(pWorkspace, strFeatureClassName);

                //生成polygon
                IPointCollection pPointCollection = new PolygonClass();
                foreach (PointStruct oPointStruct in listPointStruct)
                {
                    IPoint pPoint = new PointClass();
                    pPoint.X = oPointStruct.X;
                    pPoint.Y = oPointStruct.Y;
                    pPointCollection.AddPoint(pPoint);
                }
                IPolygon pPolygon = pPointCollection as IPolygon;

                if (pPolygon != null && pFeataureClass != null)
                {
                    //创建空间查询
                    ISpatialFilter pSpatialFilter = new SpatialFilterClass();
                    pSpatialFilter.Geometry = pPolygon;
                    pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelIntersects;
                    IFeatureCursor pFeatureCursor = pFeataureClass.Search(pSpatialFilter, false);
                    IFeature pFeature = pFeatureCursor.NextFeature();
                    while (pFeature != null)
                    {
                        pList.Add(pFeature.OID);
                        pFeature = pFeatureCursor.NextFeature();
                    }
                }
                return pList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 获取圆形相交的要素Objectid
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="oCenterPointStruct">PointStruct类型，圆心点坐标</param>
        /// <param name="dRadius">double类型，圆半径</param>
        /// <returns></returns>
        public List<Int32> GetIntersectFeatureByCircle(string strFeatureClassName, PointStruct oCenterPointStruct, double dRadius)
        {
            try
            {
                List<Int32> pList = GetIntersectFeatureByCircle(this.pWorkSpace, strFeatureClassName, oCenterPointStruct, dRadius);
                return pList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 获取圆形相交的要素Objectid
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="oCenterPointStruct">PointStruct类型，圆心点坐标</param>
        /// <param name="dRadius">double类型，圆半径</param>
        /// <returns></returns>
        public List<Int32> GetIntersectFeatureByCircle(IWorkspace pWorkspace, string strFeatureClassName, PointStruct oCenterPointStruct, double dRadius)
        {
            try
            {
                List<Int32> pList = new List<Int32>();
                IFeatureClass pFeataureClass = GetFeatureClass(pWorkspace, strFeatureClassName);

                //生成圆心点
                IPoint pPoint = new PointClass();
                pPoint.X = oCenterPointStruct.X;
                pPoint.Y = oCenterPointStruct.Y;
                if (pFeataureClass != null)
                {
                    //构建圆对象
                    IConstructCircularArc pConstructCircularArc = new CircularArcClass();
                    pConstructCircularArc.ConstructCircle(pPoint, dRadius, false);
                    ICircularArc pCircularArc = pConstructCircularArc as ICircularArc;
                    ISegment pSegment = pCircularArc as ISegment;

                    //构建Ring对象
                    ISegmentCollection pSegmentCollection = new RingClass();
                    object oTemp = Type.Missing;

                    //添加Segement对象
                    pSegmentCollection.AddSegment(pSegment, ref oTemp, ref oTemp);

                    //封闭Ring对象
                    IRing pRing = pSegmentCollection as IRing;
                    pRing.Close();

                    //构建Polygon对象
                    IGeometryCollection pGeometryCollection = new PolygonClass();
                    pGeometryCollection.AddGeometry(pRing, ref oTemp, ref oTemp);

                    //转成IGeometry
                    IGeometry pGeometry = pGeometryCollection as IGeometry;

                    //创建空间查询
                    ISpatialFilter pSpatialFilter = new SpatialFilterClass();
                    pSpatialFilter.Geometry = pGeometry;
                    pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelIntersects;
                    IFeatureCursor pFeatureCursor = pFeataureClass.Search(pSpatialFilter, false);
                    IFeature pFeature = pFeatureCursor.NextFeature();
                    while (pFeature != null)
                    {
                        pList.Add(pFeature.OID);
                        pFeature = pFeatureCursor.NextFeature();
                    }
                }
                return pList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 获取线相交的要素Objectid
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="listPointStruct">List&lt;PointStruct&gt;类型，相交图形中点的坐标值集合</param>
        /// <returns></returns>
        public List<Int32> GetIntersectFeatureByLine(string strFeatureClassName, List<PointStruct> listPointStruct)
        {
            try
            {
                List<Int32> pList = GetIntersectFeatureByLine(this.pWorkSpace, strFeatureClassName, listPointStruct);
                return pList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 获取线相交的要素Objectid
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="listPointStruct">List&lt;PointStruct&gt;类型，相交图形中点的坐标值集合</param>
        /// <returns></returns>
        public List<Int32> GetIntersectFeatureByLine(IWorkspace pWorkspace, string strFeatureClassName, List<PointStruct> listPointStruct)
        {
            try
            {
                List<Int32> pList = new List<Int32>();
                IFeatureClass pFeataureClass = GetFeatureClass(pWorkspace, strFeatureClassName);

                //生成Line
                IPointCollection pPointCollection = new PolygonClass();
                foreach (PointStruct oPointStruct in listPointStruct)
                {
                    IPoint pPoint = new PointClass();
                    pPoint.X = oPointStruct.X;
                    pPoint.Y = oPointStruct.Y;
                    pPointCollection.AddPoint(pPoint);
                }
                ILine pLine = pPointCollection as ILine;
                if (pLine != null && pFeataureClass != null)
                {
                    //创建空间查询
                    ISpatialFilter pSpatialFilter = new SpatialFilterClass();
                    pSpatialFilter.Geometry = pLine;
                    pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelIntersects;
                    IFeatureCursor pFeatureCursor = pFeataureClass.Search(pSpatialFilter, false);
                    IFeature pFeature = pFeatureCursor.NextFeature();
                    while (pFeature != null)
                    {
                        pList.Add(pFeature.OID);
                        pFeature = pFeatureCursor.NextFeature();
                    }
                }
                return pList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 获取点相交的要素Objectid
        /// </summary>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="oPointStruct">PointStruct类型，点坐标</param>
        /// <returns></returns>
        public List<Int32> GetIntersectFeatureByPoint(string strFeatureClassName, PointStruct oPointStruct)
        {
            try
            {
                List<Int32> pList = GetIntersectFeatureByPoint(this.pWorkSpace, strFeatureClassName, oPointStruct);
                return pList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 获取点相交的要素Objectid
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="strFeatureClassName">String类型，要素类名称</param>
        /// <param name="oPointStruct">PointStruct类型，点坐标</param>
        /// <returns></returns>
        public List<Int32> GetIntersectFeatureByPoint(IWorkspace pWorkspace, string strFeatureClassName, PointStruct oPointStruct)
        {
            try
            {
                List<Int32> pList = new List<Int32>();
                IFeatureClass pFeataureClass = GetFeatureClass(pWorkspace, strFeatureClassName);

                //生成LPoint
                IPoint pPoint = new PointClass();
                pPoint.X = oPointStruct.X;
                pPoint.Y = oPointStruct.Y;
                if (pFeataureClass != null)
                {
                    //创建空间查询
                    ISpatialFilter pSpatialFilter = new SpatialFilterClass();
                    pSpatialFilter.Geometry = pPoint;
                    pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelIntersects;
                    IFeatureCursor pFeatureCursor = pFeataureClass.Search(pSpatialFilter, false);
                    IFeature pFeature = pFeatureCursor.NextFeature();
                    while (pFeature != null)
                    {
                        pList.Add(pFeature.OID);
                        pFeature = pFeatureCursor.NextFeature();
                    }
                }
                return pList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #region 动态方法

        #endregion

        #region 静态方法


        #endregion

        #endregion
    }
}
