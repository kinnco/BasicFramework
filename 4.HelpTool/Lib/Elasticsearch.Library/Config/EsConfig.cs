﻿using Microsoft.Extensions.Options;
using System.Collections.Generic;

namespace Elasticsearch.Library
{
    /// <summary>
    /// ElasticSearch配置类
    /// </summary>
    public class EsConfig : IOptions<EsConfig>
    {
        /// <summary>
        /// 地址信息
        /// </summary>
        public List<string> Urls { get; set; }

        /// <summary>
        /// 是否加密
        /// </summary>
        public bool IsEncryption { get; set; }

        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        public string Ex { get; set; }

        /// <summary>
        /// 配置
        /// </summary>
        public EsConfig Value => this;

    }
}