﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Common.Library
{
    /// <summary>
    /// 时间操作帮助类
    /// </summary>
    public static class TimeHelper
    {
        /// <summary>
        /// 获取两个时间段之间的所有天时间
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public static List<string> GetDayTimeList(string startTime, string endTime)
        {
            List<string> timeList = new List<string>();
            //首先保证 rq1<=rq2
            DateTime time1 = Convert.ToDateTime(startTime);
            DateTime time2 = Convert.ToDateTime(endTime);
            while (time1 <= time2)
            {
                timeList.Add(time1.ToString("yyyy-MM-dd"));
                time1 = time1.AddDays(1);
            }
            return timeList;
        }

        /// <summary>
        /// 获取两个时间段之间的所有月份时间
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public static List<string> GetMonthTimeList(string startTime, string endTime)
        {
            List<string> timeList = new List<string>();
            //首先保证 rq1<=rq2
            DateTime time1 = Convert.ToDateTime(startTime);
            DateTime time2 = Convert.ToDateTime(endTime);
            while (time1 <= time2)
            {
                timeList.Add(time1.ToString("yyyy-MM"));
                time1 = time1.AddMonths(1);
            }
            return timeList;
        }

        /// <summary>
        /// 获取两个时间段之间的所有月份时间
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public static List<string> GetMonthTimeList(DateTime startTime, DateTime endTime)
        {
            List<string> timeList = new List<string>();
            while (startTime <= endTime)
            {
                timeList.Add(startTime.ToString("yyyy-MM"));
                startTime = startTime.AddMonths(1);
            }
            return timeList;
        }

        /// <summary>
        /// 获取对应时间月份第一天
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime GetMonthFirstTime(DateTime dateTime)
        {
            //获取某年某月有多少天
            int monthDay = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);

            DateTime firstTime = new DateTime(dateTime.Year, dateTime.Month, 1);

            return firstTime;
        }

        /// <summary>
        /// 获取对应时间月份最后一天
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime GetMonthLastTime(DateTime dateTime)
        {
            //获取某年某月有多少天
            int monthDay = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);

            DateTime lastTime = new DateTime(dateTime.Year, dateTime.Month, monthDay);

            return lastTime;
        }


        /// <summary>
        ///获取时间差
        /// </summary>
        /// <param name="dateBegin">开始时间</param>
        /// <param name="dateEnd">结束时间</param>
        /// <returns>返回(秒)单位，比如: 0.00239秒</returns>
        public static string GetDateDiff(DateTime dateBegin, DateTime dateEnd)
        {
            TimeSpan ts1 = new TimeSpan(dateBegin.Ticks);
            TimeSpan ts2 = new TimeSpan(dateEnd.Ticks);
            TimeSpan ts3 = ts1.Subtract(ts2).Duration();
            //你想转的格式
            return ts3.TotalHours.ToString("F2");
        }

        /// <summary>
        ///获取时间差
        /// </summary>
        /// <param name="dateBegin">开始时间</param>
        /// <param name="dateEnd">结束时间</param>
        /// <returns>返回(天)单位，比如: </returns>
        public static int GetDateDiffDay(DateTime dateBegin, DateTime dateEnd)
        {
            TimeSpan ts1 = new TimeSpan(dateBegin.Ticks);
            TimeSpan ts2 = new TimeSpan(dateEnd.Ticks);
            TimeSpan ts3 = ts1.Subtract(ts2).Duration();
            return ts3.Days + 1;
        }

        /// <summary>
        /// 根据出生日期计算年龄
        /// </summary>
        /// <param name="dtBirthday"></param>
        /// <param name="dtNow"></param>
        /// <returns></returns>
        public static int GetAge(DateTime dtBirthday)
        {
            DateTime dtNow = DateTime.Now;
           int age= dtNow.Year - dtBirthday.Year;
            if(dtNow.Month< dtBirthday.Month)
            {
                age--;
            }
            return age;
        }

        /// <summary>  
        /// 将c# DateTime时间格式转换为Unix时间戳格式【相差毫秒数】
        /// </summary>  
        /// <param name="time">时间</param>  
        /// <returns>long</returns>  
        public static long ConvertDateTimeToLong(DateTime time)
        {
            DateTime startTime = TimeZoneInfo.ConvertTime(new System.DateTime(1970, 1, 1), TimeZoneInfo.Local); // 当地时区
            long timeStamp = (long)(time - startTime).TotalMilliseconds; // 相差毫秒数
            return timeStamp;
        }

        /// <summary>
        /// 获取时间对应当天开始时间
        /// </summary>
        /// <param name="todayStart"></param>
        /// <returns></returns>
        public static DateTime GetStartTime(DateTime todayStart)
        {
            var year = todayStart.Year;
            var month = todayStart.Month;
            var day = todayStart.Day;
            var start = new DateTime(year, month, day, 0, 0, 0);
            return start;
        }

        /// <summary>
        /// 获取时间对应当天结束时间
        /// </summary>
        /// <param name="todayEnd"></param>
        /// <returns></returns>
        public static DateTime GetEndTime(DateTime todayEnd)
        {
            var year = todayEnd.Year;
            var month = todayEnd.Month;
            var day = todayEnd.Day;
            var endTime = new DateTime(year, month, day, 23, 59, 59);
            return endTime;
        }
    }
}
