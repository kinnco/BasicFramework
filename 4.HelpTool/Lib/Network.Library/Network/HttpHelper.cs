﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Net;

namespace Network.Library
{
    public static class HttpHelper
    {

        /// <summary>
        /// 获取JwtUserId
        /// </summary>
        /// <returns></returns>
        public static string GetJwtUserId() 
        {
            var result = string.Empty;

            var HttpContextInfo = HttpContextHelper.HttpContext;

            if (HttpContextInfo != null && HttpContextInfo.User != null && HttpContextInfo.User.FindFirst("id") != null)
            {
                result = HttpContextInfo.User.FindFirst("id").Value;
            }

            return result;
        }

        /// <summary>
        /// 判断是否是ajax请求
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static bool IsAjaxRequest(this HttpRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            return (request.Headers != null) && (request.Headers["X-Requested-With"] == "XMLHttpRequest");
        }

        /// <summary>
        /// 获得当前页面客户端的IP
        /// </summary>
        /// <returns>当前页面客户端的IP</returns>
        public static string GetIP()
        {
            string result = string.Empty;
            var headers = HttpContextHelper.HttpContext.Request.Headers;
            if (headers.Keys.Contains("X-Real-IP"))
            {
                var forwarded = headers["X-Forwarded-For"].ToString();
                if (!string.IsNullOrEmpty(forwarded))
                {
                    //多次反向代理后会有多个ip值，第一个ip才是真实ip
                    int index = forwarded.IndexOf(",");
                    if (index != -1)
                    {
                        result = forwarded.Substring(0, index);
                    }
                    else
                    {
                        result = forwarded;
                    }
                }
                else
                {
                    result = headers["X-Real-IP"];
                }
            }
            else
            {
                result = HttpContextHelper.HttpContext.Connection.RemoteIpAddress.ToString();
                if (result.Equals("::1"))
                {
                    IPHostEntry myEntry = Dns.GetHostEntry(Dns.GetHostName());
                    result = myEntry.AddressList.FirstOrDefault(e => e.AddressFamily.ToString().Equals("InterNetwork")).ToString();
                }
            }
            return result;
        }

        /// <summary>
        /// 获取本机局域网IP
        /// </summary>
        /// <returns></returns>
        public static string GetInternalIp()
        {
            IPHostEntry myEntry = Dns.GetHostEntry(Dns.GetHostName());
            return myEntry.AddressList.FirstOrDefault(e => e.AddressFamily.ToString().Equals("InterNetwork")).ToString();
        }

    }
}
