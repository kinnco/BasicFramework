﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Dapper.Library
{
    public abstract class IResolveExpression
	{
		protected SqlProvider provider;
		protected IProviderOption providerOption;
		protected AbstractSet abstractSet => provider.Context.Set;

		public IResolveExpression(SqlProvider provider)
		{
			this.provider = provider;
			this.providerOption = provider.ProviderOption;
		}

		/// <summary>
		/// 字段列列表
		/// </summary>
		private static Hashtable _tableFieldMap = new Hashtable();

		/// <summary>
		/// 根据反射对象获取表字段
		/// </summary>
		/// <param name="propertyInfos"></param>
		/// <returns></returns>
		public virtual string GetTableField(EntityObject entityObject)
		{
			lock (_tableFieldMap)
			{
				string fieldBuild = (string)_tableFieldMap[entityObject];
				if (fieldBuild == null)
				{
					///////////////////////LW修改 /////////////////////////////////////////////////////////////////////providerOption.CombineFieldName(entityObject.AsName)
					var propertyInfos = entityObject.Properties;
					string asName = entityObject.Name == entityObject.AsName ? entityObject.Name : entityObject.AsName;
					fieldBuild = string.Join(",", entityObject.FieldPairs.Select(field => $"{asName}.{providerOption.CombineFieldName(field.Value) }"));
					_tableFieldMap.Add(entityObject, fieldBuild);
				}
				return fieldBuild;
			}
		}

		/// <summary>
		/// 解析查询字段
		/// </summary>
		/// <param name="topNum"></param>
		/// <returns></returns>
		public abstract string ResolveSelect(int? topNum);

		/// <summary>
		/// 解析查询条件
		/// </summary>
		/// <param name="prefix"></param>
		/// <returns></returns>
		public virtual string ResolveWhereList(string prefix = null)
		{
			//添加Linq生成的sql条件和参数
			List<LambdaExpression> lambdaExpressionList = abstractSet.WhereExpressionList;
			StringBuilder builder = new StringBuilder("WHERE 1=1 ");
			for (int i = 0; i < lambdaExpressionList.Count; i++)
			{
				var whereParam = new WhereExpression(lambdaExpressionList[i], $"{prefix}_{i}", provider);
				builder.Append(whereParam.SqlCmd);
				//参数
				foreach (var paramKey in whereParam.Param.ParameterNames)
				{
					abstractSet.Params.Add(paramKey, whereParam.Param.Get<object>(paramKey));
				}
			}
			//添加自定义sql生成的条件和参数
			if (abstractSet.WhereBuilder != null && abstractSet.WhereBuilder.Length != 0)
			{
				//添加自定义条件sql
				builder.Append(abstractSet.WhereBuilder);
			}
			return builder.ToString();
		}

		/// <summary>
		/// 解析分组
		/// </summary>
		/// <returns></returns>
		public virtual string ResolveGroupBy()
		{
			StringBuilder builder = new StringBuilder();
			var groupExpression = abstractSet.GroupExpressionList;
			if (groupExpression != null && groupExpression.Any())
			{
				for (int i = 0; i < groupExpression.Count; i++)
				{
					var groupParam = new GroupExpression(groupExpression[i], $"Group_{i}", provider);
					if (builder.Length != 0)
						builder.Append(",");
					builder.Append(groupParam.SqlCmd);
				}
				builder.Insert(0, " GROUP BY ");
			}
			return builder.ToString();
		}

		/// <summary>
		/// 解析分组聚合条件
		/// </summary>
		/// <returns></returns>
		public virtual string ResolveHaving()
		{
			StringBuilder builder = new StringBuilder();
			var havingExpression = abstractSet.HavingExpressionList;
			if (havingExpression != null && havingExpression.Any())
			{
				for (int i = 0; i < havingExpression.Count; i++)
				{
					var whereParam = new WhereExpression(havingExpression[i], $"Having_{i}", provider);
					builder.Append(whereParam.SqlCmd);
					//参数
					foreach (var paramKey in whereParam.Param.ParameterNames)
					{
						abstractSet.Params.Add(paramKey, whereParam.Param.Get<object>(paramKey));
					}
				}
				builder.Insert(0, " Having 1=1 ");
			}
			return builder.ToString();
		}

		/// <summary>
		/// 解析排序
		/// </summary>
		/// <returns></returns>
		public virtual string ResolveOrderBy(DBType type)
		{
			string orderStr = string.Empty;

			var orderByList = abstractSet?.OrderbyExpressionList.Select(a =>
			{
				var entity = EntityCache.QueryEntity(a.Key.Type.GenericTypeArguments[0], provider);
				var columnName = a.Key.Body.GetCorrectPropertyName();
				return $"{entity.GetAsName(providerOption)}" + providerOption.CombineFieldName(columnName) + (a.Value == EOrderBy.Asc ? " ASC " : " DESC ");
			}) ?? new List<string>();

			if (!orderByList.Any() && (abstractSet.OrderbyBuilder == null || abstractSet.OrderbyBuilder.Length == 0))
				return "";

			if (type == DBType.PostgreSql)
			{
				string orderByListStr = orderByList.Count() > 0 ? $" {string.Join(",", orderByList)}" : "";
				string orderbyStr = string.Empty;
				if (abstractSet.OrderbyBuilder.ToString().IndexOf(" DESC")>0)
				{
					orderbyStr = String.Join("\",\"", abstractSet.OrderbyBuilder.ToString().Substring(0, abstractSet.OrderbyBuilder.ToString().Length-5).Split(',').ToList());

                    orderStr = $"ORDER BY  ";
                    if (!string.IsNullOrEmpty(orderByListStr))
                    {
                        orderStr += orderByListStr+ " ,";
                    }
					orderStr += $"{$"\"{orderbyStr}\""} DESC";
				}
				else
				{
                    string orderbyBuilder = abstractSet.OrderbyBuilder.ToString();

                    if (!string.IsNullOrEmpty(orderbyBuilder))
                    {
                        orderbyStr = String.Join("\",\"", orderbyBuilder.Split(',').ToList());
                        orderStr = $"ORDER BY  ";
                        if (!string.IsNullOrEmpty(orderByListStr))
                        {
                            orderStr += orderByListStr + " ,";
                        }
                        orderStr += $"{$"\"{orderbyStr}\""}";
                    }
                    else
                    {
                        orderStr = $"ORDER BY  {orderByListStr}";
                    }
				}
			}
			else
			{
				orderStr=$"ORDER BY {string.Join(",", orderByList)} {abstractSet.OrderbyBuilder}";
			}
			return orderStr;
		}


		/// <summary>
		/// 解析排序///////////////////////LW添加/////////////////////////////////////////////////////////////////////
		/// </summary>
		/// <returns></returns>
		public virtual string ResolveIfNotExists()
		{
			string ifNotExistsStr = string.Empty;

			var ifNotExistsList = abstractSet?.IfNotExistsList.Select(a =>
			{
				var entity = EntityCache.QueryEntity(a.Type.GenericTypeArguments[0], provider);
				var columnName = a.Body.GetCorrectPropertyName();
				return  providerOption.CombineFieldName(columnName);
			}) ?? new List<string>();

			if (!ifNotExistsList.Any())
				return "";

			ifNotExistsStr =string.Join(",", ifNotExistsList);

			return ifNotExistsStr;
		}


		/// <summary>
		/// 解析查询总和
		/// </summary>
		/// <param name="selector"></param>
		/// <returns></returns>
		public abstract string ResolveSum(LambdaExpression selector);

        /// <summary>
        /// 解析查询最大值 
        /// </summary>
        /// <param name="selector"></param>
        /// <returns></returns>
        public abstract string ResolveMax(LambdaExpression selector);

        /// <summary>
        /// 解析查询最小值
        /// </summary>
        /// <param name="selector"></param>
        /// <returns></returns>
        public abstract string ResolveMin(LambdaExpression selector);

		/// <summary>
		/// 解析查询更新
		/// </summary>
		/// <param name="entityObject"></param>
		/// <param name="selector"></param>
		/// <returns></returns>
		public virtual string ResolveSelectOfUpdate(EntityObject entityObject, LambdaExpression selector)
		{
			var selectSql = "";
			if (selector == null)
			{
				var propertyBuilder = new StringBuilder();
				foreach (var propertyInfo in entityObject.Properties)
				{
					if (propertyBuilder.Length > 0)
						propertyBuilder.Append(",");
					propertyBuilder.AppendFormat($"INSERTED.{ providerOption.CombineFieldName(propertyInfo.GetColumnAttributeName())} { providerOption.CombineFieldName(propertyInfo.Name)}");
				}
				selectSql = propertyBuilder.ToString();
			}
			else
			{
				var nodeType = selector.Body.NodeType;
				if (nodeType == ExpressionType.MemberAccess)
				{
					var columnName = ((MemberExpression)selector.Body).Member.GetColumnAttributeName();
					selectSql = "INSERTED." + providerOption.CombineFieldName(columnName);
				}
				else if (nodeType == ExpressionType.MemberInit)
				{
					var memberInitExpression = (MemberInitExpression)selector.Body;
					selectSql = string.Join(",", memberInitExpression.Bindings.Select(a => "INSERTED." + providerOption.CombineFieldName(a.Member.GetColumnAttributeName())));
				}
			}
			return "OUTPUT " + selectSql;
		}
		/// <summary>
		/// 解析更新
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="updateExpression"></param>
		/// <returns></returns>
		public virtual UpdateExpression ResolveUpdate<T>(Expression<Func<T, T>> updateExpression)
		{
			return new UpdateExpression(updateExpression, provider);
		}

		/// <summary>
		/// 解析更新语句
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="t"></param>
		/// <param name="Param"></param>
		/// <param name="excludeFields"></param>
		/// <returns></returns>
		public virtual string ResolveUpdates<T>(T t, DynamicParameters Param, string[] excludeFields)
		{
			var entity = EntityCache.QueryEntity(t.GetType(), provider);
			var properties = entity.Properties;
			StringBuilder builder = new StringBuilder();
			foreach (var propertiy in properties)
			{
				//是否是排除字段
				if (excludeFields != null && excludeFields.Contains(propertiy.Name))
				{
					continue;
				}
				var customAttributes = propertiy.GetCustomAttributess(true);
				//导航属性排除
				if (customAttributes.Any(x => x.GetType().Equals(typeof(ForeignKey))))
				{
					continue;
				}

                //自增组件主键标识
                var dbFieldInfo = propertiy.GetCustomAttribute<DBFieldInfo>();
                bool isIncrease = dbFieldInfo.IsIncrease;
                if (isIncrease)
                {
                    continue;
                }

                object value = propertiy.GetValue(t);
				string name = entity.FieldPairs[propertiy.Name];
				if (builder.Length != 0)
				{
					builder.Append(",");
				}
				builder.Append($"{providerOption.CombineFieldName(name)}={providerOption.ParameterPrefix}Update_{name}");

                ///LW添加 如果是Oracle,那就将 bool 转换成 String
                if (value != null&&bool.TryParse(value.ToString(), out bool boolPar) && provider.ProviderOption.ToString().Equals("Dapper.Library.DapperOracle.ProviderOption"))
                {
                    value = value.ToString().ToLower();
                }

                Param.Add($"{providerOption.ParameterPrefix}Update_{name}", value);
			}
			builder.Insert(0, " SET ");
			return builder.ToString();
		}
		public virtual string ResolveWithNoLock(bool nolock)
		{
			return nolock ? "(NOLOCK)" : "";
		}

		/// <summary>
		/// 解析连表查询
		/// </summary>
		/// <param name="joinAssTables"></param>
		/// <param name="sql"></param>
		/// <returns></returns>
		public virtual string ResolveJoinSql(List<JoinAssTable> joinAssTables, ref string sql)
		{
			StringBuilder builder = new StringBuilder(Environment.NewLine);
			if (joinAssTables.Count != 0)
			{
				sql = sql.TrimEnd();
				//循环拼接连表对象
				for (int i = 0; i < joinAssTables.Count; i++)
				{
					//当前连表对象
					var item = joinAssTables[i];
					if (item.IsMapperField == false)
					{
						continue;
					}
					item.MapperList.Clear();
					//连表实体
					var leftEntity = EntityCache.QueryEntity(item.TableType, provider);
					//默认连表
					if (item.Action == JoinAction.Default || item.Action == JoinAction.Navigation)
					{
						string leftTable = providerOption.CombineFieldName(item.LeftTabName);
						builder.Append($@" {item.JoinMode.ToString()} JOIN 
                                       {leftTable} {leftEntity.AsName} ON {leftEntity.AsName}
                                      .{providerOption.CombineFieldName(item.LeftAssName)} = {providerOption.CombineFieldName(item.RightTabName)}
                                      .{providerOption.CombineFieldName(item.RightAssName)} " + Environment.NewLine);
					}
					else//sql连表
					{
						builder.Append(" " + item.JoinSql);
						//判断是否需要显示连表的字段
						if (!item.IsMapperField)
						{
							continue;
						}
					}
					FieldDetailWith(ref sql, item, leftEntity);
				}
			}
			return builder.ToString();
		}

		/// <summary>
		/// 字段处理
		/// </summary>
		/// <param name="masterSql"></param>
		/// <param name="joinAssTable"></param>
		/// <param name="joinEntity"></param>
		/// <returns></returns>
		private string FieldDetailWith(ref string masterSql, JoinAssTable joinAssTable, EntityObject joinEntity)
		{
			StringBuilder sqlBuilder = new StringBuilder();

			/////////////////////////////////////LW修改 providerOption.CombineFieldName(joinEntity.Name)
			//表名称
			string joinTableName = joinEntity.AsName == joinEntity.Name ? joinEntity.Name : joinEntity.AsName;
			//查询的字段
			var fieldPairs = joinAssTable.SelectFieldPairs != null && joinAssTable.SelectFieldPairs.Any() ? joinAssTable.SelectFieldPairs : joinEntity.FieldPairs;
			foreach (string fieldValue in fieldPairs.Values)
			{
				if (masterSql.LastIndexOf(',') == masterSql.Length - 1 && sqlBuilder.Length == 0)
					sqlBuilder.Append($"{joinTableName}.");
				else
					//首先添加表名称
					sqlBuilder.Append($",{joinTableName}.");
				//字段
				string field = providerOption.CombineFieldName(fieldValue);
				//字符出现的次数
				int repeatCount = masterSql.Split(new string[] { field }, StringSplitOptions.None).Length - 1;
				//添加字段
				sqlBuilder.Append(field);
				if (repeatCount > 0)
				{
					sqlBuilder.Append($" AS {fieldValue}_{repeatCount}");
					joinAssTable.MapperList.Add(fieldValue, $"{fieldValue}_{repeatCount}");
				}
				else
				{
					joinAssTable.MapperList.Add(fieldValue, fieldValue);
				}
			}
			var joinEntityType = joinAssTable.IsDto == false ? joinEntity.Type : joinAssTable.DtoType;

            //LW注释掉了
			//重新注册实体映射
			//SqlMapper.SetTypeMap(joinEntityType, new CustomPropertyTypeMap(joinEntityType,
			//		(type, column) =>
			//		type.GetPropertys(joinAssTable.MapperList.FirstOrDefault(x => x.Value.Equals(column)).Key)
			//		), true);

			//设置sql字段
			masterSql += sqlBuilder;
			return masterSql;
		}
	}
}
