﻿/*
* 命名空间: Dapper.Library
*
* 功 能： 数据库数据表信息
*
* 类 名： DBTableInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/13 15:57:22 Harvey     创建
*
* Copyright (c) 2020 SCZH Corporation. All rights reserved.
*/

using System;

namespace Dapper.Library
{
    /// <summary>
    /// 数据表信息
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class DBTableInfo : Attribute
    {

        /// <summary>
        /// 模型名称
        /// </summary>
        public string Schema { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }
    }
}
