﻿/*
* 命名空间: Dapper.Library
*
* 功 能： 数据库数据字段信息
*
* 类 名： DBFieldInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/13 15:57:22 Harvey     创建
*
* Copyright (c) 2020 SCZH Corporation. All rights reserved.
*/
namespace Dapper.Library
{
    using System;

    /// <summary>
    /// 数据字段信息
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DBFieldInfo : Attribute
    {

        /// <summary>
        /// 是否是主键
        /// </summary>
        public bool IsPrimarykey { get; set; }

        /// <summary>
        /// 是否是自增序列
        /// </summary>
        public bool IsIncrease { get; set; }

        /// <summary>
        /// 字节长度
        /// </summary>
        public int ByteLength { get; set; }

        /// <summary>
        /// 长度
        /// </summary>
        public int DataLength { get; set; }

        /// <summary>
        /// 小数位数
        /// </summary>
        public int DecimalDigits { get; set; }

        /// <summary>
        /// 数据字段是否是必填(不能为空)
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// 列在数据库的名称
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// 列的描述
        /// </summary>
        public string ColumnDescribe { get; set; }

        /// <summary>
        /// 默认值（参考）
        /// </summary>
        public string DefaultValue { get; set; }
    }
}
