﻿using System.Collections.Generic;

namespace Dapper.Library
{
    /// <summary>
    /// 配置文件组
    /// </summary>
    internal class ConfigFiles
    {
        /// <summary>
        /// 配置文件集合
        /// </summary>
        public List<ConfigFile> InstanceList { get; set; }
    }
}
