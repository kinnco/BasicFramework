﻿using System.Collections.Generic;

namespace Dapper.Library.SqlContainer
{
    /// <summary>
    /// 数据库实例配置对象
    /// </summary>
    public class DatabaseInstances
    {
        /// <summary>
		/// 注入命令的集合
		/// </summary>
        public List<DatabaseInstance> InstanceList { get; set; }
    }
}
