﻿namespace Dapper.Library.SqlContainer
{
	using System.Xml.Serialization;
	/// <summary>
	/// 数据库的配置
	/// </summary>
	[XmlRoot("databases", Namespace = "http://www.Framework.com/Framework/DataAccess")]
	public class Databases
	{
		/// <summary>
		/// 数据库的分组数组
		/// </summary>
		[XmlElement("dbGroup")]
		public DatabaseGroup[] GroupList { get; set; }
	}
}
