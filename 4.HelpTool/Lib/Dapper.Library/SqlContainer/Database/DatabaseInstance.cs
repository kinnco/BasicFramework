﻿using System.Xml.Serialization;

namespace Dapper.Library.SqlContainer
{
    /// <summary>
    /// 数据库实例配置对象
    /// </summary>
    public class DatabaseInstance
    {
        /// <summary>
        /// 数据库实例名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 数据库类型
        /// </summary>
        public DatabaseType Type { get; set; }

        /// <summary>
        /// 是否加密
        /// </summary>
        public string IsEncryption { get; set; }

        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnectionString { get; set; }
    }
}
