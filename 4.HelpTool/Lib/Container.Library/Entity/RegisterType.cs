﻿namespace Container.Library
{
    /// <summary>
    /// 注入类型信息
    /// </summary>
    public class RegisterType
    {
        /// <summary>
        /// 接口地址：类型所在程序集名称,命名空间.类型名
        /// 授权实体传输映射命名空间
        /// </summary>
        public string InterfaceOrNamespace { get; set; }

        /// <summary>
        /// 实现逻辑地址：类型所在程序集名称,命名空间.类型名
        /// 传输映射配置文件名称
        /// </summary>
        public string ImplementOrProfile { get; set; }
    }
}
