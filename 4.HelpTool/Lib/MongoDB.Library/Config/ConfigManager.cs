﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MongoDB.Library
{
    /// <summary>
    /// 用于读取缓存的配置信息
    /// </summary>
    public class ConfigManager
    {
        /// <summary>
        /// 数据库配置信息
        /// </summary>
        public static MongoDBConfig config { get; private set; }

        /// <summary>
        /// 静态代码块，程序启动时初始化
        /// </summary>
        static ConfigManager()
        {
            try
            {
                string rootDic = AppContext.BaseDirectory;
                //string rootDic = AppDomain.CurrentDomain.BaseDirectory;
                using (StreamReader sr = new StreamReader(Path.Combine(rootDic, "Config/Mongodb/mongodb.config.json")))
                {
                    config = sr.ReadToEnd().JsonToObject<MongoDBConfig>();
                    if (config.IsEncryption)
                    {
                        config.ConnectionString = AESEncryptHelper.Decrypt(config.ConnectionString);
                    }
                    config.Success = true;
                }
            }
            catch (Exception ex)
            {
                config.Success = false;
                config.Ex = ex.Message;
            }
        }
    }
}
