﻿using Common.Library;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MongoDB.Library
{
    /// <summary>
    /// MongoDB扩展类
    /// </summary>
    public class MongoDBHelper<T> where T : new()
	{
		// redis配置信息
		private static MongoDBConfig mongodbConfig;
		private IMongoCollection<T> collection = null;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="config">配置对象</param>
		public MongoDBHelper()
		{
			mongodbConfig = ConfigManager.config;
			if (string.IsNullOrWhiteSpace(mongodbConfig.ConnectionString))
			{
				throw new Exception("mongodbConfig.ConnectionString不能为空");
			}
			if (string.IsNullOrWhiteSpace(mongodbConfig.DataBase))
			{
				throw new Exception("mongodbConfig.DataBase不能为空");
			}
		}

		/// <summary>
		/// 获取集合
		/// </summary>
		/// <typeparam name="T">集合对应的类型</typeparam>
		/// <returns>集合对象</returns>
		private void GetCollection()
		{
			var colName = AttributesAssemblyHelper.GetClassAttributes<DescriptionAttribute>(new T()).FirstOrDefault().Description;
			MongoClient client = new MongoClient(mongodbConfig.ConnectionString);
			collection = client.GetDatabase(mongodbConfig.DataBase).GetCollection<T>(colName);
		}


		/// <summary>
		/// 根据一个表示来查询单个匹配的数据
		/// </summary>
		/// <typeparam name="T">查询结果的类型</typeparam>
		/// <typeparam name="TField">表示表达式中要获取的对应字段的类型</typeparam>
		/// <param name="fieldExpress">匹配字段的表达式</param>
		/// <param name="value">匹配的值</param>
		/// <returns>查询结果</returns>
		public  T FindOne<TField>(Expression<Func<T, TField>> fieldExpress, TField value)
		{
			try
			{
				var equal = Builders<T>.Filter.Eq(fieldExpress, value);
				return collection.FindAsync<T>(equal).GetAwaiter().GetResult().FirstOrDefault();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// 根据表达式查询数据
		/// </summary>
		/// <typeparam name="T">查询的数据类型</typeparam>
		/// <param name="express">表达式</param>
		/// <returns>查询结果</returns>
		public IList<T> FindDataByExpress(Expression<Func<T, bool>> express)
		{
			try
			{
				GetCollection();

				return collection.AsQueryable().Where(express).ToList<T>();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// 根据表达式查询排序数据
		/// </summary>
		/// <param name="express"></param>
		/// <param name="orderBy">排序表达式</param>
		/// <param name="sort">1 ASC 2 DESC</param>
		/// <returns></returns>
		public IList<T> FindOrderDataByExpress(Expression<Func<T, bool>> express, Expression<Func<T, object>> orderBy, int sort)
		{
			try
			{
				GetCollection();

				if (sort == 1)
				{
					//var result= collection.Find(express).SortBy(orderBy).ToList<T>();
					return collection.Find(express).SortBy(orderBy).ToList<T>();
				}
				else
				{
					return collection.Find(express).SortByDescending(orderBy).ToList<T>();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// 获取所有
		/// </summary>
		/// <returns></returns>
		public IList<T> FindAll()
		{
			try
			{
				GetCollection();

				return collection.AsQueryable().ToList<T>();
			}
			catch (Exception ex)
			{

				throw ex;
			}
		}


		/// <summary>
		/// 插入自增列
		/// </summary>
		/// <typeparam name="TConditionField"></typeparam>
		/// <typeparam name="TSetField"></typeparam>
		/// <param name="conditionExpress"></param>
		/// <param name="conditionValue"></param>
		/// <param name="setExpress"></param>
		/// <param name="setValue"></param>
		/// <returns></returns>
		public T InsertNextSequence<TConditionField, TSetField>(Expression<Func<T, TConditionField>> conditionExpress, TConditionField conditionValue,
																Expression<Func<T, int>> setExpress,
																Func<T, T> action)
		{
			try
			{
				GetCollection();

				var options = new FindOneAndUpdateOptions<T, T>() { IsUpsert = true };
				var filter = Builders<T>.Filter.Eq(conditionExpress, conditionValue);
				var update = Builders<T>.Update.Inc(setExpress, 1);
				return action(collection.FindOneAndUpdate<T>(filter, update, options));
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}


		/// <summary>
		/// 插入一条数据
		/// </summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="t">数据对象</param>
		/// <returns>数据对象</returns>
		public T InsertData(T t)
		{
			try
			{
				GetCollection();
				collection.InsertOne(t);
				return t;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// 插入多条数据
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		public List<T> InsertManyData(List<T> t)
		{
			try
			{
				GetCollection();
				collection.InsertMany(t);
				return t;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// 更新数据库
		/// </summary>
		/// <typeparam name="TConditionField">条件类型</typeparam>
		/// <typeparam name="TSetField">更新类型</typeparam>
		/// <param name="conditionExpress">条件表达式</param>
		/// <param name="conditionValue">条件表达式值</param>
		/// <param name="setExpress">更新字段表达式</param>
		/// <param name="setValue">更新字段值</param>
		/// <returns></returns>
		public long UpdateMany<TConditionField, TSetField>(Expression<Func<T, TConditionField>> conditionExpress, TConditionField conditionValue, Expression<Func<T, TSetField>> setExpress, TSetField setValue)
		{
			try
			{
				GetCollection();
				var filter = Builders<T>.Filter.Eq(conditionExpress, conditionValue);
				var update = Builders<T>.Update.Set(setExpress, setValue);
				return collection.UpdateMany(filter, update).ModifiedCount;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// 删除多条数据
		/// </summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="fieldExpress">匹配字段的表达式</param>        
		/// <returns>执行结果</returns>
		public long DeleteMany(Expression<Func<T, bool>> fieldExpress)
		{
			try
			{
				GetCollection();
				return collection.DeleteMany<T>(fieldExpress).DeletedCount;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// 删除单条数据
		/// </summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="fieldExpress">匹配数据的表达式</param>        
		/// <returns>执行结果</returns>
		public bool DeleteOne(Expression<Func<T, bool>> fieldExpress)
		{
			try
			{
				GetCollection();
				return collection.DeleteOne<T>(fieldExpress).DeletedCount > 0;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
