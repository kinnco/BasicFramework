﻿namespace Scheduler.Library
{
    using System;

    /// <summary>
    /// 任务的摘要信息
    /// </summary>
    public class TaskSummary
    {
        /// <summary>
        /// 任务的名称
        /// </summary>
        public string Name { get; set; } = Guid.NewGuid().ToString().Replace("-", "").ToUpper();

        /// <summary>
        /// 分组
        /// </summary>
        public string Group { get; set; } = Guid.NewGuid().ToString().Replace("-", "").ToUpper();

        /// <summary>
        /// 任务的描述
        /// </summary>
        public string Description { get; set; } = "默认执行的任务";

        /// <summary>
        /// 最后一次处理任务的时间
        /// </summary>
        public DateTime LastProcessDate { get; } = DateTime.MinValue;

        /// <summary>
        /// 任务已经执行的次数
        /// </summary>
        public int NumberOfTimesProcessed { get; } = 0;


        /// <summary>
        /// 自上次处理日期以来的时间
        /// </summary>
        public TimeSpan ElapsedTimeSinceLastProcessDate { get; } = new TimeSpan(0);

        /// <summary>
        /// 任务是否可运行
        /// </summary>
        public bool IsActive { get; } = false;

        /// <summary>
        /// 不通过默认构造函数构建对象
        /// </summary>
        private TaskSummary() { }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name">任务名称</param>
        /// <param name="lastProcessDate">最后一次处理任务的时间</param>
        /// <param name="numberOfTimesProcessed">任务已经执行的次数</param>
        /// <param name="elaspedTime">自上次处理日期以来的时间</param>
        /// <param name="isEnabled">任务是否可运行</param>
        public TaskSummary(string name, DateTime lastProcessDate, int numberOfTimesProcessed, TimeSpan elaspedTime, bool isEnabled)
        {
            Name = name;
            NumberOfTimesProcessed = numberOfTimesProcessed;
            LastProcessDate = lastProcessDate;
            ElapsedTimeSinceLastProcessDate = elaspedTime;
            IsActive = isEnabled;
        }
    }
}
