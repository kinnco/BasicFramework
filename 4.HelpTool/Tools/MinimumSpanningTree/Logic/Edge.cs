﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*
* 命名空间:  MinimumSpanningTree.Logic
*
* 功 能：构造边的结构 Edge
*
* 类 名：Edge
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/16 13:45:29       LW      创建
*
* Copyright (c) 2020 zhdl Corporation. All rights reserved.
*/
namespace MinimumSpanningTree.Logic
{
    internal class Edge
    {
        /// <summary>
        /// 起点编号
        /// </summary>
        public int Begin { get; }

        /// <summary>
        /// 终点编号
        /// </summary>
        public int End { get; }

        /// <summary>
        /// 权值
        /// </summary>
        public double Weight { get; }

        /// <summary>
        /// 创建一个 Edge 类的新实例
        /// </summary>
        /// <param name="begin">起点编号</param>
        /// <param name="end">终点编号</param>
        /// <param name="weight">权值</param>

        public Edge(int begin, int end, double weight = 0.0000)
        {
            Begin = begin;
            End = end;
            Weight = weight;
        }
    }
}
