﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*
* 命名空间:  MinimumSpanningTree.Logic
*
* 功 能：构造边表结点的结构 EdgeNode。
*
* 类 名：EdgeNode
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/16 13:42:38       LW      创建
*
* Copyright (c) 2020 zhdl Corporation. All rights reserved.
*/
namespace MinimumSpanningTree.Logic
{
    /// <summary>
    /// 构造边表结点的结构 EdgeNode。
    /// </summary>
    public class EdgeNode
    {
        /// <summary>
        /// 获取边终点在顶点数组中的位置
        /// </summary>
        public int Index { get; }

        /// <summary>
        /// 获取边上的权值
        /// </summary>
        public double Weight { get; }

        /// <summary>
        /// 获取或设置下一个邻接点
        /// </summary>
        public EdgeNode Next { get; set; }

        /// <summary>
        /// 初始化EdgeNode类的新实例
        /// </summary>
        /// <param name="index">边终点在顶点数组中的位置</param>
        /// <param name="weight">边上的权值</param>
        /// <param name="next">下一个邻接点</param>
        public EdgeNode(int index, double weight = 0.0000, EdgeNode next = null)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException();

            Index = index;
            Weight = weight;
            Next = next;
        }
    }
}
