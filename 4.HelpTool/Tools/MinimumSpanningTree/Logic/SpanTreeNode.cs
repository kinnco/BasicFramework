﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*
* 命名空间:  MinimumSpanningTree.Logic
*
* 功 能：构造最小生成树结点的结构 SpanTreeNode。
*
* 类 名：SpanTreeNode
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/16 13:45:01       LW      创建
*
* Copyright (c) 2020 zhdl Corporation. All rights reserved.
*/
namespace MinimumSpanningTree.Logic
{
    /// <summary>
    /// 构造最小生成树结点的结构 SpanTreeNode。
    /// </summary>
    public class SpanTreeNode
    {
        /// <summary>
        /// 获取或设置结点本身的名称
        /// </summary>
        public string SelfName { get; }

        /// <summary>
        /// 获取或设置结点双亲的名称
        /// </summary>
        public string ParentName { get; }

        /// <summary>
        /// 获取或设置边的权值
        /// </summary>
        public double Weight { get; set; }

        /// <summary>
        /// 构造SpanTreeNode实例
        /// </summary>
        /// <param name="selfName">结点本身的名称</param>
        /// <param name="parentName">结点双亲的名称</param>
        /// <param name="weight">边的权值</param>
        public SpanTreeNode(string selfName, string parentName, double weight)
        {
            if (string.IsNullOrEmpty(selfName) || string.IsNullOrEmpty(parentName))
                throw new ArgumentNullException();

            SelfName = selfName;
            ParentName = parentName;
            Weight = weight;
        }
    }
}
