﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
* 命名空间:  TableToEntity.Entity
*
* 功 能：
*
* 类 名：DtoInfo  
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/8/4 14:05:34  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace TableToEntity
{

    /// <summary>
    /// Dto文件实体
    /// </summary>
	public class DtoInfo
    {

        /// <summary>
        /// 表类型
        /// </summary>
        public string TableType { get; set; }

        /// <summary>
        /// 命名空间
        /// </summary>
        public string NameSpace { get; set; }

        /// <summary>
        /// Profile名称
        /// </summary>
        public string Profile { get; set; }

        /// <summary>
        /// LogicName名称
        /// </summary>
        public string LogicName { get; set; }

        /// <summary>
        /// 逻辑文件夹路径
        /// </summary>
        public string LogicFolder { get; set; }
        
        /// <summary>
        /// 数据库对应实体名称
        /// </summary>
        public string TableModel { get; set; }

        /// <summary>
        /// 条件查询实体名称
        /// </summary>
        public string QueryModel { get; set; }

        /// <summary>
        /// 编辑实体名称
        /// </summary>
        public string ModifyModel { get; set; }

        /// <summary>
        /// 返回传输实体名称
        /// </summary>
        public string ResponseModel { get; set; }

        /// <summary>
        /// Dto文件夹路径
        /// </summary>
        public string DtoFolder { get; set; }
  
    }
}
