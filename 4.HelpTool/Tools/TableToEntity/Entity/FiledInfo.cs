﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableToEntity
{
    public class FiledInfo
    {
        /// <summary>
        /// 列名
        /// </summary>
        public string isPrimarykey { get; set; }

        /// <summary>
        /// 列名
        /// </summary>
        public string colName { get; set; }
        /// <summary>
        /// 表说明
        /// </summary>
        public string tabDescribe { get; set; }
        /// <summary>
        /// 数据类型
        /// </summary>
        public string dbType { get; set; }
        /// <summary>
        /// 列描述
        /// </summary>
        public string colDescribe { get; set; }
        /// <summary>
        /// 是否必须
        /// </summary>
        public string required { get; set; }
        /// <summary>
        /// 默认值
        /// </summary>
        public string devalue { get; set; }
    }
}
