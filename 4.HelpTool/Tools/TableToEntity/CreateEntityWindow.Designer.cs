﻿namespace TableToEntity
{
    partial class CreateEntityWindow
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.txtViewAuthorName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtView = new System.Windows.Forms.TextBox();
            this.txtModelAuthorName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label_namespace = new System.Windows.Forms.Label();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.labDBType = new System.Windows.Forms.Label();
            this.comDBType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDBAddress = new System.Windows.Forms.TextBox();
            this.txtDBName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAccount = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnCreateModel = new System.Windows.Forms.Button();
            this.btnCreateView = new System.Windows.Forms.Button();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.txtSchema = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtViewAuthorName
            // 
            this.txtViewAuthorName.Location = new System.Drawing.Point(375, 483);
            this.txtViewAuthorName.Name = "txtViewAuthorName";
            this.txtViewAuthorName.Size = new System.Drawing.Size(197, 21);
            this.txtViewAuthorName.TabIndex = 23;
            this.txtViewAuthorName.Text = "Harvey";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(307, 486);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 22;
            this.label2.Text = "作者名字：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 486);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 12);
            this.label3.TabIndex = 21;
            this.label3.Text = "视图命名空间：";
            // 
            // txtView
            // 
            this.txtView.Location = new System.Drawing.Point(103, 483);
            this.txtView.Name = "txtView";
            this.txtView.Size = new System.Drawing.Size(181, 21);
            this.txtView.TabIndex = 20;
            this.txtView.Text = "Authority.Model";
            // 
            // txtModelAuthorName
            // 
            this.txtModelAuthorName.Location = new System.Drawing.Point(376, 456);
            this.txtModelAuthorName.Name = "txtModelAuthorName";
            this.txtModelAuthorName.Size = new System.Drawing.Size(196, 21);
            this.txtModelAuthorName.TabIndex = 18;
            this.txtModelAuthorName.Text = "Harvey";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(307, 459);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 17;
            this.label1.Text = "作者名字：";
            // 
            // label_namespace
            // 
            this.label_namespace.AutoSize = true;
            this.label_namespace.Location = new System.Drawing.Point(12, 459);
            this.label_namespace.Name = "label_namespace";
            this.label_namespace.Size = new System.Drawing.Size(89, 12);
            this.label_namespace.TabIndex = 16;
            this.label_namespace.Text = "实体命名空间：";
            // 
            // txtModel
            // 
            this.txtModel.Location = new System.Drawing.Point(101, 456);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(183, 21);
            this.txtModel.TabIndex = 15;
            this.txtModel.Text = "Authority.Model";
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(16, 9);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(559, 320);
            this.txtResult.TabIndex = 13;
            // 
            // labDBType
            // 
            this.labDBType.AutoSize = true;
            this.labDBType.Location = new System.Drawing.Point(20, 350);
            this.labDBType.Name = "labDBType";
            this.labDBType.Size = new System.Drawing.Size(77, 12);
            this.labDBType.TabIndex = 24;
            this.labDBType.Text = "数据库类型：";
            // 
            // comDBType
            // 
            this.comDBType.DisplayMember = "0";
            this.comDBType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comDBType.FormattingEnabled = true;
            this.comDBType.Items.AddRange(new object[] {
            "MSSQL",
            "Oracle",
            "PostgreSQL",
            "MySQL"});
            this.comDBType.Location = new System.Drawing.Point(100, 347);
            this.comDBType.Name = "comDBType";
            this.comDBType.Size = new System.Drawing.Size(472, 20);
            this.comDBType.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 395);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 26;
            this.label4.Text = "链接地址：";
            // 
            // txtDBAddress
            // 
            this.txtDBAddress.Location = new System.Drawing.Point(100, 391);
            this.txtDBAddress.Name = "txtDBAddress";
            this.txtDBAddress.Size = new System.Drawing.Size(119, 21);
            this.txtDBAddress.TabIndex = 27;
            this.txtDBAddress.Text = "192.168.1.101";
            // 
            // txtDBName
            // 
            this.txtDBName.Location = new System.Drawing.Point(375, 391);
            this.txtDBName.Name = "txtDBName";
            this.txtDBName.Size = new System.Drawing.Size(100, 21);
            this.txtDBName.TabIndex = 29;
            this.txtDBName.Text = "sczhwater";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(308, 395);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 28;
            this.label5.Text = "数据库名：";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(375, 424);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(197, 21);
            this.txtPassword.TabIndex = 33;
            this.txtPassword.Text = "zhdl86158818";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(308, 428);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 32;
            this.label6.Text = "密    码：";
            // 
            // txtAccount
            // 
            this.txtAccount.Location = new System.Drawing.Point(100, 424);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(184, 21);
            this.txtAccount.TabIndex = 31;
            this.txtAccount.Text = "postgres";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 428);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 30;
            this.label7.Text = "账    号：";
            // 
            // btnCreateModel
            // 
            this.btnCreateModel.Location = new System.Drawing.Point(376, 518);
            this.btnCreateModel.Name = "btnCreateModel";
            this.btnCreateModel.Size = new System.Drawing.Size(90, 23);
            this.btnCreateModel.TabIndex = 34;
            this.btnCreateModel.Text = "生成数据实体";
            this.btnCreateModel.UseVisualStyleBackColor = true;
            this.btnCreateModel.Click += new System.EventHandler(this.btnCreateModel_Click);
            // 
            // btnCreateView
            // 
            this.btnCreateView.Location = new System.Drawing.Point(481, 518);
            this.btnCreateView.Name = "btnCreateView";
            this.btnCreateView.Size = new System.Drawing.Size(91, 23);
            this.btnCreateView.TabIndex = 35;
            this.btnCreateView.Text = "生成视图实体";
            this.btnCreateView.UseVisualStyleBackColor = true;
            this.btnCreateView.Click += new System.EventHandler(this.btnCreateView_Click);
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(225, 391);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(59, 21);
            this.txtPort.TabIndex = 36;
            this.txtPort.Text = "5432";
            // 
            // txtSchema
            // 
            this.txtSchema.Location = new System.Drawing.Point(481, 392);
            this.txtSchema.Name = "txtSchema";
            this.txtSchema.Size = new System.Drawing.Size(91, 21);
            this.txtSchema.TabIndex = 37;
            this.txtSchema.Text = "waterdb";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 550);
            this.Controls.Add(this.txtSchema);
            this.Controls.Add(this.txtPort);
            this.Controls.Add(this.btnCreateView);
            this.Controls.Add(this.btnCreateModel);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtAccount);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtDBName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDBAddress);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comDBType);
            this.Controls.Add(this.labDBType);
            this.Controls.Add(this.txtViewAuthorName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtView);
            this.Controls.Add(this.txtModelAuthorName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_namespace);
            this.Controls.Add(this.txtModel);
            this.Controls.Add(this.txtResult);
            this.Name = "MainWindow";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtViewAuthorName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtView;
        private System.Windows.Forms.TextBox txtModelAuthorName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_namespace;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Label labDBType;
        private System.Windows.Forms.ComboBox comDBType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDBAddress;
        private System.Windows.Forms.TextBox txtDBName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAccount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnCreateModel;
        private System.Windows.Forms.Button btnCreateView;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.TextBox txtSchema;
    }
}

