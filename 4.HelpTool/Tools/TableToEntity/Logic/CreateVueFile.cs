﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
* 命名空间:  TableToEntity.Logic
*
* 功 能：
*
* 类 名：CreateVueFile  
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/8/4 17:24:32  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace TableToEntity
{
    /// <summary>
    /// 创建vue界面
    /// </summary>
	public class CreateVueFile
    {
        /// <summary>
        /// 生成IndexVue文件
        /// </summary>
        /// <param name="profileName"></param>
        public static void CreateListIndexVue(DtoInfo dtoInfo)
        {
            string fileName = Path.Combine(dtoInfo.DtoFolder,"index.vue");
            StringBuilder sb = new StringBuilder();
            using (StreamWriter sw = File.CreateText(fileName))
            {
                sb.AppendLine("< !--");
                sb.AppendLine("@Author: LW");
                sb.AppendLine(string.Format("@Date: {0}",DateTime.Now));
                sb.AppendLine("@function:");
                sb.AppendLine("----------------------------------------------------------");
                sb.AppendLine("@Last Modifiedby:");
                sb.AppendLine("@Modified function:");
                sb.AppendLine("-->");
                sb.AppendLine(" <template>");
                sb.AppendLine("   <div>");
                sb.AppendLine("    <el-card class=\"box-card card1\">");
                sb.AppendLine("      <div class=\"searchBox\">");
                sb.AppendLine("        <el-form :inline=\"true\" :model=\"formSearch\">");
                sb.AppendLine("          <el-form-item  label=\"搜索：\">");
                sb.AppendLine("            <el-input size=\"mini\"  v-model=\"formSearch.parameters.keywords\" placeholder=\"输入关键字\"></el-input>");
                sb.AppendLine("          </el-form-item>");
                sb.AppendLine("          <el-form-item>");
                sb.AppendLine("            <el-button size=\"mini\" type=\"primary\" icon=\"el-icon-search\" @click=\"handleSearch()\">搜索</el-button>");
                sb.AppendLine("            <el-button size=\"mini\" type=\"primary\" icon=\"el-icon-plus\" @click=\"handleEdit()\">新增</el-button>");
                sb.AppendLine("            <el-button size=\"mini\" type=\"danger\" icon=\"el-icon-remove\" @click=\"handleRemove()\">删除</el-button>");
                sb.AppendLine("          </el-form-item>");
                sb.AppendLine("        </el-form>");
                sb.AppendLine("      </div>");
                sb.AppendLine("    </el-card>");
                sb.AppendLine("    <el-row :gutter=\"10\">");
                sb.AppendLine("      <el-col :span=\"24\">");
                sb.AppendLine("        <el-card class=\"box-card\">");
                sb.AppendLine("          <el-table");
                sb.AppendLine("            class=\"ptab\"");
                sb.AppendLine("            border");
                sb.AppendLine("            stripe");
                sb.AppendLine("            size=\"small\"");
                sb.AppendLine("            ref=\"multipleTable\"");
                sb.AppendLine("            highlight-current-row");
                sb.AppendLine("            v-loading=\"loading\"");
                sb.AppendLine("            element-loading-text=\"拼命加载中\"");
                sb.AppendLine("            style=\"width:100%;\"");
                sb.AppendLine("            :data=\"listData\"");
                sb.AppendLine("            :height=\"tableHeight\"");
                sb.AppendLine("            @selection-change=\"checkSelect\"");
                sb.AppendLine("            @row-click=\"handleRowClick\"");
                sb.AppendLine("          >");
                sb.AppendLine("            <el-table-column type=\"selection\" align=\"center\" width=\"55px\" ></el-table-column>");
                sb.AppendLine("            <el-table-column align=\"center\" label=\"序号\" width=\"55px\" fixed=\"left\">");
                sb.AppendLine("               <template slot-scope=\"scope\">{{ scope.$index + 1 }}</template>");
                sb.AppendLine("            </el-table-column>");
                sb.AppendLine("            <el-table-column align=\"center\" prop=\"xxxxx\" label=\"xxxxx\" width=\"150\" :show-overflow-tooltip=\"true\"></el-table-column>");
                sb.AppendLine("            <el-table-column align=\"center\" prop=\"is_hot_style_name\" label=\"是否热卖\" width=\"90\">");
                sb.AppendLine("              <template slot-scope=\"scope\">");
                sb.AppendLine("                <el-switch");
                sb.AppendLine("                  on-text=\"是\"");
                sb.AppendLine("                  off-text=\"不是\"");
                sb.AppendLine("                  active-value=\"是\"");
                sb.AppendLine("                  inactive-value=\"否\"");
                sb.AppendLine("                  on-color=\"#5B7BFA\"");
                sb.AppendLine("                  off-color=\"#DADDE5\"");
                sb.AppendLine("                  v-model=\"scope.row.is_hot_style_name\"");
                sb.AppendLine("                  @change=\"enableInfo(scope.row, 1)\"");
                sb.AppendLine("                ></el-switch>");
                sb.AppendLine("              </template>");
                sb.AppendLine("            </el-table-column>");
                sb.AppendLine("            <el-table-column align=\"left\" label=\"操作\">");
                sb.AppendLine("              <template slot-scope=\"scope\">");
                sb.AppendLine("                <el-button");
                sb.AppendLine("                  type=\"text\"");
                sb.AppendLine("                  icon=\"el-icon-edit\"");
                sb.AppendLine("                  size=\"mini\"");
                sb.AppendLine("                  class=\"btnBlue\"");
                sb.AppendLine("                  @click=\"handleEdit(scope.$index, scope.row)\"");
                sb.AppendLine("                >修改</el-button>");
                sb.AppendLine("                <el-button");
                sb.AppendLine("                  type=\"text\"");
                sb.AppendLine("                  icon=\"el-icon-remove\"");
                sb.AppendLine("                  size=\"mini\"");
                sb.AppendLine("                  class=\"btnRed\"");
                sb.AppendLine("                  @click=\"handleRemove(scope.$index, scope.row)\"");
                sb.AppendLine("                >删除</el-button>");
                sb.AppendLine("              </template>");
                sb.AppendLine("            </el-table-column>");
                sb.AppendLine("          </el-table>");
                sb.AppendLine("          <Pagination v-bind:child-msg=\"pageparm\" @callFather=\"pageListInfo\"></Pagination>");
                sb.AppendLine("        </el-card>");
                sb.AppendLine("      </el-col>");
                sb.AppendLine("    </el-row>");
                sb.AppendLine("    <!-- 编辑等操作弹窗 -->");
                sb.AppendLine("    <!-- dialog 弹窗顶部下划线 v-dialogDrag可拖拽-->");
                sb.AppendLine("    <el-dialog");
                sb.AppendLine("      :top=\"editForm.top\"");
                sb.AppendLine("      :title=\"editForm.formTitle\"");
                sb.AppendLine("      :visible.sync=\"editForm.formVisible\"");
                sb.AppendLine("      v-if=\"editForm.formVisible\"");
                sb.AppendLine("      :width=\"editForm.width\"");
                sb.AppendLine("      v-dialogDrag");
                sb.AppendLine("      custom-class=\"dialog\"");
                sb.AppendLine("      :close-on-click-modal=\"false\"");
                sb.AppendLine("    >");
                sb.AppendLine("      <component v-bind:is=\"curComponent\" v-bind:formInfo=\"formInfo\"  @callBack=\"callBack\"></component>");
                sb.AppendLine("    </el-dialog>");
                sb.AppendLine("  </div>");
                sb.AppendLine("</template>");
                sb.AppendLine("<script>");
                sb.AppendLine("import Pagination from '@/components/common/pagination'//分页组建");
                sb.AppendLine("import XXXXHttp from '@/api/XX/XXXXX' //http");
                sb.AppendLine("const { ");
                sb.AppendLine("  loadPageList,");
                sb.AppendLine(" } = XXXXHttp;");
                sb.AppendLine("export default {");
                sb.AppendLine("  //注册组件");
                sb.AppendLine("  components: {");
                sb.AppendLine("    Pagination, //分页");
                sb.AppendLine("  },");
                sb.AppendLine("  data() {");
                sb.AppendLine("    return {");
                sb.AppendLine("      //列表相关");
                sb.AppendLine("      loading: false, //是显示加载");
                sb.AppendLine("      tableHeight: window.innerHeight * 0.795, //表格高度");
                sb.AppendLine("      pageparm:{ currentPage: 1, pageSize: 16, total: 0 }, //分页参数");
                sb.AppendLine("      listData:[], //表格返回信息");
                sb.AppendLine("      multipleSelection: null, //复选框");
                sb.AppendLine("      formSearch: {");
                sb.AppendLine("        //请求数据参数");
                sb.AppendLine("        page: 1,");
                sb.AppendLine("        limit: 16,");
                sb.AppendLine("        field: 'sort',");
                sb.AppendLine("        order: 'asc',");
                sb.AppendLine("        parameters:\"\"");
                sb.AppendLine("      },");
                sb.AppendLine("      curComponent: null,");
                sb.AppendLine("      disabled: false,");
                sb.AppendLine("      //编辑框窗体参数");
                sb.AppendLine("      editForm: {");
                sb.AppendLine("        formVisible: false, //窗体是否显示");
                sb.AppendLine("        formTitle: '', //窗体标题");
                sb.AppendLine("        width:\"55%\"");
                sb.AppendLine("      },");
                sb.AppendLine("      formInfo: {},");
                sb.AppendLine("    }");
                sb.AppendLine("  },");
                sb.AppendLine("  created() {");
                sb.AppendLine("  },");
                sb.AppendLine("  mounted() {");
                sb.AppendLine("    this.loadList();");
                sb.AppendLine("  },");
                sb.AppendLine("  methods: {");
                sb.AppendLine("    //点击复选框触发，复选框样式的改变");
                sb.AppendLine("    checkSelect(val) {");
                sb.AppendLine("      this.multipleSelection = val");
                sb.AppendLine("    },");
                sb.AppendLine("    //点击行触发，选中或不选中复选框");
                sb.AppendLine("    handleRowClick(row, column, event) {");
                sb.AppendLine("      this.$refs.multipleTable.toggleRowSelection(row);");
                sb.AppendLine("    },");
                sb.AppendLine("    //获取数据方法");
                sb.AppendLine("    loadList() {");
                sb.AppendLine("      this.loading = true");
                sb.AppendLine("      loadPageList(this.formSearch).then((response) => {");
                sb.AppendLine("        let data = response.data");
                sb.AppendLine("        if (data.Code === 1000) {");
                sb.AppendLine("          this.listData = data.Data");
                sb.AppendLine("          //分页赋值");
                sb.AppendLine("          this.pageparm.currentPage = this.formSearch.page");
                sb.AppendLine("          this.pageparm.pageSize = this.formSearch.limit");
                sb.AppendLine("          this.pageparm.total = data.Count");
                sb.AppendLine("        } else {");
                sb.AppendLine("          this.$messageTip.warning(data.Msg)");
                sb.AppendLine("          this.listData = []");
                sb.AppendLine("          this.pageparm.currentPage = this.formSearch.page");
                sb.AppendLine("          this.pageparm.pageSize = this.formSearch.limit");
                sb.AppendLine("          this.pageparm.total = data.Count");
                sb.AppendLine("        }");
                sb.AppendLine("        this.loading = false");
                sb.AppendLine("      })");
                sb.AppendLine("    },");
                sb.AppendLine("    //分页插件事件");
                sb.AppendLine("    pageListInfo(par) {");
                sb.AppendLine("      this.formSearch.page = par.currentPage");
                sb.AppendLine("      this.formSearch.limit = par.pageSize");
                sb.AppendLine("      this.loadList();");
                sb.AppendLine("    },");
                sb.AppendLine("     //搜索事件");
                sb.AppendLine("    handleSearch() {");
                sb.AppendLine("      this.formSearch.page = 1;");
                sb.AppendLine("      this.formSearch.limit = 16;");
                sb.AppendLine("      this.loadList();");
                sb.AppendLine("    },");
                sb.AppendLine("    //编辑界面");
                sb.AppendLine("    handleEdit(index, row) {");
                sb.AppendLine("      this.editForm.formVisible = true;");
                sb.AppendLine("      if (row != null && row != undefined) {");
                sb.AppendLine("        this.editForm.formTitle = \"修改信息\";");
                sb.AppendLine("        this.editForm.width = \"30%\";");
                sb.AppendLine("        //不要直接赋值，不然造成-引用类型传递");
                sb.AppendLine("        let obj = JSON.parse(JSON.stringify(row)); //row是父组件传递的对象");
                sb.AppendLine("        this.formInfo = obj;");
                sb.AppendLine("        this.curComponent = EditForm;");
                sb.AppendLine("      } else {");
                sb.AppendLine("        this.editForm.formTitle = \"新增信息\";");
                sb.AppendLine("        this.editForm.width = \"60 %\";");
                sb.AppendLine("        this.formInfo = {};");
                sb.AppendLine("        this.curComponent = AddForm;");
                sb.AppendLine("      }");
                sb.AppendLine("    },");
                sb.AppendLine("    //删除");
                sb.AppendLine("    handleRemove(index, row) {");
                sb.AppendLine("      let self = this;");
                sb.AppendLine("      if (row != null||(this.$refs.multipleTable && this.$refs.multipleTable.store.states.selection.length > 0)) {");
                sb.AppendLine("        this.$messageTip.boxWarning(\"确定要删除吗?\")");
                sb.AppendLine("          .then(() => {");
                sb.AppendLine("            let par = [];");
                sb.AppendLine("            if (row != null) {");
                sb.AppendLine("              par.push(row.id);");
                sb.AppendLine("            } else {");
                sb.AppendLine("              let selectData = self.$refs.multipleTable.store.states.selection;");
                sb.AppendLine("              for (let index = 0; index < selectData.length; index++) {");
                sb.AppendLine("                par.push(selectData[index].id);");
                sb.AppendLine("              }");
                sb.AppendLine("            }");
                sb.AppendLine("            removeInfo(par).then(response => {");
                sb.AppendLine("              let data = response.data;");
                sb.AppendLine("              if (data.Code === 1000) {");
                sb.AppendLine("                self.$messageTip.success(data.Msg); ");
                sb.AppendLine("                self.loadList();");
                sb.AppendLine("              } else {");
                sb.AppendLine("                self.$messageTip.warning(data.Msg); ");
                sb.AppendLine("              }");
                sb.AppendLine("            });");
                sb.AppendLine("          })");
                sb.AppendLine("          .catch(() => {");
                sb.AppendLine("            this.$messageTip.info(\"已取消删除\");");
                sb.AppendLine("          });");
                sb.AppendLine("      } else {");
                sb.AppendLine("         this.$messageTip.info(\"请选择删除项\");");
                sb.AppendLine("      }");
                sb.AppendLine("    },");
                sb.AppendLine("  },");
                sb.AppendLine("}");
                sb.AppendLine("</script>");
                sb.AppendLine("<style>");
                sb.AppendLine("</style>");
                sw.Write(sb.ToString());
                sb.Clear();
            }
        }
    }
}
