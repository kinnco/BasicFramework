/* 
 * @Author: Havery 
 * @Date: 2020-11-12 14:00:31  
 * @function: 公用方法
 * ---------------------------------------------------------- 
 */
import store from '../store'
const GlobalObj = {
  /**
   * 获取界面唯一标识符
   */
  getUniqueIdentifier: function () {
    var data = window.top.localStorage.getItem('UniqueIdentifier');
    if (data != null && data != undefined) {
      return data;
    } else {
      var s = [];
      var hexDigits = "0123456789abcdef";
      for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
      }
      s[14] = "4";
      s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
      s[8] = s[13] = s[18] = s[23] = "-";
      data = s.join("");
      window.top.localStorage.setItem('UniqueIdentifier', data);
    }
    return data;
  },
  /**
   * 获取sessionStorage数据
   * @param {*} key 
   */
  getSession: function (key) {
    return sessionStorage.getItem(payload);
  },
  /**
   * 保存sessionStorage数据
   * @param {*} key 
   * @param {*} payload 
   */
  saveSession(key, info) {
    sessionStorage.setItem(key, info);
  },
  /**
   * 存储可操作图层信息
   */
  getLayerInfo() {
    var layerInfo = store.state.layer.layerInfo;
    if (layerInfo != undefined && layerInfo != null && layerInfo != "") {
      return layerInfo;
    }
    return null;
  },
  /**
   * 获取默认图层
   */
  getDefaultLayerInfo: function () {
    var layerInfo = store.state.layer.layerInfo;
    if (layerInfo != undefined && layerInfo != null && layerInfo != "") {
      var serverInfos = [];
      for (var i = 0; i < layerInfo.length; i++) {
        if (layerInfo[i].state.checked == true) {
          serverInfos.push(layerInfo[i]);
        }
      }
      return serverInfos;
    }
    return null;
  },
  /**
   * 获取featureclass_name指定图层
   * @param {any} par
   */
  getLayerInfoByAppoint: function (par) {
    var serve = "";
    var LayerInfo = store.state.layer.layerInfo;
    if (LayerInfo != undefined && LayerInfo != null && LayerInfo != "") {
      for (var i = 0; i < LayerInfo.length; i++) {
        if (LayerInfo[i].featureclass_name == par) {
          serve = LayerInfo[i];
          return serve;
        } else if (LayerInfo[i].nodes.length > 0) {
          for (var j = 0; j < LayerInfo[i].nodes.length; j++) {
            if (LayerInfo[i].nodes[j].featureclass_name == par) {
              serve = LayerInfo[i].nodes[j];
              return serve;
            }
          }
        }
      }
    }
    return serve;
  }
}

export default GlobalObj;