/* 
 * @Author: LW  
 * @Date: 2021-01-11 14:22:37  
 * @function:菜单管理相关接口
 * ---------------------------------------------------------- 
 */ 
import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {
  //获取应用列表信息
  loadApplicationList(par) {
    return httpRequest({
      url: "/api/Safe/ApplicationInfo/LoadSelectList",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //获取功能类型列表信息
  loadFunctionTypeList(par) {
    return httpRequest({
        url: "/api/Authority/FunctionCfg/LoadFunctionTypeList",
        method: 'GET',
        modal: false,
        data: par
    });
  },
  //获取菜单列表
  loadMenusList(par) {
    return httpRequest({
        url: "/api/Authority/FunctionCfg/LoadTreeListInfo",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //移动操作 flag==1上移   flag==2下移
   moveSort(par) {
    return httpRequest({
        url: "/api/Authority/FunctionCfg/MoveSort",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //删除信息
   removeInfo(par) {
    return httpRequest({
        url: "/api/Authority/FunctionCfg/Remove",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //启用或停用
   forbiddenInfo(par) {
    return httpRequest({
        url: "/api/Authority/FunctionCfg/ForbidOrEnable/{id}",
        method: 'GET',
        modal: false,
        data: par
    });
   },
   ///////////////////////编辑
   //增加数据
   addInfo(par) {
    return httpRequest({
        url: "/api/Authority/FunctionCfg/AddInfo",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //编辑数据
   modifyInfo(par) {
    return httpRequest({
        url: "/api/Authority/FunctionCfg/ModifyInfo",
        method: 'POST',
        modal: false,
        data: par
    });
   },
}
