/* 
 * @Author: LW  
 * @Date: 2021-01-11 14:22:37  
 * @function:部门管理相关接口
 * ---------------------------------------------------------- 
 */ 
import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {
  //获取部门列表
  loadDepartmentList(par) {
    return httpRequest({
        url: "/api/Authority/Department/LoadTreeList",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //移动操作 flag==1上移   flag==2下移
   moveSort(par) {
    return httpRequest({
        url: "/api/Authority/Department/Move",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //删除信息
   removeInfo(par) {
    return httpRequest({
        url: "/api/Authority/Department/Remove",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //启用或停用
   forbiddenInfo(par) {
    return httpRequest({
        url: "/api/Authority/Department/ForbidOrEnable/{id}",
        method: 'GET',
        modal: false,
        data: par
    });
   },
   ///////////////////////编辑
   //增加数据
   addInfo(par) {
    return httpRequest({
        url: "/api/Authority/Department/Addnode",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //编辑数据
   modifyInfo(par) {
    return httpRequest({
        url: "/api/Authority/Department/ModifyInfo",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   ///////////////////////////用户管理
   ///获取用户列表
   loadUserListInfo(par) {
    return httpRequest({
        url: "/api/Authority/Department/LoadUserListByDeparId",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //保存包含的用户数据
   saveUserInfo(par) {
    return httpRequest({
        url: "/api/Authority/Department/ModifyDeparUserInfo",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   ///////////////////////////领导管理
   ///获取领导列表
   loadLeaderListInfo(par) {
    return httpRequest({
        url: "/api/Authority/Department/LoadLeaderListByDeparId",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //保存包含的领导数据
   saveLeaderInfo(par) {
    return httpRequest({
        url: "/api/Authority/Department/ModifyDeparLeaderInfo",
        method: 'POST',
        modal: false,
        data: par
    });
   },
}
