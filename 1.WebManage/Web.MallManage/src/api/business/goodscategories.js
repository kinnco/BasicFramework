/* 
 * @Author: LW  
 * @Date: 2021-01-11 14:22:37  
 * @function:商品管理相关接口
 * ---------------------------------------------------------- 
 */
import http from '@/utils/http';
import store from '@/store';

const {
  httpRequest
} = http;

export default {
  //获取分类树状列表信息
  loadCategoriesTreeList(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Goods/Category/LoadAllTreeList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //获取分类列表
  loadCategoriesList(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Goods/Category/LoadTreeList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //移动操作 flag==1上移   flag==2下移
  moveSort(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Goods/Category/Move",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //删除信息
  removeInfo(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Goods/Category/Remove",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  ///////////////////////编辑
  //增加数据
  addInfo(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Goods/Category/Addnode",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //编辑数据
  modifyInfo(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Goods/Category/ModifyInfo",
      method: 'POST',
      modal: false,
      data: par
    });
  },
}
