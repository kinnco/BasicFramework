/* 
 * @Author: LW  
 * @Date: 2021-01-11 14:22:37  
 * @function:商品管理相关接口
 * ---------------------------------------------------------- 
 */ 
import http from '@/utils/http';
import store from '@/store';

const {
  httpRequest
} = http;

export default {

   //获取商品分页列表
   loadPageList(par) {
    return httpRequest({
        baseURL: store.getters.mallURI,
        url: "/api/Goods/GoodsInfo/LoadPageList",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //根据id获取对应的信息
   loadSingleById(par) {
    return httpRequest({
        baseURL: store.getters.mallURI,
        url: "/api/Goods/GoodsInfo/LoadSingleById/{id}",
        method: 'GET',
        modal: false,
        data: par
    });
   },
   //根据id获取对应的详情信息【基本信息+附件】
   loadDetailById(par) {
    return httpRequest({
        baseURL: store.getters.mallURI,
        url: "/api/Goods/GoodsInfo/LoadDetailById/{id}",
        method: 'GET',
        modal: false,
        data: par
    });
   },
   //根据商品id获取附件文件
   loadAttachList(par) {
    return httpRequest({
        baseURL: store.getters.mallURI,
        url: "/api/Goods/GoodsInfo/LoadAttachList/{id}",
        method: 'GET',
        modal: false,
        data: par
    });
   },
   ///////////////////////////////////////////////////////////
   //是否开启 热卖  推荐  特价  上架
   enableInfo(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Goods/GoodsInfo/EnableInfo/{id}/{type}",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //保存商品信息
  saveInfo(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Goods/GoodsInfo/SaveInfo",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //删除信息
  removeInfo(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Goods/GoodsInfo/Remove",
      method: 'POST',
      modal: false,
      data: par
    });
  },
}
