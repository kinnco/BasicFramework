import http from '@/utils/http';
const {
    httpRequest
} = http;

export default {
    /////流程相关类型select/////////////////////////
    //获取流程处理者类型
    loadFlowHandlerTypeList(par) {
        return httpRequest({
            url: "/api/FlowInfo/LoadFlowHandlerTypeList",
            method: 'GET',
            modal: false,
            data: par
        });
    },
    //获取流程退回类型
    loadFlowReturnTypeList(par) {
        return httpRequest({
            url: "/api/FlowInfo/LoadFlowReturnTypeList",
            method: 'GET',
            modal: false,
            data: par
        });
    },
    //获取会签策略
    loadFlowSignStrategyList(par) {
        return httpRequest({
            url: "/api/FlowInfo/LoadFlowSignStrategyList",
            method: 'GET',
            modal: false,
            data: par
        });
    },
    //获取流程处理策略
    loadFlowHandleStrategyList(par) {
        return httpRequest({
            url: "/api/FlowInfo/LoadFlowHandleStrategyList",
            method: 'GET',
            modal: false,
            data: par
        });
    },

    //获取流程按钮列表
    loadFlowButtonList(par) {
        return httpRequest({
            url: "/api/FlowInfo/LoadFlowOwnButtonList/{stepId}",
            method: 'GET',
            modal: false,
            data: par
        });
    },
    /////////////////////////////////////////////
    //获取流程列表
    loadPageList(par) {
        return httpRequest({
            url: "/api/FlowInfo/LoadPageList",
            method: 'POST',
            modal: false,
            data: par
        });
    },
    //获取单个流程信息
    loadSingleById(par) {
        return httpRequest({
            url: "/api/FlowInfo/LoadSingleById/{id}",
            method: 'GET',
            modal: false,
            data: par
        });
    },
    //编辑类型
    save(par) {
        return httpRequest({
            url: "/api/FlowInfo/Save",
            method: 'POST',
            modal: false,
            data: par
        });
    },
    //安装卸载、
    installUninstall(par) {
        return httpRequest({
            url: "/api/FlowInfo/InstallUninstall/{id}",
            method: 'GET',
            modal: false,
            data: par
        });
    },
    //删除类型
    remove(par) {
        return httpRequest({
            url: "/api/FlowInfo/Remove",
            method: 'POST',
            modal: false,
            data: par
        });
    },
    //获取所有的用户树状结构
    loadDepartmentUserTreeList() {
        return httpRequest({
            url: "/api/FlowInfo/LoadDepartmentUserTreeList",
            method: 'GET',
            modal: false,
        });
    },

    //获取所有类型
    loadTypeList() {
        return httpRequest({
            url: "/api/FlowCategory/LoadList",
            method: 'GET',
            modal: false
        });
    },
    ////////////////////////////////////////////////////////流程节点管理
    //编辑流程详情信息【包括流程图】
    saveFlowMapl(par) {
        return httpRequest({
            url: "/api/FlowInfo/SaveFlowMapl",
            method: 'POST',
            modal: false,
            data: par
        });
    },
    //安装流程【包括流程图】
    installFlowMapl(par) {
        return httpRequest({
            url: "/api/FlowInfo/InstallFlowMapl",
            method: 'POST',
            modal: false,
            data: par
        });
    },
}
