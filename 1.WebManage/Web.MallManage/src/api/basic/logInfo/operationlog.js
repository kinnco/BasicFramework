import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {

  //获取所有的操作日志
  loadList(par) {
    return httpRequest({
      url: "/api/Basic/OperationLog/LoadList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
 //获取操作日志类型
 loadLogTypeList() {
  return httpRequest({
    url: "/api/Basic/OperationLog/LoadLogTypeList",
    method: 'GET',
    modal: false,
  });
},
//获取操作日志类型
removeInfo(par) {
  return httpRequest({
    url: "/api/Basic/OperationLog/Remove",
    method: 'POST',
    modal: false,
    data: par
  });
},
}
