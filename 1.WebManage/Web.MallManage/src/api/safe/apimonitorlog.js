import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {

  //获取访问明细
 loadList(par) {
    return httpRequest({
      url: "/api/Safe/ApiMonitorLog/LoadList",
      method: 'POST',
      modal: false,
      data: par
    });
  },	
   //删除
   remove(par) {
    return httpRequest({
      url: "/api/Safe/ApiMonitorLog/Remove",
      method: 'POST',
      modal: false,
      data: par
    });
  },
}
