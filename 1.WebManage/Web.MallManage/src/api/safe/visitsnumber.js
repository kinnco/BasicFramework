import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {

  //获取所有的应用
  loadPageList(par) {
    return httpRequest({
      url: "/api/Safe/ApiMonitorNumber/LoadPageList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
}
