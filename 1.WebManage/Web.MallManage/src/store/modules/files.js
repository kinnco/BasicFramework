
 const state = {
   service: {
        basic_service:"",//运维基础服务---几个系统都要用到的文件，就放在这个里面
        official_service:"",//官网服务使用的文件夹
        mall_service:"",//商城服务使用的文件夹
    },
    folder:{
      sys_user_picture:"",//用户头像信息保存地址
      basic_settings_attach:"",//官网基础信息设置附件
      goods_picture:"",//商品附件
    },
 };

/**
 * 更改store中state状态的唯一方法就是提交mutation，就很类似事件。
 * 每个mutation都有一个字符串类型的事件类型和一个回调函数， 我们需要改变state的值就要在回调函数中改变。
 * 我们要执行这个回调函数，那么我们需要执行一个相应的调用方法：store.commit
 */
const mutations = {
  /**
   * 初始化
   * @param {*} state 
   */
  initFiles(state) {
    //所属服务
    state.service.basic_service="BasicInterfaceService";//运维基础服务---几个系统都要用到的文件，就放在这个里面
    state.service.official_service="OfficialService";//官网服务使用的文件夹
    state.service.mall_service="MallService";//官网服务使用的文件夹

    //文件夹
    state.folder.sys_user_picture="SysUserInfo";//用户头像信息保存地址
    state.folder.basic_settings_attach="OfficeBasicSettings";//官网基础信息设置附件
    state.folder.goods_picture="GoodsPicture";//官网基础信息设置附件
  }
};

/**
 * action可以提交mutation，在action中可以执行store.commit，而且action中可以有任何的异步操作。
 * 在页面中如果我们要嗲用这个action，则需要执行store.dispatch
 */
const actions = {
  //执行配置初始化
  initFiles(context) {
    context.commit('initFiles');
  },
}
export default {
    state,
    mutations,
    actions
  };
  