/* 
 * @Author: LW  
 * @Date: 2020-07-31 11:29:35  
 * @function:用户登录状态模块
 */
import common from '@/utils/common';

/**
 * state为单一状态树，在state中需要定义我们所需要管理的数组、对象、字符串等等，只有在这里定义了，
 * 在vue.js的组件中才能获取你定义的这个对象的状态。
 */
const state = {
  userInfo: {},
  isLogin: false,
  uniqueIdentifier: common.getUniqueIdentifier(),
};

/**
 * 更改store中state状态的唯一方法就是提交mutation，就很类似事件。
 * 每个mutation都有一个字符串类型的事件类型和一个回调函数， 我们需要改变state的值就要在回调函数中改变。
 * 我们要执行这个回调函数，那么我们需要执行一个相应的调用方法：store.commit
 */
const mutations = {
  /**
   * 保存账户信息到sessionStorage
   * @param {*} state 
   * @param {*} userInfo 
   */
  setUserInfo(state, userInfo) {
    state.userInfo = userInfo;
    state.isLogin = true;
    //sessionStorage.setItem("userInfo", JSON.stringify(userInfo));
    //sessionStorage.setItem("isLogin", true);
  },
  /**
   * 退出登录
   * @param {} state 
   */
  logout(state) {
    state.userInfo = {};
    state.isLogin = false;
    //sessionStorage.removeItem('userInfo');
    //sessionStorage.removeItem("isLogin");
  },
};

/**
 * action可以提交mutation，在action中可以执行store.commit，而且action中可以有任何的异步操作。
 * 在页面中如果我们要嗲用这个action，则需要执行store.dispatch
 */
const actions = {
  //执行用户登录
  login({commit}, userInfo) {
    commit('setUserInfo', userInfo);
  },
  //执行用户退出登录
  logout(context) {
    context.commit('logout');
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
