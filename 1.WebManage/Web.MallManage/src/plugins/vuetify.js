import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';
import zhHans from 'vuetify/es5/locale/zh-Hans';
// import '@fortawesome/fontawesome-free/css/all.css';
const colors = require('@/utils/colors.js');

console.log(colors)
Vue.use(Vuetify, {
  theme: colors,
  customProperties: true,
  iconfont: 'md',
  lang: {
    locales: { zhHans },
    current: 'zh-Hans',
  },
});
